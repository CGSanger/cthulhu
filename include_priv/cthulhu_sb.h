/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__CTHULHU_SB_H__)
#define __CTHULHU_SB_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define PRIVATE __attribute__ ((visibility("hidden")))
#define UNUSED __attribute__((unused))

#include <cthulhu/cthulhu_sb_data.h>

typedef enum _cthulhu_sb_status {
    cthulhu_sb_status_disabled = 0,
    cthulhu_sb_status_up,
    cthulhu_sb_status_restarting,
    cthulhu_sb_status_last             // delimiter of enum
} cthulhu_sb_status_t;

typedef struct _cb_data_stop {
    char* sb_id;
} cb_data_stop_t;
/**
 * @brief Private data of a sandbox
 *
 */
typedef struct _cthulhu_sb_priv {
    char* cgroup_dir;
    char* storage_imagefile;
    char* storage_mountpoint;
    cb_data_stop_t cb_data_stop;
    cthulhu_sb_status_t real_status;
} cthulhu_sb_priv_t;

static inline cthulhu_sb_priv_t* cthulhu_sb_get_priv(amxd_object_t* sb_obj) {
    if(!sb_obj) {
        return NULL;
    }
    return (cthulhu_sb_priv_t*) sb_obj->priv;
}

void PRIVATE cthulhu_sb_init(void);
void PRIVATE cthulhu_sb_cleanup(void);

cthulhu_action_res_t cthulhu_sb_create(const char* sb_id, const char* parent_sb,
                                       const char* version, const char* vendor,
                                       bool enable, uint32_t cpu, uint32_t memory,
                                       uint32_t diskspace, bool created_by_container,
                                       cthulhu_notif_data_t* notif_data);
cthulhu_action_res_t PRIVATE cthulhu_sb_start(const char* sb_id, cthulhu_notif_data_t* notif_data);
cthulhu_action_res_t PRIVATE cthulhu_sb_stop(const char* sb_id, cthulhu_notif_data_t* notif_data);
cthulhu_action_res_t PRIVATE cthulhu_sb_restart(const char* sb_id, cthulhu_notif_data_t* notif_data);
cthulhu_action_res_t PRIVATE cthulhu_sb_remove(const char* sb_id, cthulhu_notif_data_t* notif_data);
int PRIVATE cthulhu_sb_exec(const char* sb_id, const char* cmd);

const char PRIVATE* cthulhu_sb_status_to_string(cthulhu_sb_status_t status);
cthulhu_sb_status_t PRIVATE cthulhu_string_to_sb_status(const char* status_string);
bool PRIVATE cthulhu_is_sb_enabled(const char* sb_id);

amxd_object_t PRIVATE* cthulhu_sb_get(const char* sb_id);
cthulhu_sb_status_t PRIVATE cthulhu_sb_get_status_dm(amxd_object_t* sb_obj);
cthulhu_sb_status_t PRIVATE cthulhu_sb_get_status_real(amxd_object_t* sb_obj);
int PRIVATE cthulhu_sb_set_status_real(amxd_object_t* sb_obj, cthulhu_sb_status_t sb_status);
cthulhu_sb_data_t PRIVATE* cthulhu_create_sb_data(const char* sb_id);
void PRIVATE delete_sb_priv(amxd_object_t* sb_obj);

#ifdef __cplusplus
}
#endif

#endif // __CTHULHU_SB_H__

