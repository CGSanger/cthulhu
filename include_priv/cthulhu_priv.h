/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__CTHULHU_PRIV_H__)
#define __CTHULHU_PRIV_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxc/amxc_llist.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <amxo/amxo.h>
#include <amxo/amxo_save.h>

#include <amxp/amxp_timer.h>

#include <amxm/amxm.h>

#include <cthulhu/cthulhu_error.h>
#include <cthulhu/cthulhu_ctr_info.h>
#include <cthulhu/cthulhu_ctr_start_data.h>

typedef enum _cthulhu_action_res {
    cthulhu_action_res_error = -1,
    cthulhu_action_res_done = 0,
    cthulhu_action_res_busy = 1
} cthulhu_action_res_t;

typedef struct _cthulhu_resource_stats {
    int64_t total;
    int64_t used;
    int64_t free;
} cthulhu_resource_stats_t;

#include "cthulhu_trace.h"
#include "cthulhu_assert.h"
#include "cthulhu_dm.h"
#include "cthulhu_oci_spec.h"
#include "cthulhu_notif.h"
#include "cthulhu_ctr.h"
#include "cthulhu_sb.h"
#include "cthulhu_namespace.h"
#include "cthulhu_cgroup.h"
#include "cthulhu_overlayfs.h"
#include "cthulhu_archive.h"
#include "cthulhu_autorestart.h"
#include "cthulhu_trans.h"
#include "cthulhu_storage.h"
#include "cthulhu_sigmngrs.h"
#include "cthulhu_syslogng.h"
#include "cthulhu_plugins.h"

#define PRIVATE __attribute__ ((visibility("hidden")))
#define UNUSED __attribute__((unused))

#define ALIAS_SIZE 64

 #define STR1(x)  #x
 #define STR(x)  STR1(x)

typedef struct _cthulhu_app {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    // rlyeh locations
    char* image_location;
    char* blob_location;
    // cthulhu locations
    char* rootfs_location;
    char* layer_location;
    char* data_location;
    char* sb_host_data_location;
    char* syslogng_location;
} cthulhu_app_t;

typedef struct _cthulhu_ctr_priv {
    amxp_timer_t* retry_timer;
    amxp_timer_t* retry_reset_timer;
    cthulhu_ctr_status_t real_status;
} cthulhu_ctr_priv_t;

int _cthulhu_main(int reason,
                  amxd_dm_t* dm,
                  amxo_parser_t* parser);

amxd_dm_t PRIVATE* cthulhu_get_dm(void);
amxo_parser_t PRIVATE* cthulhu_get_parser(void);
cthulhu_app_t PRIVATE* cthulhu_get_app_data(void);

void cthulhu_ctr_priv_clean(cthulhu_ctr_priv_t* ctr_priv);
void cthulhu_ctr_priv_init(cthulhu_ctr_priv_t* ctr_priv);
cthulhu_ctr_priv_t PRIVATE* cthulhu_ctr_get_priv(amxd_object_t* ctr);
void PRIVATE delete_ctr_priv(amxd_object_t* ctr);

bool cthulhu_load_backend(const char* plugin_so);

int llist_append_string(amxc_llist_t* list, const char* str);

#define string_is_empty(s) (!s || !*s)
#define free_null(p) free(p); p = NULL

#ifdef __cplusplus
}
#endif

#endif // __CTHULHU_PRIV_H__