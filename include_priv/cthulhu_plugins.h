/****************************************************************************
**
** Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__CTHULHU_PLUGINS_H__)
#define __CTHULHU_PLUGINS_H__

#ifdef __cplusplus
extern "C"
{
#endif


#define PRIVATE __attribute__ ((visibility("hidden")))
#define UNUSED __attribute__((unused))

int cthulhu_plugins_init(void);
void cthulhu_plugins_cleanup(void);

void cthulhu_plugin_init(void);
void cthulhu_plugin_cleanup(void);

void cthulhu_plugin_ctr_create(const char* ctr_id, const amxc_var_t* data, const image_spec_schema_image_manifest_schema* image_manifest);
void cthulhu_plugin_ctr_postcreate(const char* ctr_id);
void cthulhu_plugin_ctr_prestart(const char* ctr_id, const char* rootfs, cthulhu_ctr_start_data_t* start_data);
void cthulhu_plugin_ctr_prestartnamespace(const char* ctr_id, const char* rootfs);
void cthulhu_plugin_ctr_poststart(const char* ctr_id);
void cthulhu_plugin_ctr_statechanged(const char* ctr_id, cthulhu_ctr_status_t old_state, cthulhu_ctr_status_t new_state);
void cthulhu_plugin_ctr_prestop(const char* ctr_id);
void cthulhu_plugin_ctr_poststop(const char* ctr_id);
void cthulhu_plugin_ctr_preremove(const char* ctr_id);
void cthulhu_plugin_ctr_postremove(const char* ctr_id);

void cthulhu_plugin_sb_create(const char* sb_id, const char* sb_host_data_dir);
void cthulhu_plugin_sb_prestart(const char* sb_id, const char* sb_host_data_dir);
void cthulhu_plugin_sb_poststart(const char* sb_id);
void cthulhu_plugin_sb_statechanged(const char* sb_id, cthulhu_sb_status_t old_state, cthulhu_sb_status_t new_state);
void cthulhu_plugin_sb_prestop(const char* sb_id);
void cthulhu_plugin_sb_poststop(const char* sb_id);
void cthulhu_plugin_sb_preremove(const char* sb_id);
void cthulhu_plugin_sb_postremove(const char* sb_id);


#ifdef __cplusplus
}
#endif

#endif // __CTHULHU_PLUGINS_H__


