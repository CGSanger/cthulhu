#if !defined(__CTHULHU_ASSERT_H__)
#define __CTHULHU_ASSERT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define when_null(x, l) if(x == NULL) { goto l; }
#define when_not_null(x, l) if(x != NULL) { goto l; }
#define when_true(x, l) if(x) { goto l; }
#define when_false(x, l) if(!(x)) { goto l; }
#define when_failed(x, l) if(x != 0) { goto l; }
#define when_str_empty(x, l) if(x == NULL || x[0] == '\0') { goto l; }

#ifdef __cplusplus
}
#endif

#endif // __CTHULHU_ASSERT_H__

