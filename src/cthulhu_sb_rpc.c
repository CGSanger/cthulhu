/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>

#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_helpers.h>

#include "cthulhu_priv.h"

#define ME "cthulhu_rpc"

#define GET_INT32_DEFAULT(a, n, d) (GET_ARG(a, n) ? GET_INT32(a, n) : d)

/**
 * Get a container with a specific sb_id
 *
 * @param sb_id the container sb_id
 *
 * @return amxd_object_t* ctr or NULL when not found
 */
amxd_object_t* cthulhu_sb_get(const char* sb_id) {
    return amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES ".[" CTHULHU_SANDBOX_ID " == '%s'].", sb_id);
}

/**
 * Create an Sandbox. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Sandbox_create(UNUSED amxd_object_t* obj,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;

    const char* sb_id = GET_CHAR(args, CTHULHU_SANDBOX_ID);
    const char* parent_sb = GET_CHAR(args, CTHULHU_SANDBOX_PARENT);
    const char* version = GET_CHAR(args, CTHULHU_SANDBOX_VERSION);
    const char* vendor = GET_CHAR(args, CTHULHU_SANDBOX_VENDOR);
    bool enable = GET_BOOL(args, CTHULHU_SANDBOX_ENABLE);
    uint32_t cpu = GET_INT32_DEFAULT(args, CTHULHU_SANDBOX_CPU, -1);
    uint32_t memory = GET_INT32_DEFAULT(args, CTHULHU_SANDBOX_MEMORY, -1);
    uint32_t diskspace = GET_INT32_DEFAULT(args, CTHULHU_SANDBOX_DISKSPACE, -1);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_sb_create, command_id);

    if(cthulhu_sb_create(sb_id, parent_sb, version, vendor, enable, cpu, memory,
                         diskspace, false, notif_data) != cthulhu_action_res_done) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: could not be created", sb_id);
        goto exit;
    }

    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * Start an Sandbox. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Sandbox_start(UNUSED amxd_object_t* obj,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* sb_id = GET_CHAR(args, CTHULHU_SANDBOX_ID);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_sb_start, command_id);

    if(cthulhu_sb_start(sb_id, notif_data) != cthulhu_action_res_done) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] could not be started", sb_id);
        goto exit;
    }

    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * Start an Sandbox. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Sandbox_stop(UNUSED amxd_object_t* obj,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* sb_id = GET_CHAR(args, CTHULHU_SANDBOX_ID);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_sb_start, command_id);

    if(cthulhu_sb_stop(sb_id, notif_data) == cthulhu_action_res_error) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] could not be stopped", sb_id);
        goto exit;
    }

    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * Restart an Sandbox. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Sandbox_restart(UNUSED amxd_object_t* obj,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* sb_obj = NULL;
    const char* sb_id = GET_CHAR(args, CTHULHU_SANDBOX_ID);
    const char* restart_reason = GET_CHAR(args, CTHULHU_SANDBOX_RESTART_REASON);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_sb_restart, command_id);

    SAH_TRACEZ_INFO(ME, "Restart sandbox [%s] reason [%s]", sb_id, restart_reason);

    sb_obj = cthulhu_sb_get(sb_id);
    if(!sb_obj) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, CTHULHU_SANDBOX " %s does not exist", sb_id);
        goto exit;
    }
    {
        amxd_trans_t trans;
        amxd_status_t trans_status = amxd_status_unknown_error;
        cthulhu_init_trans(&trans, sb_obj);
        amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_STATUS, cthulhu_sb_status_to_string(cthulhu_sb_status_restarting));
        amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_RESTART_REASON, restart_reason ? restart_reason : "");
        trans_status = cthulhu_send_trans(&trans);
        if(trans_status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Transaction failed: %d", trans_status);
            goto exit;
        }
    }

    if(cthulhu_sb_restart(sb_id, notif_data) == cthulhu_action_res_error) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] could not be restarted", sb_id);
        goto exit;
    }

    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * Removes an Sandbox. If the Sandbox is active, it will be stopped first.
 * Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Sandbox_remove(UNUSED amxd_object_t* obj,
                              UNUSED amxd_function_t* func,
                              UNUSED amxc_var_t* args,
                              amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* sb_id = GET_CHAR(args, CTHULHU_SANDBOX_ID);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_sb_remove, command_id);

    if(cthulhu_sb_remove(sb_id, notif_data) == cthulhu_action_res_error) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] could not be removed", sb_id);
        goto exit;
    }

    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

amxd_status_t _Sandbox_command(UNUSED amxd_object_t* obj,
                               UNUSED amxd_function_t* func,
                               UNUSED amxc_var_t* args,
                               amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* command = GET_CHAR(args, CTHULHU_COMMAND_COMMAND);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    amxc_var_t* params = GET_ARG(args, CTHULHU_COMMAND_PARAMETERS);
    amxc_var_t* params_new = NULL;

    if(!string_is_empty(command_id)) {
        if(params == NULL) {
            amxc_var_new(&params_new);
            amxc_var_set_type(params_new, AMXC_VAR_ID_HTABLE);
            params = params_new;
        }
        amxc_var_add_key(cstring_t, params, CTHULHU_NOTIF_COMMAND_ID, command_id);
    }

    status = amxd_object_invoke_function(obj, command, params, ret);

    return status;
}

bool cthulhu_is_sb_enabled(const char* sb_id) {
    amxd_object_t* sb_obj = NULL;

    // check if the Sandbox exists
    sb_obj = cthulhu_sb_get(sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] does not exist", sb_id);
        return false;
    }
    return (cthulhu_sb_get_status_real(sb_obj) == cthulhu_sb_status_up);
}

amxd_status_t _Sandbox_ls(amxd_object_t* obj,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    bool quiet = amxc_var_dyncast(bool, GET_ARG(args, CTHULHU_CMD_CTR_LS_QUIET));
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_sb_ls, command_id);

    cthulhu_notif_sb_list(notif_data, quiet);
    amxc_var_set(bool, ret, true);

    cthulhu_notif_data_delete(&notif_data);
    return amxd_status_ok;
}

/**
 * Execute a command in the namespace. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Sandbox_exec(UNUSED amxd_object_t* obj,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    //amxd_object_t* dm_obj = NULL;
    const char* sb_id = GET_CHAR(args, CTHULHU_SANDBOX_ID);
    const char* cmd = GET_CHAR(args, "Cmd");

    if(cthulhu_sb_exec(sb_id, cmd) != 0) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] exec failed", sb_id);
        goto exit;
    }
    status = amxd_status_ok;
exit:
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}
