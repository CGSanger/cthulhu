/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>

#include "cthulhu_priv.h"

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_config.h>
#include <cthulhu/cthulhu_config_variant.h>
#include <cthulhu/cthulhu_ctr_info.h>
#include <cthulhu/cthulhu_ctr_info_variant.h>
#include <cthulhu/cthulhu_ctr_start_data.h>
#include <cthulhu/cthulhu_ctr_start_data_variant.h>
#include <cthulhu/cthulhu_defines.h>
#include <amxj/amxj_variant.h>

#define ME "cthulhu_dm"

static amxm_shared_object_t* backend = NULL;
static amxp_timer_t* update_timer = NULL;
static unsigned int update_poll_time = 2000;        // time in ms between polling of container states
static amxc_htable_t ctr_sigmngrs;
static amxc_set_t* ctr_start_cb_underway_set;

#define str_or_empty(s) s ? s : ""

static void cthulhu_ctr_update_dm(const char* ctr_id, amxc_var_t* params);
static cthulhu_action_res_t cthulhu_ctr_create(const char* ctr_id, const char* bundle_name,
                                               const char* bundle_version, const char* disklocation,
                                               const char* sb_id, cthulhu_notif_data_t* notif_data,
                                               bool update, const amxc_var_t* const metadata);
static char* cthulhu_ctr_get_priv_sb_id(amxd_object_t* const ctr_obj);
static void cthulhu_ctr_start_canceled_cb(UNUSED const char* const sig_name,
                                          UNUSED const amxc_var_t* const data,
                                          void* const priv);

typedef struct _cb_data_ctr {
    char* ctr_id;
    cthulhu_notif_data_t* notif_data;
} cb_data_ctr_t;

/**
 * Helper to append a string as a amxc_var_t to an amxc_llist_t
 *
 * @return 0 on success
 *
 */
int llist_append_string(amxc_llist_t* list, const char* str) {
    int retval = -1;
    amxc_var_t* var = NULL;

    when_null(list, exit);
    when_null(str, exit);

    when_failed(amxc_var_new(&var), exit);
    when_failed(amxc_var_set(cstring_t, var, str), exit);

    when_failed(amxc_llist_append(list, &var->lit), exit);
    retval = 0;

exit:
    if(retval != 0) {
        amxc_var_delete(&var);
    }
    return retval;
}

/**
 * Helper to append a string as a amxc_var_t to an amxc_htable_t
 *
 * @return int 0 on success
 *
 */
static int htable_add_string(amxc_htable_t* table, const char* key, const char* value) {
    int retval = -1;
    amxc_var_t* var = NULL;
    when_null(table, exit);
    when_null(key, exit);
    when_null(value, exit);

    when_failed(amxc_var_new(&var), exit);
    when_failed(amxc_var_set(cstring_t, var, value), exit);

    when_failed(amxc_htable_insert(table, key, &var->hit), exit);
    retval = 0;

exit:
    if(retval != 0) {
        amxc_var_delete(&var);
    }
    return retval;
}


cthulhu_ctr_priv_t* cthulhu_ctr_get_priv(amxd_object_t* ctr) {
    if(!ctr) {
        return NULL;
    }
    if(!ctr->priv) {
        ctr->priv = (cthulhu_ctr_priv_t*) calloc(1, sizeof(cthulhu_ctr_priv_t));
        cthulhu_ctr_priv_t* ctr_priv = (cthulhu_ctr_priv_t* ) ctr->priv;
        cthulhu_ctr_priv_init(ctr_priv);

    }
    return (cthulhu_ctr_priv_t*) ctr->priv;
}

void delete_ctr_priv(amxd_object_t* ctr) {
    cthulhu_ctr_priv_t* ctr_priv = (cthulhu_ctr_priv_t*) ctr->priv;
    cthulhu_ctr_priv_clean(ctr_priv);
    free(ctr_priv);
    ctr->priv = NULL;
}

/**
 * Get a container with a specific name
 *
 * @param ctr_id the container name
 *
 * @return amxd_object_t* ctr or NULL when not found
 */
amxd_object_t* cthulhu_ctr_get(const char* ctr_id) {
    return amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES ".[" CTHULHU_CONTAINER_ID " == '%s'].", ctr_id);
}

/**
 * @brief get the status of the container as shown in the DM
 * this status can be different from the real status, since the DM does funny things
 * with the restart status
 *
 * @param ctr_obj
 * @return cthulhu_ctr_status_t
 */
cthulhu_ctr_status_t cthulhu_ctr_status_dm_get(amxd_object_t* ctr_obj) {
    cthulhu_ctr_status_t ctr_status = cthulhu_ctr_status_unknown;
    if(!ctr_obj) {
        return ctr_status;
    }
    char* ctr_status_str = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_STATUS, NULL);
    if(!ctr_status_str) {
        return ctr_status;
    }
    ctr_status = cthulhu_string_to_ctr_status(ctr_status_str);
    free(ctr_status_str);
    return ctr_status;
}

/**
 * @brief get the real status of the container
 * this status can be different from the status in the DM, since the DM does funny things
 * with the restart status
 *
 * @param ctr_obj
 * @return cthulhu_ctr_status_t
 */
cthulhu_ctr_status_t cthulhu_ctr_status_real_get(amxd_object_t* ctr_obj) {
    cthulhu_ctr_priv_t* ctr_priv = cthulhu_ctr_get_priv(ctr_obj);
    if(!ctr_priv) {
        return cthulhu_ctr_status_unknown;
    }
    return ctr_priv->real_status;
}

int cthulhu_ctr_status_real_set(amxd_object_t* ctr_obj, cthulhu_ctr_status_t ctr_status) {
    int res = -1;
    cthulhu_ctr_priv_t* ctr_priv = cthulhu_ctr_get_priv(ctr_obj);
    if(!ctr_priv) {
        goto exit;
    }
    ctr_priv->real_status = ctr_status;
    res = 0;
exit:
    return res;
}

static char* cthulhu_ctr_priv_sb_create(const char* ctr_id, const char* sb_id, cthulhu_notif_data_t* notif_data) {
    char* new_sb_id = NULL;

    if(asprintf(&new_sb_id, "__%s_%s__", sb_id, ctr_id) < 0) {
        printf("Could not allocated mem for string");
        goto exit;
    }
    if(cthulhu_sb_get(new_sb_id)) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Private sandbox already exists. Reuse storage", ctr_id);
        goto exit;
    }
    if(cthulhu_sb_create(new_sb_id, sb_id, NULL, NULL, false, 100, -1, -1, true, notif_data) != cthulhu_action_res_done) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: could not create private container sandbox [%s]", ctr_id, new_sb_id);
        free(new_sb_id);
        new_sb_id = NULL;
    }
exit:
    return new_sb_id;
}

static void cthulhu_ctr_priv_sb_stop(amxd_object_t* ctr_obj, cthulhu_notif_data_t* notif_data) {
    char* priv_sb_id = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    cthulhu_action_res_t res;
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
    // start the private sandbox of the container
    priv_sb_id = cthulhu_ctr_get_priv_sb_id(ctr_obj);
    if(string_is_empty(priv_sb_id)) {
        SAH_TRACEZ_ERROR(ME, "CTR: priv_sb_id is empty");
        goto exit;
    }
    if((res = cthulhu_sb_stop(priv_sb_id, notif_data_no_id)) != cthulhu_action_res_done) {
        SAH_TRACEZ_ERROR(ME, "CTR: Expected state 'done' instead of %d", res);
    }
exit:
    free(priv_sb_id);
    cthulhu_notif_data_delete(&notif_data_no_id);
}

static void cthulhu_ctr_priv_sb_remove(amxd_object_t* ctr_obj, cthulhu_notif_data_t* notif_data) {
    char* priv_sb_id = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    cthulhu_action_res_t res;
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
    // start the private sandbox of the container
    priv_sb_id = cthulhu_ctr_get_priv_sb_id(ctr_obj);
    if(string_is_empty(priv_sb_id)) {
        SAH_TRACEZ_ERROR(ME, "CTR: priv_sb_id is empty");
        goto exit;
    }
    if((res = cthulhu_sb_remove(priv_sb_id, notif_data_no_id)) != cthulhu_action_res_done) {
        SAH_TRACEZ_ERROR(ME, "CTR: Expected state 'done' instead of %d", res);
    }
exit:
    free(priv_sb_id);
    cthulhu_notif_data_delete(&notif_data_no_id);
}


/**
 * @brief Callback to continue stopping when the container has stopped
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_ctr_start_cb(UNUSED const char* const sig_name,
                                 UNUSED const amxc_var_t* const data,
                                 void* const priv) {
    amxd_object_t* ctr_obj = NULL;
    amxp_signal_mngr_t* sigmngr = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_ctr_t* cb_data = (cb_data_ctr_t*) priv;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to start container", cb_data->ctr_id);
    amxc_set_remove_flag(ctr_start_cb_underway_set, cb_data->ctr_id);

    ctr_obj = cthulhu_ctr_get(cb_data->ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: does not exist anymore", cb_data->ctr_id);
    }

    // remove the callback
    sigmngr = cthulhu_ctr_get_sigmngr(cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_RUNNING, cthulhu_ctr_start_cb);
        amxp_slot_disconnect(sigmngr, SIG_RUNNING_CANCELED, cthulhu_ctr_start_canceled_cb);
    }
    // notify
    // NOTE replace here with cthulhu_notif_ctr_started if this would ever be necessary
    if(cb_data->notif_data && ctr_obj) {
        cb_data->notif_data->obj = ctr_obj;
        cthulhu_notif_ctr_updated(cb_data->notif_data, NULL);
    }
    cthulhu_plugin_ctr_poststart(cb_data->ctr_id);

    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->ctr_id);
    free(cb_data);
}

/**
 * @brief Callback to continue stopping when the container has stopped
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_ctr_start_canceled_cb(UNUSED const char* const sig_name,
                                          UNUSED const amxc_var_t* const data,
                                          void* const priv) {
    amxd_object_t* ctr_obj = NULL;
    amxp_signal_mngr_t* sigmngr = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_ctr_t* cb_data = (cb_data_ctr_t*) priv;

    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to start cancel container", cb_data->ctr_id);
    amxc_set_remove_flag(ctr_start_cb_underway_set, cb_data->ctr_id);

    ctr_obj = cthulhu_ctr_get(cb_data->ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: does not exist anymore", cb_data->ctr_id);
    }

    // remove the callback
    sigmngr = cthulhu_ctr_get_sigmngr(cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_RUNNING, cthulhu_ctr_start_cb);
        amxp_slot_disconnect(sigmngr, SIG_RUNNING_CANCELED, cthulhu_ctr_start_canceled_cb);
    }

    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->ctr_id);
    free(cb_data);
}

/**
 * @brief Start all containers of a sandbox
 *
 * @param sb_id
 * @return int 0 on success, -1 on failure
 */
static cthulhu_ctr_start_data_t* cthulhu_ctr_get_start_data(amxd_object_t* const ctr_obj) {
    cthulhu_ctr_start_data_t* start_data = NULL;
    char* priv_sb_id = NULL;
    char* ctr_id = NULL;
    amxd_object_t* mount_templ = NULL;

    when_failed(cthulhu_ctr_start_data_new(&start_data), exit);
    ctr_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_ID, NULL);
    when_null(ctr_id, error);
    priv_sb_id = cthulhu_ctr_get_priv_sb_id(ctr_obj);
    when_null(priv_sb_id, error);

    start_data->sb_data = cthulhu_create_sb_data(priv_sb_id);
    if(!start_data->sb_data) {
        goto error;
    }

    mount_templ = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_MOUNTS);
    if(!mount_templ) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get the mounts", ctr_id);
        goto exit;
    }
    // go over all containers
    amxd_object_for_each(instance, it_mnt, mount_templ) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_mnt, amxd_object_t, it);
        char* source = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_SOURCE, NULL);
        char* destination = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_DESTINATION, NULL);
        char* type = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_TYPE, NULL);
        char* options = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_OPTIONS, NULL);
        if(cthulhu_ctr_start_data_add_mount_data(start_data, source, destination, type, options) < 0) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to add mountpoint to the start_data", ctr_id);
            free(source);
            free(destination);
            goto error;
        }
        free(source);
        free(destination);
        free(type);
        free(options);
    }
    goto exit;
error:
    cthulhu_ctr_start_data_delete(&start_data);
exit:
    free(ctr_id);
    free(priv_sb_id);
    return start_data;
}

cthulhu_action_res_t cthulhu_ctr_start(const char* ctr_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Starting container", ctr_id);
    amxc_var_t args;
    amxc_var_t ret;
    char* sb_id = NULL;
    char* priv_sb_id = NULL;
    amxd_object_t* ctr_obj = NULL;
    cthulhu_ctr_status_t ctr_status = cthulhu_ctr_status_unknown;
    cthulhu_ctr_status_t status_in_dm = cthulhu_ctr_status_unknown;
    cthulhu_ctr_start_data_t* start_data = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    char* rootfs = NULL;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    // check the backend
    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "No backend is loaded");
        goto exit;
    }

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }
    // check if the container is already running
    ctr_status = cthulhu_ctr_status_real_get(ctr_obj);
    if((ctr_status == cthulhu_ctr_status_starting) ||
       ( ctr_status == cthulhu_ctr_status_running)) {
        SAH_TRACEZ_INFO(ME, "Container %s already %s", ctr_id, cthulhu_ctr_status_to_string(ctr_status));
        res = cthulhu_action_res_done;
        goto exit;
    }
    // create the sandbox data for the backend
    sb_id = cthulhu_ctr_get_sb_id(ctr_obj);
    if(sb_id) {
        if(!cthulhu_is_sb_enabled(sb_id)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_du_state, "EE [%s] is disabled. Container [%s] will not be started", sb_id, ctr_id);
            goto exit;
        }
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);

    // start the private sandbox of the container
    priv_sb_id = cthulhu_ctr_get_priv_sb_id(ctr_obj);
    if(cthulhu_sb_start(priv_sb_id, notif_data_no_id) != cthulhu_action_res_done) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_du_state, "Private SB [%s] cannot be started. Container [%s] will not be started", priv_sb_id, ctr_id);
        goto exit;
    }

    start_data = cthulhu_ctr_get_start_data(ctr_obj);
    if(!start_data) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_du_state, "EE [%s] has no valid data. Container [%s] will not be started", priv_sb_id, ctr_id);
        goto exit;
    }
    if(cthulhu_syslogng_enable(ctr_id, start_data) < 0) {
        // do continue when syslogng fails to start
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to configure syslog-ng for this container", ctr_id);
    }

    // setup overlayfs
    if(cthulhu_overlayfs_supported()) {
        const char* data_dir = cthulhu_storage_get_dir(priv_sb_id);
        cthulhu_overlayfs_create_rootfs(ctr_id, data_dir);
    }
    rootfs = cthulu_overlayfs_get_rootfs(ctr_id);

    cthulhu_plugin_ctr_prestart(ctr_id, rootfs, start_data);

    // set a callback to send a message
    if(amxc_set_has_flag(ctr_start_cb_underway_set, ctr_id)) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: a callback is already registered", ctr_id);
    } else {
        cb_data_ctr_t* cb_data = (cb_data_ctr_t*) calloc(1, sizeof(cb_data_ctr_t));
        cb_data->ctr_id = strdup(ctr_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        amxp_signal_mngr_t* sigmngr = cthulhu_ctr_get_sigmngr(ctr_id);
        if(sigmngr) {
            amxc_set_add_flag(ctr_start_cb_underway_set, ctr_id);
            amxp_slot_connect(sigmngr,
                              SIG_RUNNING,
                              NULL, cthulhu_ctr_start_cb, cb_data);
            amxp_slot_connect(sigmngr,
                              SIG_RUNNING_CANCELED,
                              NULL, cthulhu_ctr_start_canceled_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->ctr_id);
            free(cb_data);
        }
    }

    // request backend to start
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_START_CTRID, ctr_id);
    amxc_var_add_new_key_cthulhu_ctr_start_data_t(&args, CTHULHU_PLUGIN_START_DATA, start_data);

    if(amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_START, &args, &ret) < 0) {
        goto exit;
    }
    status_in_dm = cthulhu_ctr_status_dm_get(ctr_obj);
    if(status_in_dm != cthulhu_ctr_status_restarting) {
        amxc_var_t params;
        amxc_var_init(&params);
        amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
        amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_STATUS, cthulhu_ctr_status_to_string(cthulhu_ctr_status_starting));
        cthulhu_ctr_update_dm(ctr_id, &params);
        amxc_var_clean(&params);
    }
    cthulhu_ctr_status_real_set(ctr_obj, cthulhu_ctr_status_starting);


    res = cthulhu_action_res_done;
exit:
    free(sb_id);
    free(priv_sb_id);
    free(rootfs);
    cthulhu_ctr_start_data_delete(&start_data);
    cthulhu_notif_data_delete(&notif_data_no_id);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return res;
}

static cthulhu_action_res_t cthulhu_ctr_stop_priv(const char* ctr_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    cthulhu_ctr_status_t ctr_status = cthulhu_ctr_status_unknown;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }

    ctr_status = cthulhu_ctr_status_real_get(ctr_obj);
    if(ctr_status == cthulhu_ctr_status_ghost) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Container is a Ghost", ctr_id);
        res = cthulhu_action_res_done;
        goto exit;
    }

    if(ctr_status == cthulhu_ctr_status_stopped) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Container already %s", ctr_id, cthulhu_ctr_status_to_string(ctr_status));
        res = cthulhu_action_res_done;
        goto exit;
    } else if(ctr_status == cthulhu_ctr_status_stopping) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Container already %s", ctr_id, cthulhu_ctr_status_to_string(ctr_status));
        res = cthulhu_action_res_busy;
        goto exit;
    }

    cthulhu_plugin_ctr_prestop(ctr_id);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    if(amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_STOP, &args, &ret) < 0) {
        goto exit;
    }
    res = cthulhu_action_res_busy;
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return res;
}

/**
 * @brief Callback to continue stopping when the container has stopped
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_ctr_stop_cb(UNUSED const char* const sig_name,
                                UNUSED const amxc_var_t* const data,
                                void* const priv) {
    amxd_object_t* ctr_obj = NULL;
    amxp_signal_mngr_t* sigmngr = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_ctr_t* cb_data = (cb_data_ctr_t*) priv;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to stop sandbox", cb_data->ctr_id);
    ctr_obj = cthulhu_ctr_get(cb_data->ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: does not exist anymore", cb_data->ctr_id);
        goto exit;
    }

    // remove the callback
    sigmngr = cthulhu_ctr_get_sigmngr(cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_ctr_stop_cb);
    }

    cthulhu_plugin_ctr_poststop(cb_data->ctr_id);
    // continue stopping the container
    cthulhu_ctr_stop(cb_data->ctr_id, cb_data->notif_data);
exit:
    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->ctr_id);
    free(cb_data);
}

cthulhu_action_res_t cthulhu_ctr_stop(const char* ctr_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;

    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    SAH_TRACEZ_INFO(ME, "CTR[%s]: Stopping container", ctr_id);

    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "No backend is loaded");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }
    if(notif_data) {
        notif_data->obj = ctr_obj;
    }

    res = cthulhu_ctr_stop_priv(ctr_id, notif_data);
    if(res == cthulhu_action_res_error) {
        goto exit;
    }
    if(res == cthulhu_action_res_busy) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Container is not stopped yet", ctr_id);
        cb_data_ctr_t* cb_data = (cb_data_ctr_t*) calloc(1, sizeof(cb_data_ctr_t));
        cb_data->ctr_id = strdup(ctr_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        amxp_signal_mngr_t* sigmngr = cthulhu_ctr_get_sigmngr(ctr_id);
        if(sigmngr) {
            amxp_sigmngr_trigger_signal(sigmngr, SIG_RUNNING_CANCELED, NULL);
            amxp_slot_connect(sigmngr,
                              SIG_STOPPED,
                              NULL, cthulhu_ctr_stop_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->ctr_id);
            free(cb_data);
        }
        goto exit;
    }
    cthulhu_ctr_priv_sb_stop(ctr_obj, notif_data);
    cthulhu_syslogng_disable(ctr_id);
    cthulhu_notif_ctr_updated(notif_data, NULL);
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return res;
}

typedef struct _cb_data_sb {
    char* ctr_id;
    cthulhu_notif_data_t* notif_data;
} cb_data_sb_t;

static void cthulhu_ctr_restart_cb(UNUSED const char* const sig_name,
                                   UNUSED const amxc_var_t* const data,
                                   void* const priv) {
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_sb_t* cb_data = (cb_data_sb_t*) priv;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to restart container", cb_data->ctr_id);
    // remove the callback
    amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_ctr_restart_cb);
    }
    // restart the container
    cthulhu_ctr_restart(cb_data->ctr_id, cb_data->notif_data);

    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->ctr_id);
    free(cb_data);
}

cthulhu_action_res_t cthulhu_ctr_restart(const char* ctr_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;

    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "No backend is loaded");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "CTR[%s]: Container does not exist", ctr_id);
        goto exit;
    }
    if(notif_data) {
        notif_data->obj = ctr_obj;
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);

    res = cthulhu_ctr_stop(ctr_id, notif_data_no_id);
    if(res == cthulhu_action_res_error) {
        goto exit;
    }
    if(res == cthulhu_action_res_busy) {
        cthulhu_ctr_priv_t* priv = cthulhu_ctr_get_priv(ctr_obj);
        if(!priv) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Container does not have private data!", ctr_id);
            goto exit;
        }
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Container will be restarted after it has stopped", ctr_id);
        cb_data_sb_t* cb_data = (cb_data_sb_t*) calloc(1, sizeof(cb_data_sb_t));
        cb_data->ctr_id = strdup(ctr_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);

        amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, ctr_id);
        if(sigmngr) {
            amxp_slot_connect(sigmngr,
                              SIG_STOPPED,
                              NULL, cthulhu_ctr_restart_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->ctr_id);
            free(cb_data);
        }
        goto exit;
    }

    // restart when the container is stopped
    res = cthulhu_ctr_start(ctr_id, notif_data);
    if(res != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to start container");
    }
exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    return res;
}

typedef struct _cb_data_update {
    char* ctr_id;
    cthulhu_notif_data_t* notif_data;
    char* bundle;
    char* bundle_version;
    char* disk_location;
    char* sb_id;
    bool remove_data;
    amxc_var_t* meta_data;
} cb_data_update_t;

static void cthulhu_data_update_delete(cb_data_update_t** cb_data) {
    when_null(cb_data, exit);
    when_null(*cb_data, exit);

    cthulhu_notif_data_delete(&(*cb_data)->notif_data);
    free((*cb_data)->ctr_id);
    free((*cb_data)->bundle);
    free((*cb_data)->bundle_version);
    free((*cb_data)->disk_location);
    free((*cb_data)->sb_id);
    amxc_var_delete(&(*cb_data)->meta_data);
    free((*cb_data));
    *cb_data = NULL;

exit:
    return;
}

static cthulhu_action_res_t cthulhu_ctr_update_ctu(const char* ctr_id, const char* bundle,
                                                   const char* bundle_version, const char* disk_location,
                                                   const char* sb_id, cthulhu_notif_data_t* notif_data,
                                                   const amxc_var_t* const metadata);

static void cthulhu_ctr_update_cb(UNUSED const char* const sig_name,
                                  UNUSED const amxc_var_t* const data,
                                  void* const priv) {
    cb_data_update_t* cb_data = (cb_data_update_t*) priv;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to update container", cb_data->ctr_id);
    // remove the callback
    amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_REMOVED, cthulhu_ctr_update_cb);
    }
    // create the new container
    cthulhu_ctr_update_ctu(cb_data->ctr_id, cb_data->bundle, cb_data->bundle_version,
                           cb_data->disk_location, cb_data->sb_id,
                           cb_data->notif_data, cb_data->meta_data);

    cthulhu_data_update_delete(&cb_data);
}

cthulhu_action_res_t cthulhu_ctr_update(const char* ctr_id, const char* bundle,
                                        const char* bundle_version, const char* disklocation,
                                        const char* sb_id, bool remove_data,
                                        cthulhu_notif_data_t* notif_data, const amxc_var_t* const metadata) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;

    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "No backend is loaded");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "CTR[%s]: Container does not exist", ctr_id);
        goto exit;
    }
    if(notif_data) {
        notif_data->obj = ctr_obj;
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);

    res = cthulhu_ctr_remove(ctr_id, remove_data, notif_data_no_id);
    if(res == cthulhu_action_res_error) {
        goto exit;
    }
    if(res == cthulhu_action_res_busy) {
        cthulhu_ctr_priv_t* priv = cthulhu_ctr_get_priv(ctr_obj);
        if(!priv) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Container does not have private data!", ctr_id);
            goto exit;
        }
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Container will be updated after it has stopped", ctr_id);
        cb_data_update_t* cb_data = (cb_data_update_t*) calloc(1, sizeof(cb_data_update_t));
        cb_data->ctr_id = strdup(ctr_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        cb_data->bundle = strdup(bundle);
        cb_data->bundle_version = strdup(bundle_version);
        if(disklocation) {
            cb_data->disk_location = strdup(disklocation);
        }
        cb_data->sb_id = strdup(sb_id);
        cb_data->remove_data = remove_data;
        amxc_var_new(&cb_data->meta_data);
        amxc_var_copy(cb_data->meta_data, metadata);

        amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, ctr_id);
        if(sigmngr) {
            amxp_slot_connect(sigmngr,
                              SIG_REMOVED,
                              NULL, cthulhu_ctr_update_cb, cb_data);
        } else {
            cthulhu_data_update_delete(&cb_data);
        }
        goto exit;
    }

    res = cthulhu_ctr_update_ctu(ctr_id, bundle, bundle_version, disklocation, sb_id, notif_data, metadata);

exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    return res;
}


static cthulhu_action_res_t cthulhu_ctr_update_ctu(const char* ctr_id, const char* bundle,
                                                   const char* bundle_version, const char* disk_location,
                                                   const char* sb_id, cthulhu_notif_data_t* notif_data,
                                                   const amxc_var_t* const metadata) {
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    amxd_object_t* ctr_obj = NULL;
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
    cthulhu_action_res_t res = cthulhu_ctr_create(ctr_id, bundle, bundle_version, disk_location, sb_id, notif_data_no_id, true, metadata);
    if(res < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not create container");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(notif_data && ctr_obj) {
        notif_data->obj = ctr_obj;
        cthulhu_notif_ctr_updated(notif_data, NULL);
    }
exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    return res;
}
typedef struct _cb_data_remove {
    char* ctr_id;
    cthulhu_notif_data_t* notif_data;
    bool remove_data;
} cb_data_remove_t;

static void cthulhu_ctr_remove_cb(UNUSED const char* const sig_name,
                                  UNUSED const amxc_var_t* const data,
                                  void* const priv) {
    cb_data_remove_t* cb_data = (cb_data_remove_t*) priv;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Callback to remove container", cb_data->ctr_id);

    // remove the callback
    amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, cb_data->ctr_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_ctr_remove_cb);
    }
    // remove the container
    cthulhu_ctr_remove(cb_data->ctr_id, cb_data->remove_data, cb_data->notif_data);

    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->ctr_id);
    free(cb_data);
}

static bool cthulhu_ctr_exists(const char* const ctr_id) {
    return !!cthulhu_ctr_get(ctr_id);
}

cthulhu_action_res_t cthulhu_ctr_remove(const char* ctr_id, bool remove_data, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    char* sb_id = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    amxp_signal_mngr_t* sigmngr = NULL;

    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "CTR[%s]: No backend is loaded", ctr_id);
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "CTR[%s]: Container does not exist", ctr_id);
        goto exit;
    }
    if(notif_data) {
        notif_data->obj = ctr_obj;
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);

    res = cthulhu_ctr_stop(ctr_id, notif_data_no_id);
    if(res == cthulhu_action_res_error) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Error when stopping. Try to remove anyway.", ctr_id);
    } else if(res == cthulhu_action_res_busy) {
        cthulhu_ctr_priv_t* priv = cthulhu_ctr_get_priv(ctr_obj);
        if(!priv) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Container does not have private data!", ctr_id);
            goto exit;
        }
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Container will be removed when it has stopped", ctr_id);
        cb_data_remove_t* cb_data = ( cb_data_remove_t* ) calloc(1, sizeof(cb_data_remove_t));
        cb_data->ctr_id = strdup(ctr_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        cb_data->remove_data = remove_data;
        sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, cb_data->ctr_id);
        if(sigmngr) {
            amxp_slot_connect(sigmngr,
                              SIG_STOPPED,
                              NULL, cthulhu_ctr_remove_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->ctr_id);
            free(cb_data);
        }
        goto exit;
    }

    cthulhu_plugin_ctr_preremove(ctr_id);

    {
        amxc_var_t args;
        amxc_var_t ret;
        amxc_var_init(&args);
        amxc_var_init(&ret);

        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
        if(amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_REMOVE, &args, &ret)) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not remove container on backend", ctr_id);
            res = cthulhu_action_res_error;
            amxc_var_clean(&args);
            amxc_var_clean(&ret);
            goto exit;
        }
        amxc_var_clean(&args);
        amxc_var_clean(&ret);
    }

    if(remove_data) {
        cthulhu_ctr_priv_sb_remove(ctr_obj, notif_data);
    }

    delete_ctr_priv(ctr_obj);
    // remove instance from DM
    cthulhu_notif_ctr_removed(notif_data);
    amxd_object_emit_del_inst(ctr_obj);
    amxd_object_delete(&ctr_obj);

    cthulhu_remove_unused_layers();
    cthulhu_syslogng_cleanup(ctr_id);
    sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, ctr_id);
    if(sigmngr) {
        amxp_sigmngr_emit_signal(sigmngr, SIG_REMOVED, NULL);
    }
    cthulhu_sigmngrs_delayed_remove(&ctr_sigmngrs, ctr_id, cthulhu_ctr_exists);

    cthulhu_plugin_ctr_postremove(ctr_id);
    res = cthulhu_action_res_done;
exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    free(sb_id);
    return res;
}

static void cthulhu_ctr_update_dm(const char* ctr_id, amxc_var_t* params) {
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* ctr_instances_obj = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj != NULL) {
        // update instance
        amxd_trans_select_object(&trans, ctr_obj);
    } else {
        // add new instance
        ctr_instances_obj = amxd_dm_findf(dm, CTHULHU_DM_CONTAINER_INSTANCES);
        char alias[ALIAS_SIZE + 1];
        if(ctr_instances_obj == NULL) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not find " CTHULHU_DM_CONTAINER_INSTANCES, ctr_id);
            goto exit;
        }
        amxd_trans_select_object(&trans, ctr_instances_obj);
        snprintf(alias, ALIAS_SIZE, "cpe-%s", ctr_id);
        amxd_trans_add_inst(&trans, 0, alias);
        // these params should be immutable, so they should only be set once
        amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_ID, ctr_id);
    }
    amxc_var_for_each(param, params) {
        const char* key = amxc_var_key(param);
        amxd_trans_set_param(&trans, key, param);
    }

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
    }
exit:
    amxd_trans_clean(&trans);
}

static void cthulhu_ctr_started(amxd_object_t* ctr_obj) {
    cthulhu_autorestart_ctr_started(ctr_obj);
}

static void cthulhu_ctr_stopped(const char* ctr_id, amxd_object_t* ctr_obj, cthulhu_ctr_status_t before) {

    if(before == cthulhu_ctr_status_ghost) {
        return;
    }
    cthulhu_autorestart_ctr_stopped(ctr_obj);
    if(cthulhu_overlayfs_supported()) {
        cthulhu_overlayfs_umount_rootfs(ctr_id);
    }
}

static void cthulhu_ctr_status_changed(const char* ctr_id, cthulhu_ctr_status_t before, cthulhu_ctr_status_t after) {
    amxd_object_t* ctr_obj = NULL;
    SAH_TRACEZ_INFO(ME, "CTR[%s]: STATE changed from %s to %s", ctr_id, cthulhu_ctr_status_to_string(before),
                    cthulhu_ctr_status_to_string(after));
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not find ctr object", ctr_id);
        return;
    }
    amxp_signal_mngr_t* sigmngr = cthulhu_sigmngrs_get(&ctr_sigmngrs, ctr_id);
    switch(after) {
    case cthulhu_ctr_status_running:
        cthulhu_ctr_started(ctr_obj);
        if(sigmngr) {
            amxp_sigmngr_emit_signal(sigmngr, SIG_RUNNING, NULL);
        }
        break;
    case cthulhu_ctr_status_stopped:
        cthulhu_ctr_stopped(ctr_id, ctr_obj, before);
        if(sigmngr) {
            amxp_sigmngr_emit_signal(sigmngr, SIG_STOPPED, NULL);
        }
    default:
        break;
    }
    cthulhu_plugin_ctr_statechanged(ctr_id, before, after);
}

/**
 * Update the containers
 *
 * All container info will be fetched form the backend, and the
 * datamodel will be updated accordingly
 *
 *
 * @return int 0 on success
 */
static int cthulhu_update_containers(void) {
    int res = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* ctr_obj = NULL;
    amxd_dm_t* dm = cthulhu_get_dm();
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    if(backend == NULL) {
        SAH_TRACEZ_ERROR(ME, "No backend is loaded");
        goto exit;

    }

    res = amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_LIST, &args, &ret);
    if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to call '" CTHULHU_BACKEND_CMD_LIST "' on module (res: %d)", res);
        goto exit;
    }

    amxc_var_for_each(var, (&ret)) {
        const char* ctr_id = NULL;
        const cthulhu_ctr_info_t* ctr_info = amxc_var_get_const_cthulhu_ctr_info_t(var);
        cthulhu_ctr_status_t status_before = cthulhu_ctr_status_unknown;
        cthulhu_ctr_status_t status_in_dm = cthulhu_ctr_status_unknown;

        if(ctr_info == NULL) {
            SAH_TRACEZ_ERROR(ME, "Could not convert list item to cthulhu_ctr_info_t");
            continue;
        }
        ctr_id = ctr_info->ctr_id;
        ctr_obj = cthulhu_ctr_get(ctr_id);
        status_before = cthulhu_ctr_status_real_get(ctr_obj);
        status_in_dm = cthulhu_ctr_status_dm_get(ctr_obj);

        amxc_var_t params;
        amxc_var_init(&params);
        amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
        // todo: remove this. thsi should be stored in the datamodel, not in the plugin
        amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_OWNER, str_or_empty(ctr_info->owner));
        amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_CREATED, str_or_empty(ctr_info->created));
        if((status_in_dm != cthulhu_ctr_status_restarting) ||
           ((status_in_dm == cthulhu_ctr_status_restarting) && (ctr_info->status != cthulhu_ctr_status_stopped)
            && (ctr_info->status != cthulhu_ctr_status_stopping))) {
            // While a restart of a container, the status has to keep its value to "Restarting" until the container is restarted.
            // Therefore, if the status of the container is "stopped" or "stopping" we do not set the value.
            amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_STATUS, cthulhu_ctr_status_to_string(ctr_info->status));
        }
        amxc_var_add_new_key_int32_t(&params, CTHULHU_CONTAINER_PID, ctr_info->pid);
        cthulhu_ctr_update_dm(ctr_id, &params);
        amxc_var_clean(&params);
        cthulhu_ctr_status_real_set(ctr_obj, ctr_info->status);

        if(status_before != ctr_info->status) {
            cthulhu_ctr_status_changed(ctr_id, status_before, ctr_info->status);
        }
    }

    // now put the status to Ghost for all containers that are not reported back
    ctr_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    if(ctr_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_CONTAINER_INSTANCES);
        goto exit;
    }
    amxd_object_for_each(instance, it, ctr_obj) {
        bool found = false;
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* ctr_id = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ID, NULL);
        amxc_var_for_each(var, (&ret)) {
            const cthulhu_ctr_info_t* ctr_info = amxc_var_get_const_cthulhu_ctr_info_t(var);
            if(ctr_info == NULL) {
                SAH_TRACEZ_ERROR(ME, "Could not convert list item to cthulhu_ctr_info_t");
                continue;
            }
            if(strcmp(ctr_info->ctr_id, ctr_id) == 0) {
                found = true;
                break;
            }
        }
        free(ctr_id);

        if(!found) {
            amxd_trans_init(&trans);
            amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
            amxd_trans_select_object(&trans, instance);
            amxd_trans_set_value(cstring_t, &trans, CTHULHU_CONTAINER_STATUS,
                                 cthulhu_ctr_status_to_string(cthulhu_ctr_status_ghost));
            status = amxd_trans_apply(&trans, dm);
            if(status != amxd_status_ok) {
                SAH_TRACEZ_INFO(ME, "Transaction failed. " CTHULHU_CONTAINER_STATUS ": %d", status);
            }
            amxd_trans_clean(&trans);
        }
    }


    res = 0;
exit:
    amxc_var_for_each(var, (&ret)) {
        cthulhu_ctr_info_t* ctr_info = amxc_var_take_cthulhu_ctr_info_t(var);
        cthulhu_ctr_info_delete(&ctr_info);
    }
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return res;
}

/**
 * Fill in a cthulhu_config_t object with data taken from an OCI
 * config schema
 *
 *
 * @param image_config
 * @param cthulhu_config
 *
 * @return int 0 on success
 */
static int cthulhu_config_fill(image_spec_schema_config_schema_config* image_config,
                               cthulhu_config_t* cthulhu_config) {
    int res = -1;
    if(image_config == NULL) {
        SAH_TRACEZ_ERROR(ME, "Image_config is empty");
        goto exit;
    }
    if(cthulhu_config == NULL) {
        SAH_TRACEZ_ERROR(ME, "config is NULL");
        goto exit;
    }

    // User (Optional)
    if(image_config->user != NULL) {
        cthulhu_config->user = strdup(image_config->user);
    }
    // ExposedPorts (Optional)
    if(image_config->exposed_ports != NULL) {
        for(size_t i = 0; i < image_config->exposed_ports->len; ++i) {
            llist_append_string(&cthulhu_config->exposed_ports,
                                image_config->exposed_ports->keys[i]);
        }
    }
    // Env (Optional)
    for(size_t i = 0; i < image_config->env_len; ++i) {
        llist_append_string(&cthulhu_config->env, image_config->env[i]);
    }

    // Entrypoint (Optional)
    for(size_t i = 0; i < image_config->entrypoint_len; ++i) {
        llist_append_string(&cthulhu_config->entry_point, image_config->entrypoint[i]);
    }

    // Cmd (Optional)
    for(size_t i = 0; i < image_config->cmd_len; ++i) {
        llist_append_string(&cthulhu_config->cmd, image_config->cmd[i]);
    }

    // Volumes (Optional)
    if(image_config->volumes) {
        for(size_t i = 0; i < image_config->volumes->len; ++i) {
            llist_append_string(&cthulhu_config->volumes, image_config->volumes->keys[i]);
        }
    }
    // WorkingDir (Optional)
    if(image_config->working_dir) {
        cthulhu_config->working_dir = strdup(image_config->working_dir);
    }

    // Labels (Optional)
    if(image_config->labels) {
        for(size_t i = 0; i < image_config->labels->len; ++i) {
            htable_add_string(&cthulhu_config->labels,
                              image_config->labels->keys[i],
                              image_config->labels->values[i]);
        }
    }

    // StopSignal (Optional)
    if(image_config->stop_signal) {
        cthulhu_config->stop_signal = strdup(image_config->stop_signal);
    }

    res = 0;
exit:
    return res;
}

static int cthulhu_extract_layers(const char* ctr_id,
                                  image_spec_schema_image_manifest_schema* image_manifest,
                                  UNUSED const char* bundle_name,
                                  const char* storage_location,
                                  const char* layer_location,
                                  const char* ctr_rootfs) {
    int retval = -1;
    amxc_string_t target;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* layer_templ = NULL;
    amxd_object_t* layer_inst = NULL;

    amxc_string_init(&target, 0);

    if(image_manifest->layers_len == 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: No layers are defined for image %s", ctr_id, bundle_name);
        goto exit;
    }

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get DM object", ctr_id);
    }

    layer_templ = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_LAYERS);
    if(!layer_templ) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get the layers", ctr_id);
        goto exit;
    }

    // create the rootfs location. if it exists, delete it first
    // the fact that we are here means that this is a new container instance with this name
    if(cthulhu_isdir(ctr_rootfs)) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Delete directory %s", ctr_id, ctr_rootfs);
        if(cthulhu_overlayfs_supported()) {
            // unmount if it is mounted
            cthulhu_overlayfs_umount_rootfs(ctr_id);
        }
        cthulhu_rmdir(ctr_rootfs);
        if(cthulhu_isdir(ctr_rootfs)) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to remove dir %s", ctr_id, ctr_rootfs);
            goto exit;
        }
    }
    cthulhu_mkdir(ctr_rootfs, false);

    for(size_t i = 0; i < image_manifest->layers_len; i++) {
        if(!image_manifest->layers[i]) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: The content descriptor is empty for index %zu", ctr_id, i);
            goto exit;
        }
        if(cthulhu_overlayfs_supported()) {
            if(cthulhu_extract_layer(image_manifest->layers[i],
                                     storage_location,
                                     layer_location) != 0) {
                SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not extract all layers", ctr_id);
                goto exit;
            }
        } else {
            if(cthulhu_extract_layer_to_rootfs(image_manifest->layers[i],
                                               storage_location,
                                               ctr_rootfs) != 0) {
                SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not extract all layers", ctr_id);
                goto exit;
            }
        }
        amxd_object_add_instance(&layer_inst, layer_templ, NULL, 0, NULL);
        if(!layer_inst) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not create layer instance", ctr_id);
        } else {
            amxd_object_set_uint32_t(layer_inst, CTHULHU_CONTAINER_LAYERS_INDEX, i);
            amxd_object_set_cstring_t(layer_inst, CTHULHU_CONTAINER_LAYERS_LAYER, image_manifest->layers[i]->digest);
        }

    }
    retval = 0;
exit:
    amxc_string_clean(&target);
    return retval;
}

static void cthulhu_update_manifest_params(const char* ctr_id, image_spec_schema_image_manifest_schema* manifest_schema) {
    amxc_var_t params;
    size_t it_annotations;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    if(manifest_schema->annotations) {
        for(it_annotations = 0; it_annotations < manifest_schema->annotations->len; it_annotations++) {
            char* annotation_key = manifest_schema->annotations->keys[it_annotations];
            if(strcmp("org.opencontainers.image.description", annotation_key) == 0) {
                amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_DESCRIPTION, manifest_schema->annotations->values[it_annotations]);
            } else if(strcmp("org.opencontainers.image.vendor", annotation_key) == 0) {
                amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_VENDOR, manifest_schema->annotations->values[it_annotations]);
            }
        }
    }
    cthulhu_ctr_update_dm(ctr_id, &params);
    amxc_var_clean(&params);
}


static int cthulhu_ctr_apply_metadata_resources(const char* ctr_id, const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* resources_obj = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxc_var_t* arg = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_str_empty(ctr_id, exit);
    when_null(metadata, exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get DM object", ctr_id);
        goto exit;
    }

    resources_obj = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_RESOURCES);
    if(!resources_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get Resources from object", ctr_id);
        goto exit;
    }

    amxd_trans_select_object(&trans, resources_obj);

    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_RESOURCES_DISKSPACE)) != NULL) {
        int32_t diskspace = amxc_var_get_int32_t(arg);
        if(diskspace >= -1) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Set Diskspace limit to: %d", ctr_id, diskspace);
            amxd_trans_set_int32_t(&trans, CTHULHU_CONTAINER_RESOURCES_DISKSPACE, diskspace);
        }
    }
    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_RESOURCES_MEMORY)) != NULL) {
        int32_t memory = amxc_var_get_int32_t(arg);
        if(memory >= -1) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Set Memory limit to: %d", ctr_id, memory);
            amxd_trans_set_int32_t(&trans, CTHULHU_CONTAINER_RESOURCES_MEMORY, memory);
        }
    }
    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_RESOURCES_CPU)) != NULL) {
        int32_t cpu = amxc_var_get_int32_t(arg);
        if((cpu >= 0) && (cpu <= 100)) {
            SAH_TRACEZ_INFO(ME, "CTR[%s]: Set CPU limit to: %d", ctr_id, cpu);
            amxd_trans_set_int32_t(&trans, CTHULHU_CONTAINER_RESOURCES_CPU, cpu);
        }
    }

    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
        goto exit;
    }
    ret = 0;

exit:
    amxd_trans_clean(&trans);
    return ret;
}

static int cthulhu_ctr_apply_metadata_autostart(const char* ctr_id, const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxc_var_t* arg = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_str_empty(ctr_id, exit);
    when_null(metadata, exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get DM object", ctr_id);
        goto exit;
    }

    amxd_trans_select_object(&trans, ctr_obj);

    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_AUTOSTART)) != NULL) {
        bool autostart = amxc_var_get_bool(arg);
        amxd_trans_set_bool(&trans, CTHULHU_CONTAINER_AUTOSTART, autostart);
    }

    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
        goto exit;
    }
    ret = 0;

exit:
    amxd_trans_clean(&trans);
    return ret;
}

static int cthulhu_ctr_apply_metadata_autorestart(const char* ctr_id, const amxc_var_t* const metadata) {
    int ret = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* autorestart_obj = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxc_var_t* arg = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_str_empty(ctr_id, exit);
    when_null(metadata, exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get DM object", ctr_id);
        goto exit;
    }

    autorestart_obj = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_AUTORESTART);
    if(!autorestart_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not get Resources from object", ctr_id);
        goto exit;
    }

    amxd_trans_select_object(&trans, autorestart_obj);
    // amxd_trans_add_inst(&trans, 0, NULL);

    if((arg = GET_ARG(metadata, CTHULHU_CONTAINER_AUTORESTART)) != NULL) {
        bool enable = amxc_var_get_bool(arg);
        amxd_trans_set_bool(&trans, CTHULHU_DM_CTR_AR_ENABLED, enable);
    }

    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Status: %d", ctr_id, status);
        goto exit;
    }

exit:
    amxd_trans_clean(&trans);
    return ret;
}

static int cthulhu_ctr_apply_metadata(const char* ctr_id, const amxc_var_t* const metadata) {
    int res = 0;

    res |= cthulhu_ctr_apply_metadata_resources(ctr_id, metadata);
    res |= cthulhu_ctr_apply_metadata_autostart(ctr_id, metadata);
    res |= cthulhu_ctr_apply_metadata_autorestart(ctr_id, metadata);

    return res;
}


static int cthulhu_build_container(cthulhu_notif_data_t* notif_data,
                                   const char* ctr_id,
                                   const char* bundle_name,
                                   const char* bundle_version,
                                   const char* disklocation,
                                   const char* sb_id,
                                   const amxc_var_t* const metadata) {
    int res = -1;
    cthulhu_config_t cthulhu_config;
    amxc_var_t create_data;
    image_spec_schema_image_manifest_schema* image_manifest = NULL;
    image_spec_schema_config_schema* image_config = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_t* data_arg = NULL;
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    const char* bundle_location = string_is_empty(disklocation)? bundle_name : disklocation;

    SAH_TRACEZ_INFO(ME, "CTR[%s]: Create container from %s:%s (location: %s)",
                    ctr_id, bundle_name, bundle_version, bundle_location);

    cthulhu_config_init(&cthulhu_config);
    amxc_var_init(&create_data);
    amxc_var_new(&ret);

    cthulhu_config.id = strdup(ctr_id);
    cthulhu_config.bundle_name = strdup(bundle_name);
    cthulhu_config.bundle_version = strdup(bundle_version);
    if(sb_id) {
        cthulhu_config.sandbox = strdup(sb_id);
        char* cgroup_dir = cthulhu_cgroup_dir_get(sb_id);
        if(cgroup_dir) {
            cthulhu_config.cgroup_dir = cgroup_dir;
        } else {
            cthulhu_config.cgroup_dir = strdup("");
        }
    } else {
        cthulhu_config.sandbox = strdup("");
        cthulhu_config.cgroup_dir = strdup("");
    }
    image_manifest = cthulhu_get_image_manifest_schema_from_image(bundle_location, bundle_version, app_data->image_location, app_data->blob_location);
    if(image_manifest == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not find Image Manifest for image %s:%s", bundle_name, bundle_version);
        goto exit;
    }

    cthulhu_update_manifest_params(ctr_id, image_manifest);

    data_arg = GET_ARG(metadata, CTHULHU_CONTAINER_DATA);

    cthulhu_plugin_ctr_create(ctr_id, data_arg, image_manifest);

    cthulhu_ctr_apply_metadata(ctr_id, metadata);

    if(asprintf(&cthulhu_config.rootfs, "%s/%s", app_data->rootfs_location, ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not allocate the rootfs string", ctr_id);
        goto exit;
    }

    if(cthulhu_extract_layers(ctr_id, image_manifest, bundle_name, app_data->blob_location, app_data->layer_location, cthulhu_config.rootfs) != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not extract the layers for image %s. Is the disk full?", bundle_name);
        goto exit;
    }

    image_config = cthulhu_get_image_config(image_manifest, app_data->blob_location);
    if(image_config == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not find Image Config for image %s", bundle_name);
        goto exit;
    }
    // put the config data needed by the plugins in the config_hash
    if(cthulhu_config_fill(image_config->config, &cthulhu_config) != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to convert config to amxc_var");
        goto exit;
    }

    amxc_var_set(cthulhu_config_t, &create_data, &cthulhu_config);

    res = amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_CMD_CTR_CREATE, &create_data, ret);
    if(res != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Module error (%d): %s",
                     res, amxc_var_constcast(cstring_t, ret));
        goto exit;
    }
    cthulhu_plugin_ctr_postcreate(ctr_id);

    cthulhu_update_containers();

    res = 0;

exit:
    amxc_var_delete(&ret);

    cthulhu_config_clean(&cthulhu_config);
    amxc_var_clean(&create_data);
    if(image_manifest != NULL) {
        free_image_spec_schema_image_manifest_schema(image_manifest);
    }
    if(image_config != NULL) {
        free_image_spec_schema_config_schema(image_config);
    }

    return res;
}

static void cthulhu_ctr_add_mount_to_dm(const char* ctr_id,
                                        const char* source,
                                        const char* destination,
                                        const char* type,
                                        const char* options
                                        ) {
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* mount_instances_obj = NULL;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        goto exit;
    }
    mount_instances_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_MOUNTS);
    if(!mount_instances_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Mounts not found", ctr_id);
        goto exit;
    }
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&trans, mount_instances_obj);
    amxd_trans_add_inst(&trans, 0, NULL);
    // these params should be immutable, so they should only be set once
    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_MOUNTS_SOURCE, source);
    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_MOUNTS_DESTINATION, destination);
    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_MOUNTS_TYPE, type);
    amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_MOUNTS_OPTIONS, options);
    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Transaction failed. Mount not added. Status: %d", ctr_id, status);
    }

exit:
    amxd_trans_clean(&trans);
}
static void cthulhu_ctr_add_defaults(const char* ctr_id) {
    cthulhu_ctr_add_mount_to_dm(ctr_id, "tmpfs", "tmp", "tmpfs", "rw,nodev,nosuid,noatime,create=dir");
    cthulhu_ctr_add_mount_to_dm(ctr_id, "tmpfs", "run", "tmpfs", "rw,nodev,nosuid,noatime,create=dir");
}

static cthulhu_action_res_t cthulhu_ctr_create(const char* ctr_id, const char* bundle_name,
                                               const char* bundle_version, const char* disklocation,
                                               const char* sb_id, cthulhu_notif_data_t* notif_data,
                                               bool update, const amxc_var_t* const metadata) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* config = NULL;
    char* priv_sb_id = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;
    bool autostart = false;

    if(!update) {
        // check if there is already a container with this id
        ctr_obj = cthulhu_ctr_get(ctr_id);
        if(ctr_obj != NULL) {
            res = cthulhu_action_res_done;
            goto exit;
        }
    }
    config = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONFIG);
    if(config == NULL) {
        SAH_TRACEZ_ERROR(ME, CTHULHU_DM_CONFIG " not found in DM");
        goto exit;
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
    if((priv_sb_id = cthulhu_ctr_priv_sb_create(ctr_id, sb_id, notif_data_no_id)) == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "CTR[%s]: Could not create private sandbox", ctr_id);
        goto exit;
    }

    // store initial values in DM
    amxc_var_t params;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_BUNDLE, bundle_name);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_BUNDLEVERSION, bundle_version);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_SANDBOX, str_or_empty(sb_id));
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_CONTAINER_PRIVATESANDBOX, priv_sb_id);
    cthulhu_ctr_update_dm(ctr_id, &params);
    amxc_var_clean(&params);

    cthulhu_ctr_add_defaults(ctr_id);

    if(cthulhu_build_container(notif_data, ctr_id, bundle_name, bundle_version, disklocation, priv_sb_id, metadata) < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "CTR[%s]: Could not create container", ctr_id);
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj && notif_data) {
        notif_data->obj = ctr_obj;
        cthulhu_notif_ctr_created(notif_data);
    }

    autostart = amxd_object_get_bool(ctr_obj, CTHULHU_CONTAINER_AUTOSTART, NULL);
    if(autostart) {
        cthulhu_ctr_start(ctr_id, notif_data_no_id);
    }

    res = cthulhu_action_res_done;

exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    free(priv_sb_id);
    return res;
}

static void cthulhu_fill_ctr_metadata(amxc_var_t* ctr_metadata, amxc_var_t* args) {
    amxc_var_t* var = NULL;
    const char* metadata_params[] = {CTHULHU_CONTAINER_RESOURCES_CPU,
        CTHULHU_CONTAINER_RESOURCES_DISKSPACE,
        CTHULHU_CONTAINER_RESOURCES_MEMORY,
        CTHULHU_CONTAINER_AUTOSTART,
        CTHULHU_CONTAINER_AUTORESTART,
        CTHULHU_CONTAINER_DATA};
    size_t param_cnt = sizeof(metadata_params) / sizeof(metadata_params[0]);

    when_null(ctr_metadata, exit);
    when_null(args, exit);

    for(size_t i = 0; i < param_cnt; i++) {

        var = amxc_var_take_key(args, metadata_params[i]);
        if(!var) {
            continue;
        }
        amxc_var_set_key(ctr_metadata, metadata_params[i], var, AMXC_VAR_FLAG_DEFAULT);
    }
exit:
    return;
}


/**
 * Create a container. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Container_create(amxd_object_t* obj,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    int res = -1;
    const char* bundle_name = GET_CHAR(args, CTHULHU_CONTAINER_BUNDLE);
    const char* bundle_version = GET_CHAR(args, CTHULHU_CONTAINER_BUNDLEVERSION);
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    const char* sb_id = GET_CHAR(args, CTHULHU_CONTAINER_SANDBOX);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    const char* disklocation = GET_CHAR(args, CTHULHU_CONTAINER_DISKLOCATION);
    amxc_var_t* ctr_metadata;
    cthulhu_notif_data_t* notif_data = NULL;

    SAH_TRACEZ_INFO(ME, "bundle_name: %s", bundle_name);
    SAH_TRACEZ_INFO(ME, "bundle_version: %s", bundle_version);
    SAH_TRACEZ_INFO(ME, "ctr_id: %s", ctr_id);
    SAH_TRACEZ_INFO(ME, "sb_id: %s", sb_id);
    SAH_TRACEZ_INFO(ME, "command_id: %s", command_id);
    SAH_TRACEZ_INFO(ME, "disklocation: %s", disklocation);

    amxd_object_t* ctr_obj = NULL;
    bool update = false;

    amxc_var_new(&ctr_metadata);
    amxc_var_set_type(ctr_metadata, AMXC_VAR_ID_HTABLE);

    cthulhu_fill_ctr_metadata(ctr_metadata, args);

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_create, command_id);

    if(backend == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "No backend is loaded");
        goto exit;
    }
    if(string_is_empty(bundle_name)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, CTHULHU_CONTAINER_BUNDLE " is empty");
        goto exit;
    }
    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, CTHULHU_CONTAINER_ID " is empty");
        goto exit;
    }
    if(string_is_empty(bundle_version)) {
        SAH_TRACEZ_WARNING(ME, "No " CTHULHU_CONTAINER_BUNDLEVERSION " given using 'latest'");
        bundle_version = "latest";
    }
    if(!string_is_empty(sb_id)) {
        amxd_object_t* sb_obj = cthulhu_sb_get(sb_id);
        if(!sb_obj) {
            NOTIF_FAILED(notif_data, tr181_fault_unknown_exec_env, "ExecutionEnvironment %s does not exist", sb_id);
            goto exit;
        }
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj) {
        if(cthulhu_ctr_status_dm_get(ctr_obj) != cthulhu_ctr_status_ghost) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "The container already exists");
            goto exit;
        }
        update = true;
    }

    res = cthulhu_ctr_create(ctr_id, bundle_name, bundle_version, disklocation, sb_id, notif_data, update, ctr_metadata);
    if(res < 0) {
        cthulhu_ctr_remove(ctr_id, update, NULL);
    } else {
        status = amxd_status_ok;
    }
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_delete(&ctr_metadata);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * If ContainerId does not match the ContainerId of an existing
 * container then Cthulhu will publish an error message and will
 * take no further action.
 * If the specified container is already running then Cthulhu
 * will immediately publish a "started" notification and will
 * take no further action. Otherwise Cthulhu will attempt to
 * start the container and either a "started" notification or an
 * error will result.
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Container_start(amxd_object_t* obj,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    int res = 0;
    amxd_status_t status = amxd_status_unknown_error;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    SAH_TRACEZ_INFO(ME, "ctr_id: %s", ctr_id);
    SAH_TRACEZ_INFO(ME, "command_id: %s", command_id);

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_start, command_id);

    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "No ContainerId provided");
        goto exit;
    }


    res = cthulhu_ctr_start(ctr_id, notif_data);
    if(res != 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to start container %s",
                     ctr_id);
        goto exit;
    }

    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * If ContainerId does not match the ContainerId of an existing
 * container then Cthulhu will publish an error message and will
 * take no further action.
 * If the specified container is not running then Cthulhu will
 * immediately publish a "stopped" notification and will take no
 * further action. Otherwise Cthulhu will attempt to stop the
 * container and either a "stopped" notification or an error
 * will result.
 */
amxd_status_t _Container_stop(amxd_object_t* obj,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_status_t status = amxd_status_unknown_error;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    SAH_TRACEZ_INFO(ME, "ctr_id: %s", ctr_id);
    SAH_TRACEZ_INFO(ME, "command_id: %s", command_id);

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_stop, command_id);

    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "No ContainerId provided");
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Stopping container", ctr_id);
    res = cthulhu_ctr_stop(ctr_id, notif_data);
    if(res == cthulhu_action_res_error) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to stop container %s",
                     ctr_id);
        goto exit;
    }

    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * If ContainerId does not match the ContainerId of an existing
 * container then Cthulhu will publish an error message and will
 * take no further action.
 * If the specified container is not running then Cthulhu will
 * immediately publish a "stopped" notification and will take no
 * further action. Otherwise Cthulhu will attempt to restart the
 * container and either a "restarted" notification or an error
 * will result.
 */
amxd_status_t _Container_restart(amxd_object_t* obj,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    char* sb_id = NULL;
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    SAH_TRACEZ_INFO(ME, "ctr_id: %s", ctr_id);
    SAH_TRACEZ_INFO(ME, "command_id: %s", command_id);

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_restart, command_id);
    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "No ContainerId provided");
        goto exit;
    }

    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(!ctr_obj) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "CTR[%s]: container does not exist", ctr_id);
        goto exit;
    }
    sb_id = cthulhu_ctr_get_sb_id(ctr_obj);
    if(sb_id) {
        if(!cthulhu_is_sb_enabled(sb_id)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_du_state, "EE [%s] is disabled. Container [%s] will not be started", sb_id, ctr_id);
            goto exit;
        }
    }
    {
        amxd_trans_t trans;
        amxd_status_t trans_status = amxd_status_unknown_error;
        cthulhu_init_trans(&trans, ctr_obj);
        amxd_trans_set_cstring_t(&trans, CTHULHU_CONTAINER_STATUS, cthulhu_ctr_status_to_string(cthulhu_ctr_status_restarting));
        trans_status = cthulhu_send_trans(&trans);
        if(trans_status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "CTR[%s]: Transaction failed: %d", ctr_id, trans_status);
            goto exit;
        }
    }
    res = cthulhu_ctr_restart(ctr_id, notif_data);
    if(res == cthulhu_action_res_error) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "CTR[%s]: Failed to restart container",
                     ctr_id);
        goto exit;
    }
    status = amxd_status_ok;
exit:
    free(sb_id);
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * If ContainerId does not match the ContainerId of an existing
 * container then Cthulhu will publish an error message and will
 * take no further action.
 * If the specified container is running then Cthulhu will stop
 * the container before removing it.
 */
amxd_status_t _Container_remove(amxd_object_t* obj,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_status_t status = amxd_status_unknown_error;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    bool remove_data = GET_BOOL(args, CTHULHU_CONTAINER_REMOVEDATA);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    SAH_TRACEZ_INFO(ME, "ctr_id: %s", ctr_id);
    SAH_TRACEZ_INFO(ME, "command_id: %s", command_id);
    SAH_TRACEZ_INFO(ME, "remove_data: %d", remove_data);

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_remove, command_id);

    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "No ContainerId provided");
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Removing container. %s data.", ctr_id, remove_data ? "Removing" : "Keeping");
    res = cthulhu_ctr_remove(ctr_id, remove_data, notif_data);
    if(res == cthulhu_action_res_error) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to remove container %s",
                     ctr_id);
        goto exit;
    }

    cthulhu_update_containers();
    status = amxd_status_ok;
exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * If ContainerId does not match the ContainerId of an existing
 * container then Cthulhu will publish an error message and will
 * take no further action.
 * If the specified container is running then Cthulhu will stop
 * the container before updating it.
 */
amxd_status_t _Container_update(amxd_object_t* obj,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* ctr_obj = NULL;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    const char* bundle = GET_CHAR(args, CTHULHU_CONTAINER_BUNDLE);
    const char* bundle_version = GET_CHAR(args, CTHULHU_CONTAINER_BUNDLEVERSION);
    const char* disklocation = GET_CHAR(args, CTHULHU_CONTAINER_DISKLOCATION);
    bool remove_data = GET_BOOL(args, CTHULHU_CONTAINER_REMOVEDATA);
    char* bundle_default = NULL;
    char* sb_id = NULL;
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    amxc_var_t* ctr_metadata;
    cthulhu_notif_data_t* notif_data = NULL;

    SAH_TRACEZ_INFO(ME, "bundle: %s", bundle);
    SAH_TRACEZ_INFO(ME, "bundle_version: %s", bundle_version);
    SAH_TRACEZ_INFO(ME, "ctr_id: %s", ctr_id);
    SAH_TRACEZ_INFO(ME, "command_id: %s", command_id);
    SAH_TRACEZ_INFO(ME, "disklocation: %s", disklocation);

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_update, command_id);
    amxc_var_new(&ctr_metadata);
    amxc_var_set_type(ctr_metadata, AMXC_VAR_ID_HTABLE);

    if(string_is_empty(ctr_id)) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "No ContainerId provided");
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }

    if(string_is_empty(bundle)) {
        bundle_default = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_BUNDLEVERSION, NULL);
        bundle = bundle_default;
        if(string_is_empty(bundle)) {
            NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Container %s Bundle is unknown", ctr_id);
            goto exit;
        }
    }
    if(string_is_empty(bundle_version)) {
        bundle_version = "latest";
    }

    cthulhu_fill_ctr_metadata(ctr_metadata, args);

    sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_SANDBOX, NULL);
    res = cthulhu_ctr_update(ctr_id, bundle, bundle_version, disklocation, sb_id, remove_data, notif_data, ctr_metadata);
    if(res == cthulhu_action_res_error) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to update container %s",
                     ctr_id);
        goto exit;
    }

    cthulhu_update_containers();
    status = amxd_status_ok;
exit:
    free(bundle_default);
    free(sb_id);
    amxc_var_delete(&ctr_metadata);
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

/**
 * Call a generic command. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Container_command(amxd_object_t* obj,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;

    const char* command = GET_CHAR(args, CTHULHU_COMMAND_COMMAND);
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    amxc_var_t* params = GET_ARG(args, CTHULHU_COMMAND_PARAMETERS);
    amxc_var_t* params_new = NULL;

    if(!string_is_empty(command_id)) {
        if(params == NULL) {
            amxc_var_new(&params_new);
            amxc_var_set_type(params_new, AMXC_VAR_ID_HTABLE);
            params = params_new;
        }
        amxc_var_add_key(cstring_t, params, CTHULHU_NOTIF_COMMAND_ID, command_id);
    }

    status = amxd_object_invoke_function(obj, command, params, ret);

    amxc_var_delete(&params_new);
    return status;
}

/**
 * Call a generic command. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Container_ls(amxd_object_t* obj,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    bool quiet = amxc_var_dyncast(bool, GET_ARG(args, CTHULHU_CMD_CTR_LS_QUIET));
    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_ctr_update, command_id);

    if(backend != NULL) {
        cthulhu_update_containers();
    }

    cthulhu_notif_ctr_list(notif_data, quiet);

    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, true);
    return amxd_status_ok;
}

bool cthulhu_load_backend(const char* backend_so) {
    bool ret = false;

    if(backend) {
        SAH_TRACEZ_ERROR(ME, "Backend already set: %p", backend);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Load backend [%s]", backend_so);
    if(amxm_so_open(&backend, "packager", backend_so) != 0) {
        SAH_TRACEZ_ERROR(ME, "Can not load backend [%s]", backend_so);
        goto exit;
    }

    if(amxm_so_get_module(backend, MOD_CTHULHU) == NULL) {
        SAH_TRACEZ_ERROR(ME, "Library does not provide %s", MOD_CTHULHU);
        amxm_so_close(&backend);
        goto exit;
    }

    ret = true;
exit:
    return ret;
}

/**
 * Update the versions of cthulhu and the backend
 *
 */
static void cthulhu_update_versions(void) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_dm_t* dm = cthulhu_get_dm();
    amxd_object_t* info = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    info = amxd_dm_findf(dm, CTHULHU_DM_INFO);
    if(!info) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_INFO);
        goto exit;
    }
    amxd_trans_select_object(&trans, info);
#ifdef VERSION
    amxd_trans_set_cstring_t(&trans, CTHULHU_INFO_VERSION, STR(VERSION));
#endif
    if(backend) {
        amxc_var_t ret;
        amxc_var_t args;
        amxc_var_init(&ret);
        amxc_var_init(&args);
        int res = amxm_so_execute_function(backend, MOD_CTHULHU, CTHULHU_BACKEND_CMD_VERSION, &args, &ret);
        if(res != 0) {
            SAH_TRACEZ_ERROR(ME, "Could not get version from backend");
        } else {
            amxd_trans_set_param(&trans, CTHULHU_INFO_BACKENDVERSION, &ret);
        }
        amxc_var_clean(&ret);
        amxc_var_clean(&args);
    }

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed. " CTHULHU_CONTAINER_STATUS ": %d", status);
    }
exit:
    amxd_trans_clean(&trans);
}

/**
 * Load a plugin. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Cthulhu_loadBackend(UNUSED amxd_object_t* cthulhu,
                                   UNUSED amxd_function_t* func,
                                   amxc_var_t* args,
                                   amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* backend_so = GET_CHAR(args, CTHULHU_CMD_CTR_LOAD_BACKEND);

    if(cthulhu_load_backend(backend_so)) {
        status = amxd_status_ok;
        cthulhu_update_versions();
    }

    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

static void update_timer_cb(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    if(backend != NULL) {
        cthulhu_update_containers();
    }
}

char* cthulhu_ctr_get_sb_id(amxd_object_t* ctr_obj) {
    char* sb_id = NULL;
    when_null(ctr_obj, exit);
    sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_SANDBOX, NULL);
    if(string_is_empty(sb_id)) {
        free(sb_id);
        sb_id = NULL;
    }
exit:
    return sb_id;
}

static char* cthulhu_ctr_get_priv_sb_id(amxd_object_t* const ctr_obj) {
    char* priv_sb_id = NULL;
    when_null(ctr_obj, exit);
    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    if(string_is_empty(priv_sb_id)) {
        free(priv_sb_id);
        priv_sb_id = NULL;
    }
exit:
    return priv_sb_id;
}

amxp_signal_mngr_t* cthulhu_ctr_get_sigmngr(const char* ctr_id) {
    return cthulhu_sigmngrs_get(&ctr_sigmngrs, ctr_id);
}

int cthulhu_init(void) {
    int res = -1;
    amxd_object_t* config = NULL;
    char* backend_path = NULL;
    char* storage_location = NULL;
    amxp_timer_new(&update_timer, update_timer_cb, NULL);
    amxp_timer_set_interval(update_timer, update_poll_time);
    amxp_timer_start(update_timer, update_poll_time);
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    cthulhu_sigmngrs_init(&ctr_sigmngrs);
    amxc_set_new(&ctr_start_cb_underway_set, false);

    config = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONFIG);
    if(config) {
        backend_path = amxd_object_get_cstring_t(config, CTHULHU_CONFIG_DEFAULTBACKEND, NULL);
        storage_location = amxd_object_get_cstring_t(config, CTHULHU_CONFIG_STORAGE_LOCATION, NULL);
        if(!string_is_empty(backend_path)) {
            SAH_TRACEZ_NOTICE(ME, "Load backend %s", backend_path);
            cthulhu_load_backend(backend_path);
        }
        app_data->image_location = amxd_object_get_cstring_t(config, CTHULHU_CONFIG_IMAGE_LOCATION, NULL);
        app_data->blob_location = amxd_object_get_cstring_t(config, CTHULHU_CONFIG_BLOB_LOCATION, NULL);
        if(storage_location) {
            if(asprintf(&(app_data->rootfs_location), "%s/%s", storage_location, "rootfs") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->rootfs_location");
                goto exit;
            }
            if(asprintf(&(app_data->data_location), "%s/%s", storage_location, "data") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->data_location");
                goto exit;
            }
            if(asprintf(&(app_data->sb_host_data_location), "%s/%s", storage_location, "sb_host_data") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->data_location");
                goto exit;
            }
            if(asprintf(&(app_data->layer_location), "%s/%s", storage_location, "layers") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->data_location");
                goto exit;
            }
            if(asprintf(&(app_data->syslogng_location), "%s/%s", storage_location, "syslogng") < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not init string app_data->syslogng_location");
                goto exit;
            }
            if(cthulhu_mkdir(app_data->rootfs_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make rootdir [%s]", app_data->rootfs_location);
                goto exit;
            }
            if(cthulhu_mkdir(app_data->data_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", app_data->data_location);
                goto exit;
            }
            if(cthulhu_mkdir(app_data->sb_host_data_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", app_data->sb_host_data_location);
                goto exit;
            }
            if(cthulhu_mkdir(app_data->layer_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", app_data->layer_location);
                goto exit;
            }
            if(cthulhu_mkdir(app_data->syslogng_location, true) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", app_data->syslogng_location);
                goto exit;
            }
        }
    }
    cthulhu_syslogng_init();
    cthulhu_update_versions();
    cthulhu_plugins_init();

    res = 0;
exit:
    free(backend_path);
    free(storage_location);
    return res;
}

static int cb_container_cleanup(UNUSED amxd_object_t* templ,
                                amxd_object_t* instance,
                                UNUSED void* priv) {
    delete_ctr_priv(instance);
    return 0;
}

static int cb_sb_cleanup(UNUSED amxd_object_t* templ,
                         amxd_object_t* instance,
                         UNUSED void* priv) {
    char* sb_id = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_ID, NULL);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, instance, cthulhu_notif_cmd_none, NULL);
    cthulhu_sb_stop(sb_id, notif_data);

    cthulhu_notif_data_delete(&notif_data);
    delete_sb_priv(instance);
    free(sb_id);
    return 0;
}


void cthulhu_cleanup(void) {
    cthulhu_plugins_cleanup();
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    amxp_timer_stop(update_timer);
    amxp_timer_delete(&update_timer);
    // cleanup ee
    amxd_object_t* sandboxes = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES);
    amxd_object_for_all(sandboxes, "*", cb_sb_cleanup, NULL);
    // cleanup containers
    amxd_object_t* containers = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    amxd_object_for_all(containers, "*", cb_container_cleanup, NULL);
    amxm_so_close(&backend);
    cthulhu_sigmngrs_clean(&ctr_sigmngrs);
    amxc_set_delete(&ctr_start_cb_underway_set);

    free(app_data->image_location);
    free(app_data->blob_location);
    free(app_data->rootfs_location);
    free(app_data->layer_location);
    free(app_data->data_location);
    free(app_data->sb_host_data_location);
    free(app_data->syslogng_location);
}

