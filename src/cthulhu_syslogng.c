/****************************************************************************
**
** Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <cthulhu/cthulhu_helpers.h>

#include "cthulhu_priv.h"

#define ME "cthulhu_syslogng"


static char* cthulhu_syslogng_get_log_dir(void) {
    char* log_dir = NULL;
    cthulhu_app_t* app_data = cthulhu_get_app_data();

    when_null(app_data, exit);

    if(asprintf(&log_dir, "%s/logs", app_data->syslogng_location) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate the syslogng logs string");
        goto exit;
    }

exit:
    return log_dir;
}

static char* cthulhu_syslogng_get_ctr_log_dir(const char* ctr_id) {
    char* log_dir = NULL;
    cthulhu_app_t* app_data = cthulhu_get_app_data();

    when_null(ctr_id, exit);
    when_null(app_data, exit);

    if(asprintf(&log_dir, "%s/logs/%s", app_data->syslogng_location, ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not allocate the syslogng logs string", ctr_id);
        goto exit;
    }

exit:
    return log_dir;
}

static char* cthulhu_syslogng_get_config_dir(void) {
    char* config_dir = NULL;
    cthulhu_app_t* app_data = cthulhu_get_app_data();

    when_null(app_data, exit);

    if(asprintf(&config_dir, "%s/configs", app_data->syslogng_location) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR: Could not allocate the config string");
        goto exit;
    }

exit:
    return config_dir;
}

static char* cthulhu_syslogng_get_config_filename(const char* ctr_id) {
    char* config_filename = NULL;
    char* config_dir = cthulhu_syslogng_get_config_dir();

    when_null(ctr_id, exit);
    when_null(config_dir, exit);

    if(asprintf(&config_filename, "%s/%s.conf", config_dir, ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR: Could not allocate the config filename string");
        goto exit;
    }

exit:
    free(config_dir);
    return config_filename;
}

static char* cthulhu_syslogng_get_socket_filename(const char* ctr_id) {
    char* log_socketname = NULL;
    char* log_dir = cthulhu_syslogng_get_ctr_log_dir(ctr_id);

    when_null(log_dir, exit);

    if(asprintf(&log_socketname, "%s/log.sock", log_dir) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not allocate the socket filename string", ctr_id);
        goto exit;
    }

exit:
    free(log_dir);
    return log_socketname;
}

static char* cthulhu_syslogng_get_messages_filename(const char* ctr_id) {
    char* log_messagesname = NULL;
    char* log_dir = cthulhu_syslogng_get_ctr_log_dir(ctr_id);

    when_null(log_dir, exit);

    if(asprintf(&log_messagesname, "%s/messages", log_dir) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not allocate the messages filename string", ctr_id);
        goto exit;
    }

exit:
    free(log_dir);
    return log_messagesname;
}

static int cthulhu_syslogng_create_ctr_log_dir(const char* ctr_id) {
    int retval = -1;
    char* log_dir = cthulhu_syslogng_get_ctr_log_dir(ctr_id);

    when_null(log_dir, exit);

    if(cthulhu_mkdir(log_dir, true) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not create syslog dir [%s]", ctr_id, log_dir);
        goto exit;
    }

    retval = 0;
exit:
    free(log_dir);
    return retval;
}

static int cthulhu_syslogng_reload_config(void) {
    int retval = -1;
    int cmd_status = -1;

    if((cthulhu_proc_wait("syslog-ng-ctl reload", &cmd_status) < 0) || (cmd_status != 0)) {
        SAH_TRACEZ_ERROR(ME, "Could not reload syslog-ng (%d)", cmd_status);
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

static int cthulhu_syslogng_remove_config(const char* ctr_id) {
    int retval = -1;
    char* config_filename = NULL;

    config_filename = cthulhu_syslogng_get_config_filename(ctr_id);
    when_null(config_filename, exit);

    if(!cthulhu_isfile(config_filename)) {
        SAH_TRACEZ_WARNING(ME, "CTR[%s]: Syslogng config file does not exist [%s]", ctr_id, config_filename);
        goto exit;
    }

    if(unlink(config_filename) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: could not remove syslogng config file [%s] (%d: %s)",
                         ctr_id, config_filename, errno, strerror(errno));
        goto exit;
    }

    retval = 0;
exit:
    free(config_filename);
    return retval;
}

static int cthulhu_syslogng_remove_socket(const char* ctr_id) {
    int retval = -1;
    char* socket_filename = NULL;

    socket_filename = cthulhu_syslogng_get_socket_filename(ctr_id);
    when_null(socket_filename, exit);

    if(!cthulhu_issocket(socket_filename)) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Syslogng socket file does not exist [%s]", ctr_id, socket_filename);
        retval = 0;
        goto exit;
    }

    if(unlink(socket_filename) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: could not remove syslogng socket file [%s] (%d: %s)",
                         ctr_id, socket_filename, errno, strerror(errno));
        goto exit;
    }
    retval = 0;
exit:
    free(socket_filename);
    return retval;
}

static int cthulhu_syslogng_create_messages_file(const char* ctr_id) {
    int retval = -1;
    char* messages_file = NULL;
    int fd = -1;

    when_null(ctr_id, exit);

    messages_file = cthulhu_syslogng_get_messages_filename(ctr_id);
    when_null(messages_file, exit);

    if(cthulhu_isfile(messages_file)) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: messages file already exists [%s]", ctr_id, messages_file);
        retval = 0;
        goto exit;
    }
    if((fd = creat(messages_file, 0777)) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: creat failed [%s] (%d: %s)", messages_file, messages_file, errno, strerror(errno));
        goto exit;
    }
    close(fd);
    retval = 0;


exit:
    free(messages_file);
    return retval;
}

static int cthulhu_syslogng_create_config(const char* ctr_id) {
    int retval = -1;
    char* config_filename = NULL;
    char* socket_filename = NULL;
    char* messages_filename = NULL;
    FILE* f = NULL;

    config_filename = cthulhu_syslogng_get_config_filename(ctr_id);
    when_null(config_filename, exit);
    socket_filename = cthulhu_syslogng_get_socket_filename(ctr_id);
    when_null(socket_filename, exit);
    messages_filename = cthulhu_syslogng_get_messages_filename(ctr_id);
    when_null(messages_filename, exit);

    if((f = fopen(config_filename, "w")) == NULL) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to open syslogng config file [%s]", ctr_id, config_filename);
        goto exit;
    }

    int res = fprintf(f,
                      "source s_%s { unix-dgram(\"%s\"); };\n"
                      "destination d_%s { file(\"%s\"); };\n"
                      "log { source(s_%s); destination(d_%s); };\n",
                      ctr_id, socket_filename, ctr_id, messages_filename, ctr_id, ctr_id);
    if(res < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to write data to config file", ctr_id);
        goto exit;
    }

    retval = 0;
exit:
    if(f) {
        fclose(f);
    }
    free(config_filename);
    free(socket_filename);
    free(messages_filename);
    return retval;
}

int cthulhu_syslogng_enable(const char* ctr_id,
                            cthulhu_ctr_start_data_t* start_data) {
    int retval = -1;
    char* socket_filename = NULL;
    char* config_filename = NULL;

    socket_filename = cthulhu_syslogng_get_socket_filename(ctr_id);
    when_null(socket_filename, exit);
    config_filename = cthulhu_syslogng_get_config_filename(ctr_id);
    when_null(config_filename, exit);

// first cleanup any leftovers from a previous session
    if(cthulhu_syslogng_remove_socket(ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not remove syslogng socket", ctr_id);
        goto exit;
    }
    if(cthulhu_isfile(config_filename)) {
        SAH_TRACEZ_WARNING(ME, "CTR[%s]: Cleanup existing config", ctr_id);
        cthulhu_syslogng_remove_config(ctr_id);
        if(cthulhu_syslogng_reload_config() < 0) {
            SAH_TRACEZ_WARNING(ME, "CTR[%s]: Failed to reload syslog-ng config. Is syslog-ng running", ctr_id);
            retval = 0;
            goto exit;
        }
    }

    if(cthulhu_syslogng_create_ctr_log_dir(ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not create logdir", ctr_id);
        goto exit;
    }
    if(cthulhu_syslogng_create_messages_file(ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not create messages file", ctr_id);
        goto exit;
    }
    if(cthulhu_syslogng_create_config(ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not create config file", ctr_id);
        goto exit;
    }
    if(cthulhu_syslogng_reload_config() < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to restart syslogng", ctr_id);
        goto exit;
    }
    if(!cthulhu_issocket(socket_filename)) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Syslogng did not create the log socket [%s]", ctr_id, socket_filename);
        goto exit;
    }
    if(cthulhu_ctr_start_data_add_mount_data(start_data, socket_filename, "/run/log.sock", NULL, NULL) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not configure socket mount", ctr_id);
        goto exit;
    }

    retval = 0;
    goto exit;

exit:
    free(socket_filename);
    free(config_filename);
    return retval;
}

int cthulhu_syslogng_disable(const char* ctr_id) {
    int retval = -1;

    if(cthulhu_syslogng_remove_socket(ctr_id) < 0) {

        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not remove syslogng socket", ctr_id);
    }
    if(cthulhu_syslogng_remove_config(ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not remove syslogng config", ctr_id);
        goto exit;
    }
    if(cthulhu_syslogng_reload_config() < 0) {
        SAH_TRACEZ_WARNING(ME, "CTR[%s]: Failed to restart syslogng", ctr_id);
    }

    retval = 0;
exit:
    return retval;
}

static void cthulhu_syslogng_remove_log_dir(const char* ctr_id) {
    char* log_dir = NULL;

    log_dir = cthulhu_syslogng_get_ctr_log_dir(ctr_id);
    when_null(log_dir, exit);

    SAH_TRACEZ_ERROR(ME, "CTR[%s]: Delete directory [%s]", ctr_id, log_dir);
    if(!cthulhu_isdir(log_dir)) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Syslogng dir does not exist [%s]", ctr_id, log_dir);
        goto exit;
    }
    if(cthulhu_rmdir(log_dir) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to remove dir [%s]", ctr_id, log_dir);
        goto exit;
    }
exit:
    free(log_dir);
}

void cthulhu_syslogng_cleanup(const char* ctr_id) {
    cthulhu_syslogng_remove_log_dir(ctr_id);
    cthulhu_syslogng_remove_config(ctr_id);
}

int cthulhu_syslogng_init(void) {
    int retval = -1;
    char* log_dir = NULL;
    char* config_dir = NULL;

    log_dir = cthulhu_syslogng_get_log_dir();
    when_null(log_dir, exit);
    if(!cthulhu_isdir(log_dir)) {
        if(cthulhu_mkdir(log_dir, true) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not create syslog dir [%s]", log_dir);
            goto exit;
        }
    }
    config_dir = cthulhu_syslogng_get_config_dir();
    when_null(config_dir, exit);
    if(!cthulhu_isdir(config_dir)) {
        if(cthulhu_mkdir(config_dir, true) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not create syslog config [%s]", log_dir);
            goto exit;
        }
    }

    retval = 0;
exit:
    free(log_dir);
    free(config_dir);
    return retval;
}
