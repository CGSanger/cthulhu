/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <math.h>

#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_priv.h"

#define ME "cthulhu_autorestart"

static bool cthulhu_start_retry_timer(amxd_object_t* ctr_obj);
static void cthulu_retrytimer_elapsed(UNUSED amxp_timer_t* timer, void* priv);


static void cthulu_retrytimer_elapsed(UNUSED amxp_timer_t* timer, void* priv) {
    amxd_object_t* ctr_obj = (amxd_object_t*) priv;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxc_ts_t now;
    amxd_object_t* restart = amxd_object_get_child(ctr_obj, CTHULHU_CONTAINER_AUTORESTART);
    char* sb_id = NULL;
    char* ctr_id = NULL;
    int res = 0;

    ctr_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_ID, NULL);
    SAH_TRACEZ_INFO(ME, "[%s] Retry timer elapsed", ctr_id);

    cthulhu_init_trans(&trans, restart);
    amxc_ts_now(&now);
    amxd_trans_set_amxc_ts_t(&trans, CTHULHU_DM_CTR_AR_LASTRETRY, &now);
    status = cthulhu_send_trans(&trans);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
    }

    // start the container
    sb_id = cthulhu_ctr_get_sb_id(ctr_obj);
    if(sb_id && !cthulhu_is_sb_enabled(sb_id)) {
        SAH_TRACEZ_NOTICE(ME, "CTR[%s]: Sandbox [%s] is disabled. Container will not be started", ctr_id, sb_id);
        goto exit;
    }

    res = cthulhu_ctr_start(ctr_id, NULL);
    if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "Module error: %d", res);
    }
    cthulhu_start_retry_timer(ctr_obj);
exit:
    free(ctr_id);
    free(sb_id);
}

static void cthulu_retryresettimer_elapsed(UNUSED amxp_timer_t* timer, void* priv) {
    amxd_object_t* ctr_obj = (amxd_object_t*) priv;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_object_t* restart = amxd_object_get_child(ctr_obj, CTHULHU_CONTAINER_AUTORESTART);
    char* ctr_id = NULL;

    ctr_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_ID, NULL);
    SAH_TRACEZ_INFO(ME, "CTR[%s]: reset retry counter", ctr_id);
    cthulhu_init_trans(&trans, restart);
    amxd_trans_set_uint32_t(&trans, CTHULHU_DM_CTR_AR_RETRYCOUNT, 0);
    status = cthulhu_send_trans(&trans);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
    }
    free(ctr_id);
}

static bool cthulhu_start_retry_timer(amxd_object_t* ctr_obj) {
    // increase retrycount
    bool res = false;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxc_ts_t next_retry;
    amxc_ts_t ts_zero = {0, 0, 0};
    unsigned int nextRestartTime;
    uint32_t retryCount;
    cthulhu_ctr_priv_t* ctr_priv = NULL;
    char* ctr_id = NULL;

    ctr_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_ID, NULL);
    amxd_object_t* restart = amxd_object_get_child(ctr_obj, CTHULHU_CONTAINER_AUTORESTART);
    if(!restart) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: No restart data found", ctr_id);
        goto exit;
    }

    retryCount = amxd_object_get_uint32_t(restart, CTHULHU_DM_CTR_AR_RETRYCOUNT, NULL);
    nextRestartTime = 60 * pow(2, retryCount);
    if(nextRestartTime > 86400) {
        nextRestartTime = 86400;
    }
    SAH_TRACEZ_INFO(ME, "CTR[%s]: next auto start in %ds", ctr_id, nextRestartTime);
    amxc_ts_now(&next_retry);
    next_retry.sec += nextRestartTime;

    // update dm
    cthulhu_init_trans(&trans, restart);
    amxd_trans_set_uint32_t(&trans, CTHULHU_DM_CTR_AR_RETRYCOUNT, retryCount + 1);
    amxd_trans_set_amxc_ts_t(&trans, CTHULHU_DM_CTR_AR_RUNNINGSINCE, &ts_zero);
    amxd_trans_set_amxc_ts_t(&trans, CTHULHU_DM_CTR_AR_NEXTRETRY, &next_retry);
    status = cthulhu_send_trans(&trans);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
    }
    // start the timer
    ctr_priv = cthulhu_ctr_get_priv(ctr_obj);
    if(!ctr_priv->retry_timer) {
        amxp_timer_new(&(ctr_priv->retry_timer), cthulu_retrytimer_elapsed, ctr_obj);
        if(!ctr_priv->retry_timer) {
            SAH_TRACEZ_ERROR(ME, "Could not create retry timer");
            goto exit;
        }
    }
    amxp_timer_start(ctr_priv->retry_timer, nextRestartTime * 1000);
    res = true;
exit:
    free(ctr_id);
    return res;
}

void cthulhu_autorestart_ctr_started(amxd_object_t* ctr_obj) {
    amxd_object_t* restart = amxd_object_get_child(ctr_obj, CTHULHU_CONTAINER_AUTORESTART);
    bool restart_enabled = false;
    amxd_trans_t trans;
    amxc_ts_t now;
    amxc_ts_t ts_zero = {0, 0, 0};
    cthulhu_ctr_priv_t* ctr_priv = NULL;
    amxd_status_t status = amxd_status_ok;
    char* ctr_id = NULL;

    ctr_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_ID, NULL);
    if(!restart) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: No restart data found", ctr_id);
        return;
    }
    restart_enabled = amxd_object_get_bool(restart, CTHULHU_DM_CTR_AR_ENABLED, NULL);
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Container has started. Autorestart is %senabled", ctr_id, restart_enabled ? "" : "not ");

    // stop timer
    ctr_priv = cthulhu_ctr_get_priv(ctr_obj);
    if(ctr_priv->retry_timer) {
        amxp_timer_stop(ctr_priv->retry_timer);
    }
    // reset timer
    if(!ctr_priv->retry_reset_timer) {
        amxp_timer_new(&(ctr_priv->retry_reset_timer), cthulu_retryresettimer_elapsed, ctr_obj);
        if(!ctr_priv->retry_reset_timer) {
            SAH_TRACEZ_ERROR(ME, "Could not create retry reset timer");
            return;
        }
    }
    amxp_timer_start(ctr_priv->retry_reset_timer, 5 * 60 * 1000);

    cthulhu_init_trans(&trans, restart);
    amxc_ts_now(&now);
    amxd_trans_set_amxc_ts_t(&trans, CTHULHU_DM_CTR_AR_RUNNINGSINCE, &now);
    amxd_trans_set_amxc_ts_t(&trans, CTHULHU_DM_CTR_AR_NEXTRETRY, &ts_zero);
    status = cthulhu_send_trans(&trans);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
    }
    free(ctr_id);
}

void cthulhu_autorestart_ctr_stopped(amxd_object_t* ctr_obj) {
    amxd_object_t* restart = amxd_object_get_child(ctr_obj, CTHULHU_CONTAINER_AUTORESTART);
    bool restart_enabled = false;
    amxd_trans_t trans;
    amxc_ts_t ts_zero = {0, 0, 0};
    amxd_status_t status = amxd_status_ok;
    char* ctr_id = NULL;
    char* sb_id = NULL;

    if(!restart) {
        SAH_TRACEZ_ERROR(ME, "No restart data found");
        goto exit;
    }
    ctr_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_ID, NULL);
    when_null(ctr_id, exit);

    sb_id = cthulhu_ctr_get_sb_id(ctr_obj);
    if(sb_id && !cthulhu_is_sb_enabled(sb_id)) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Sandbox [%s] is not up, container will not autorestart", ctr_id, sb_id);
        goto exit;
    }

    restart_enabled = amxd_object_get_bool(restart, CTHULHU_DM_CTR_AR_ENABLED, NULL);
    SAH_TRACEZ_INFO(ME, "CTR[%s]: Container has stopped. Autorestart is %senabled", ctr_id, restart_enabled ? "" : "not ");

    if(restart_enabled) {
        cthulhu_start_retry_timer(ctr_obj);
    } else {
        cthulhu_init_trans(&trans, restart);
        amxd_trans_set_amxc_ts_t(&trans, CTHULHU_DM_CTR_AR_RUNNINGSINCE, &ts_zero);
        amxd_trans_set_amxc_ts_t(&trans, CTHULHU_DM_CTR_AR_NEXTRETRY, &ts_zero);
        status = cthulhu_send_trans(&trans);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
        }
    }
exit:
    free(ctr_id);
    free(sb_id);
}
