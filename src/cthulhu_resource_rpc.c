/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "cthulhu_priv.h"

#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_helpers.h>

#include "cthulhu_priv.h"

#define ME "cthulhu_rpc"

typedef struct _cthulhu_update_data {
    char* sb_id;
    char* param;
    int32_t val;
} cthulhu_update_data_t;

static int cthulhu_update_data_new(cthulhu_update_data_t** update_data,
                                   const char* sb_id,
                                   const char* param,
                                   int32_t val
                                   ) {
    int res = -1;
    when_null(update_data, exit);
    when_null(sb_id, exit);
    when_null(param, exit);
    *update_data = (cthulhu_update_data_t*) calloc(1, sizeof(cthulhu_update_data_t));
    when_null(*update_data, exit);

    (*update_data)->sb_id = strdup(sb_id);
    (*update_data)->param = strdup(param);
    (*update_data)->val = val;

    res = 0;

exit:
    return res;
}

static void cthulhu_update_data_delete(cthulhu_update_data_t** update_data) {
    when_null(update_data, exit);
    when_null(*update_data, exit);

    free((*update_data)->sb_id);
    free((*update_data)->param);

    free(*update_data);
    *update_data = NULL;
exit:
    return;
}

static void cthulhu_stats_fill(amxc_var_t* retval, cthulhu_resource_stats_t* stats) {
    amxc_var_t* var = NULL;

    var = amxc_var_get_key(retval, CTHULHU_STATS_ELEM_TOTAL, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_int64_t(var, stats->total);
    var = amxc_var_get_key(retval, CTHULHU_STATS_ELEM_USED, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_int64_t(var, stats->used);
    var = amxc_var_get_key(retval, CTHULHU_STATS_ELEM_FREE, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_int64_t(var, stats->free);
}

amxd_status_t _sb_stats_read(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    cthulhu_resource_stats_t cthulhu_stats;
    amxd_object_t* sb_obj = NULL;
    char* sb_id = NULL;

    cthulhu_stats.total = -1;
    cthulhu_stats.free = -1;
    cthulhu_stats.used = -1;

    status = amxd_action_object_read(object, param, reason, args, retval, priv);
    when_true(status != amxd_status_ok && status != amxd_status_parameter_not_found, exit);

    sb_obj = amxd_object_get_parent(amxd_object_get_parent(object));
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "Parent object is NULL");
        goto exit;
    }
    if(cthulhu_sb_get_status_real(sb_obj) == cthulhu_sb_status_up) {
        sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_ID, NULL);

        const char* object_name = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
        if(strcmp(object_name, CTHULHU_STATS_DISKSPACE) == 0) {
            cthulhu_sb_diskspace_resources_get(sb_id, &cthulhu_stats);
        } else if(strcmp(object_name, CTHULHU_STATS_MEMORY) == 0) {
            cthulhu_cgroup_memory_resources_get(sb_id, &cthulhu_stats);
        } else {
            SAH_TRACEZ_ERROR(ME, "Unknown stat [%s] ", object_name);
        }
    }
    cthulhu_stats_fill(retval, &cthulhu_stats);
exit:
    free(sb_id);
    return status;
}

amxd_status_t _ctr_stats_read(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* sb_obj = NULL;
    amxd_object_t* stat_obj = NULL;
    amxd_object_t* resource_obj = NULL;
    char* priv_sb_id = NULL;

    ctr_obj = amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(object)));
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "Parent object is NULL");
        goto exit;
    }
    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    if(string_is_empty(priv_sb_id)) {
        goto exit;
    }
    sb_obj = cthulhu_sb_get(priv_sb_id);
    stat_obj = amxd_object_get_child(sb_obj, CTHULHU_STATS);
    resource_obj = amxd_object_get_child(stat_obj, amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    status = _sb_stats_read(resource_obj, param, reason, args, retval, priv);

exit:
    free(priv_sb_id);
    return status;
}

amxd_status_t _ctr_resource_read(amxd_object_t* object,
                                 amxd_param_t* param,
                                 UNUSED amxd_action_t reason,
                                 UNUSED const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* sb_obj = NULL;
    char* priv_sb_id = NULL;

    ctr_obj = amxd_object_get_parent(object);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "Parent object is NULL");
        amxc_var_set(int32_t, retval, -1);
        goto exit;
    }
    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    if(string_is_empty(priv_sb_id)) {
        amxc_var_set(int32_t, retval, -1);
        goto exit;
    }
    sb_obj = cthulhu_sb_get(priv_sb_id);
    if(!sb_obj) {
        amxc_var_set(int32_t, retval, -1);
        goto exit;
    }
    amxc_var_set(int32_t, retval, amxd_object_get_int32_t(sb_obj, param->name, NULL));

    status = amxd_status_ok;
exit:
    free(priv_sb_id);
    return status;
}

static void update_timer_cb(amxp_timer_t* timer, void* priv) {
    cthulhu_update_data_t* update_data = (cthulhu_update_data_t*) priv;
    amxd_object_t* sb_obj = NULL;

    when_null(update_data, exit);
    sb_obj = cthulhu_sb_get(update_data->sb_id);
    when_null(sb_obj, exit);

    amxd_object_set_int32_t(sb_obj, update_data->param, update_data->val);

exit:
    cthulhu_update_data_delete(&update_data);
    amxp_timer_delete(&timer);
}

amxd_status_t _ctr_resource_write(UNUSED amxd_object_t* object,
                                  UNUSED amxd_param_t* param,
                                  UNUSED amxd_action_t reason,
                                  UNUSED const amxc_var_t* const args,
                                  UNUSED amxc_var_t* const retval,
                                  UNUSED void* priv) {

    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* ctr_obj = NULL;
    char* priv_sb_id = NULL;
    cthulhu_update_data_t* update_data = NULL;

    ctr_obj = amxd_object_get_parent(object);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "Parent object is NULL");
        goto exit;
    }
    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    if(string_is_empty(priv_sb_id)) {
        goto exit;
    }

    if(cthulhu_update_data_new(&update_data, priv_sb_id, param->name, amxc_var_get_const_int32_t(args)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not set update data");
        goto exit;
    }

    amxp_timer_t* update_timer = NULL;
    amxp_timer_new(&update_timer, update_timer_cb, update_data);
    amxp_timer_start(update_timer, 0);

    status = amxd_status_ok;
exit:
    return status;
}

amxd_status_t _Sandbox_modify(UNUSED amxd_object_t* obj,
                              UNUSED amxd_function_t* func,
                              UNUSED amxc_var_t* args,
                              amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    cthulhu_notif_data_t* notif_data = NULL;
    amxd_object_t* sb_obj = NULL;
    amxd_trans_t trans;

    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    const char* sb_id = GET_CHAR(args, CTHULHU_SANDBOX_ID);
    amxc_var_t* var = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_sb_modify, command_id);

    // check if there is already an Sandbox with this sb_id
    sb_obj = cthulhu_sb_get(sb_id);
    if(sb_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_unknown_exec_env, CTHULHU_SANDBOX " %s does not exist", sb_id);
        goto exit;
    }

    cthulhu_init_trans(&trans, sb_obj);
    if((var = GET_ARG(args, CTHULHU_SANDBOX_CPU)) != NULL) {
        amxd_trans_set_value(int32_t, &trans, CTHULHU_SANDBOX_CPU, amxc_var_dyncast(int32_t, var));
    }
    if((var = GET_ARG(args, CTHULHU_SANDBOX_MEMORY)) != NULL) {
        amxd_trans_set_value(int32_t, &trans, CTHULHU_SANDBOX_MEMORY, amxc_var_dyncast(int32_t, var));
    }
    if((var = GET_ARG(args, CTHULHU_SANDBOX_DISKSPACE)) != NULL) {
        amxd_trans_set_value(int32_t, &trans, CTHULHU_SANDBOX_DISKSPACE, amxc_var_dyncast(int32_t, var));
    }
    status = cthulhu_send_trans(&trans);
    if(status != amxd_status_ok) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Transaction failed. Status: %d", status);
        goto exit;
    }

exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

amxd_status_t _Container_modify(UNUSED amxd_object_t* obj,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    cthulhu_notif_data_t* notif_data = NULL;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* res_obj = NULL;
    amxd_trans_t trans;

    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    amxc_var_t* var = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_sb_modify, command_id);

    // check if there is already an Sandbox with this sb_id
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }

    res_obj = amxd_object_get_child(ctr_obj, CTHULHU_CONTAINER_RESOURCES);
    if(res_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Could not get resources");
        goto exit;
    }

    cthulhu_init_trans(&trans, res_obj);
    if((var = GET_ARG(args, CTHULHU_CONTAINER_RESOURCES_CPU)) != NULL) {
        amxd_trans_set_value(int32_t, &trans, CTHULHU_CONTAINER_RESOURCES_CPU, amxc_var_dyncast(int32_t, var));
    }
    if((var = GET_ARG(args, CTHULHU_CONTAINER_RESOURCES_MEMORY)) != NULL) {
        amxd_trans_set_value(int32_t, &trans, CTHULHU_CONTAINER_RESOURCES_MEMORY, amxc_var_dyncast(int32_t, var));
    }
    if((var = GET_ARG(args, CTHULHU_CONTAINER_RESOURCES_DISKSPACE)) != NULL) {
        amxd_trans_set_value(int32_t, &trans, CTHULHU_CONTAINER_RESOURCES_DISKSPACE, amxc_var_dyncast(int32_t, var));
    }
    status = cthulhu_send_trans(&trans);
    if(status != amxd_status_ok) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Transaction failed. Status: %d", status);
        goto exit;
    }

exit:
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}
