/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>

#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_error.h>

#include "cthulhu_priv.h"
#include "cthulhu_notif.h"
#include <amxd/amxd_object.h>

#define ME "cthulhu_notif"

static void fill_notif_variant_from_dm_obj(amxc_var_t* var, amxd_object_t* object) {
    switch(amxd_object_get_type(object)) {
    case amxd_object_template:
        // if template object only print instances
        amxd_object_for_each(instance, it, object) {
            amxd_object_t* subobject = amxc_container_of(it, amxd_object_t, it);
            const char* subobjname = amxd_object_get_name(subobject, AMXD_OBJECT_NAMED);
            amxc_var_t* value = amxc_var_add_key(amxc_htable_t, var, subobjname, NULL);
            if(value) {
                fill_notif_variant_from_dm_obj(value, subobject);
            } else {
                SAH_TRACEZ_ERROR(ME, "Cannot add subobject value of '%s'", subobjname);
            }
        }
        break;
    case amxd_object_instance:
    default:
        amxd_object_for_each(parameter, it, object) {
            amxd_param_t* param = amxc_container_of(it, amxd_param_t, it);
            const char* paramname = amxd_param_get_name(param);
            amxc_var_t* value;
            amxc_var_new(&value);
            if(amxd_param_get_value(param, value) == amxd_status_ok) {
                if(amxc_var_set_key(var, paramname, value, AMXC_VAR_FLAG_DEFAULT) != amxd_status_ok) {
                    amxc_var_delete(&value);
                }
            } else {
                SAH_TRACEZ_ERROR(ME, "Cannot get param value of '%s'", paramname);
                amxc_var_delete(&value);
            }
        }
        amxd_object_for_each(child, it, object) {
            amxd_object_t* subobject = amxc_container_of(it, amxd_object_t, it);
            const char* subobjname = amxd_object_get_name(subobject, AMXD_OBJECT_NAMED);
            amxc_var_t* value = amxc_var_add_key(amxc_htable_t, var, subobjname, NULL);
            if(value) {
                fill_notif_variant_from_dm_obj(value, subobject);
            } else {
                SAH_TRACEZ_ERROR(ME, "Cannot add subobject value of '%s'", subobjname);
            }
        }
        break;
    }

}

static inline amxc_var_t* amxc_var_add_new_key_cstring_from_object(amxc_var_t* var,
                                                                   const char* var_key,
                                                                   amxd_object_t* obj,
                                                                   const char* obj_key) {
    amxc_var_t* subvar = NULL;

    when_null(var, exit);
    subvar = amxc_var_add_new_key(var, var_key);
    when_null(subvar, exit);

    if(amxc_var_push(cstring_t, subvar, amxd_object_get_cstring_t(obj, obj_key, NULL)) != 0) {
        amxc_var_delete(&subvar);
    }

exit:
    return subvar;

}

static void cthulhu_notif(cthulhu_notif_data_t* notif_data,
                          const char* event,
                          amxc_var_t* data_changed) {
    amxc_var_t notif;
    amxc_var_t* params = NULL;

    when_null(notif_data, exit);
    when_null(notif_data->obj, exit);

    amxc_var_init(&notif);
    amxc_var_set_type(&notif, AMXC_VAR_ID_HTABLE);

    // Add command id
    if(!string_is_empty(notif_data->command_id)) {
        amxc_var_add_key(cstring_t, &notif, CTHULHU_NOTIF_COMMAND_ID, notif_data->command_id);
    }

    // Add full dm obj
    params = amxc_var_add_key(amxc_htable_t, &notif, CTHULHU_NOTIF_DM_OBJ_FULL, NULL);
    if(params) {
        fill_notif_variant_from_dm_obj(params, notif_data->obj);
    }

    //
    if(data_changed) {
        amxc_var_set_key(&notif, CTHULHU_NOTIF_DM_OBJ_CHANGED, data_changed, AMXC_VAR_FLAG_COPY);
    }

    amxd_object_emit_signal(notif_data->obj, event, &notif);

exit:
    amxc_var_clean(&notif);
}

void cthulhu_notif_ctr_created(cthulhu_notif_data_t* notif_data) {
    cthulhu_notif(notif_data, CTHULHU_NOTIF_CTR_CREATED, NULL);
}

void cthulhu_notif_ctr_updated(cthulhu_notif_data_t* notif_data, amxc_var_t* data_changed) {
    cthulhu_notif(notif_data, CTHULHU_NOTIF_CTR_UPDATED, data_changed);
}

void cthulhu_notif_ctr_removed(cthulhu_notif_data_t* notif_data) {
    cthulhu_notif(notif_data, CTHULHU_NOTIF_CTR_REMOVED, NULL);
}

void cthulhu_notif_sb_list(cthulhu_notif_data_t* notif_data, bool quiet) {
    amxc_var_t notif;
    amxd_object_t* sb_obj = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* sb_list = NULL;

    when_null(notif_data, exit);
    when_null(notif_data->obj, exit);

    amxc_var_init(&notif);
    amxc_var_set_type(&notif, AMXC_VAR_ID_HTABLE);

    sb_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES);
    if(sb_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_SANDBOX_INSTANCES);
        goto exit;
    }

    // Add command id
    if(!string_is_empty(notif_data->command_id)) {
        amxc_var_add_key(cstring_t, &notif, CTHULHU_NOTIF_COMMAND_ID, notif_data->command_id);
    }

    params = amxc_var_add_key(amxc_htable_t, &notif, CTHULHU_NOTIF_DM_OBJ_FULL, NULL);
    sb_list = amxc_var_add_key(amxc_llist_t, params, CTHULHU_DM_SANDBOX_INSTANCES, NULL);
    amxd_object_for_each(instance, it, sb_obj) {
        amxc_var_t* sb_data = NULL;
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        if(instance == NULL) {
            SAH_TRACEZ_ERROR(ME, "Could not get instance");
            continue;
        }
        sb_data = amxc_var_add(amxc_htable_t, sb_list, NULL);
        amxc_var_add_new_key_cstring_from_object(sb_data, CTHULHU_SANDBOX_ID, instance, CTHULHU_SANDBOX_ID);
        if(!quiet) {
            fill_notif_variant_from_dm_obj(sb_data, instance);
        }
    }

    amxd_object_emit_signal(notif_data->obj, CTHULHU_NOTIF_SB_LIST, &notif);

exit:
    amxc_var_clean(&notif);
}

void cthulhu_notif_ctr_list(cthulhu_notif_data_t* notif_data, bool quiet) {
    amxc_var_t notif;
    amxd_object_t* ctr_obj = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* ctr_list = NULL;

    when_null(notif_data, exit);
    when_null(notif_data->obj, exit);

    amxc_var_init(&notif);
    amxc_var_set_type(&notif, AMXC_VAR_ID_HTABLE);

    ctr_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    if(ctr_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_CONTAINER_INSTANCES);
        goto exit;
    }

    // Add command id
    if(!string_is_empty(notif_data->command_id)) {
        amxc_var_add_key(cstring_t, &notif, CTHULHU_NOTIF_COMMAND_ID, notif_data->command_id);
    }

    params = amxc_var_add_key(amxc_htable_t, &notif, CTHULHU_NOTIF_DM_OBJ_FULL, NULL);
    ctr_list = amxc_var_add_key(amxc_llist_t, params, CTHULHU_DM_CONTAINER_INSTANCES, NULL);
    amxd_object_for_each(instance, it, ctr_obj) {
        amxc_var_t* ctr_data = NULL;
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        if(instance == NULL) {
            SAH_TRACEZ_ERROR(ME, "Could not get instance");
            continue;
        }
        ctr_data = amxc_var_add(amxc_htable_t, ctr_list, NULL);
        amxc_var_add_new_key_cstring_from_object(ctr_data, CTHULHU_CONTAINER_ID, instance, CTHULHU_CONTAINER_ID);
        if(!quiet) {
            fill_notif_variant_from_dm_obj(ctr_data, instance);
            amxc_var_add_key(int32_t, ctr_data, CTHULHU_CONTAINER_PID, amxd_object_get_value(int32_t, instance, CTHULHU_CONTAINER_PID, NULL));
        }
    }

    amxd_object_emit_signal(notif_data->obj, CTHULHU_NOTIF_CTR_LIST, &notif);

exit:
    amxc_var_clean(&notif);
}

void cthulhu_notif_ctr_ps(cthulhu_notif_data_t* notif_data) {
    cthulhu_notif(notif_data, CTHULHU_NOTIF_CTR_PS, NULL);
}

void cthulhu_notif_sb_created(cthulhu_notif_data_t* notif_data) {
    cthulhu_notif(notif_data, CTHULHU_NOTIF_SB_CREATED, NULL);
}

void cthulhu_notif_sb_removed(cthulhu_notif_data_t* notif_data) {
    cthulhu_notif(notif_data, CTHULHU_NOTIF_SB_REMOVED, NULL);
}

void cthulhu_notif_sb_updated(cthulhu_notif_data_t* notif_data, amxc_var_t* data_changed) {
    cthulhu_notif(notif_data, CTHULHU_NOTIF_SB_UPDATED, data_changed);
}

static const char* cthulhu_notif_cmd_names[] = {
    "none",                  // cthulhu_notif_cmd_none = 0,
    CTHULHU_CMD_CTR_CREATE,  // cthulhu_notif_cmd_ctr_create,
    CTHULHU_CMD_CTR_START,   // cthulhu_notif_cmd_ctr_start,
    CTHULHU_CMD_CTR_STOP,    // cthulhu_notif_cmd_ctr_stop,
    CTHULHU_CMD_CTR_RESTART, // cthulhu_notif_cmd_ctr_restart,
    CTHULHU_CMD_CTR_REMOVE,  // cthulhu_notif_cmd_ctr_remove,
    CTHULHU_CMD_CTR_UPDATE,  // cthulhu_notif_cmd_ctr_update,
    CTHULHU_CMD_CTR_LS,      // cthulhu_notif_cmd_ctr_list,
    CTHULHU_CMD_CTR_MODIFY,  // cthulhu_notif_cmd_ctr_modify,
    CTHULHU_CMD_SB_CREATE,   // cthulhu_notif_cmd_sb_create,
    CTHULHU_CMD_SB_START,    // cthulhu_notif_cmd_sb_start,
    CTHULHU_CMD_SB_STOP,     // cthulhu_notif_cmd_sb_stop,
    CTHULHU_CMD_SB_RESTART,  //cthulhu_notif_cmd_sb_restart,
    CTHULHU_CMD_SB_REMOVE,   //cthulhu_notif_cmd_sb_remove,
    CTHULHU_CMD_SB_MODIFY,   //cthulhu_notif_cmd_sb_modify,
    CTHULHU_CMD_SB_LS,       //cthulhu_notif_cmd_sb_ls,

    // cthulhu_notif_cmd_last
};

static const char* cthulhu_notif_cmd_string(cthulhu_notif_cmd_t cmd) {
    if((cmd >= 0) && (cmd < cthulhu_notif_cmd_last)) {
        return cthulhu_notif_cmd_names[cmd];
    } else {
        return NULL;
    }
}

void cthulhu_notif_error(cthulhu_notif_data_t* notif_data,
                         tr181_fault_type_t error,
                         const char* reason,
                         ...) {
    amxc_var_t notif;
    va_list args;
    const char* cmd_name = NULL;

    when_null(notif_data, exit);
    when_null(notif_data->obj, exit);

    amxc_var_init(&notif);

    amxc_var_set_type(&notif, AMXC_VAR_ID_HTABLE);
    if(!string_is_empty(notif_data->command_id)) {
        amxc_var_add_key(cstring_t, &notif, CTHULHU_NOTIF_COMMAND_ID, notif_data->command_id);
    }

    cmd_name = cthulhu_notif_cmd_string(notif_data->command);

    if(cmd_name || !string_is_empty(reason)) {
        amxc_var_t* info = amxc_var_add_key(amxc_htable_t, &notif, CTHULHU_NOTIF_INFORMATION, NULL);
        if(cmd_name) {
            amxc_var_add_key(cstring_t, info, CTHULHU_CMD_COMMAND, cmd_name);
        }
        if(!string_is_empty(reason)) {
            amxc_string_t r;
            amxc_string_init(&r, 0);
            va_start(args, reason);
            amxc_string_vsetf(&r, reason, args);
            va_end(args);
            amxc_var_add_key(cstring_t, info, CTHULHU_NOTIF_REASON, amxc_string_get(&r, 0));
            amxc_string_clean(&r);
        }
        amxc_var_add_key(int32_t, info, CTHULHU_NOTIF_ERROR_TYPE, error);
        amxc_var_add_key(cstring_t, info, CTHULHU_NOTIF_ERROR_TYPESTR, tr181_fault_type_to_string(error));
    }

    amxd_object_emit_signal(notif_data->obj, CTHULHU_NOTIF_ERROR, &notif);

exit:
    amxc_var_clean(&notif);
}

int cthulhu_notif_data_new(cthulhu_notif_data_t** notif_data,
                           amxd_object_t* obj,
                           cthulhu_notif_cmd_t command,
                           const char* command_id
                           ) {
    int res = -1;
    when_null(notif_data, exit);
    *notif_data = (cthulhu_notif_data_t*) calloc(1, sizeof(cthulhu_notif_data_t));
    when_null(*notif_data, exit);
    (*notif_data)->obj = obj;

    (*notif_data)->command = command;
    if(command_id) {
        (*notif_data)->command_id = strdup(command_id);
    }

    res = 0;

exit:
    return res;
}
void cthulhu_notif_data_delete(cthulhu_notif_data_t** notif_data) {
    when_null(notif_data, exit);
    when_null(*notif_data, exit);

    free((*notif_data)->command_id);

    free(*notif_data);
    *notif_data = NULL;
exit:
    return;
}

cthulhu_notif_data_t* cthulhu_notif_data_no_id(cthulhu_notif_data_t* notif_data) {
    cthulhu_notif_data_t* new_notif_data = NULL;
    when_null(notif_data, exit);
    cthulhu_notif_data_new(&new_notif_data, notif_data->obj, notif_data->command, NULL);
exit:
    return new_notif_data;
}

cthulhu_notif_data_t* cthulhu_notif_data_no_obj(cthulhu_notif_data_t* notif_data) {
    cthulhu_notif_data_t* new_notif_data = NULL;
    when_null(notif_data, exit);
    cthulhu_notif_data_new(&new_notif_data, NULL, notif_data->command, notif_data->command_id);
exit:
    return new_notif_data;
}
