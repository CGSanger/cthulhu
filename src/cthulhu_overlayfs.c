/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <linux/version.h>
#include <sys/mount.h>
#include <dirent.h>

#include "cthulhu_priv.h"
#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_helpers.h>


#include "cthulhu_overlayfs.h"

#define ME "cthulhu_ovl"

static bool ovl_supported = false;
static char* ovl_module = NULL;

static int cthulhu_overlayfs_unmount(const char* target);

void cthulhu_overlayfs_init(void) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 0, 0)
    // since kernel 4.0.0 it is possible to have several lower layers
    SAH_TRACEZ_NOTICE(ME, "OverlayFS is not enabled on this kernel");
    return;
#endif

    // check the config flag
    amxd_object_t* config = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONFIG);
    if(config) {
        bool use_overlayfs = amxd_object_get_bool(config, CTHULHU_CONFIG_USEOVERLAYFS, NULL);
        if(!use_overlayfs) {
            SAH_TRACEZ_NOTICE(ME, "OverlayFS is disabled in the config");
            return;
        }
    }

    char lower_template[] = "/tmp/ovll_XXXXXX";
    char upper_template[] = "/tmp/ovlu_XXXXXX";
    char merged_template[] = "/tmp/ovlm_XXXXXX";
    char work_template[] = "/tmp/ovlw_XXXXXX";
    char* lower = NULL;
    char* upper = NULL;
    char* work = NULL;
    char* merged = NULL;
    char* options = NULL;
    int res;

    // create two tmp directories, and try to create an overlayfs from it
    lower = mkdtemp(lower_template);
    if(lower == NULL) {
        SAH_TRACEZ_WARNING(ME, "Could not create tmp directory");
        goto exit;
    }
    upper = mkdtemp(upper_template);
    if(upper == NULL) {
        SAH_TRACEZ_WARNING(ME, "Could not create tmp directory");
        goto exit;
    }
    work = mkdtemp(work_template);
    if(work == NULL) {
        SAH_TRACEZ_WARNING(ME, "Could not create tmp directory");
        goto exit;
    }
    merged = mkdtemp(merged_template);
    if(merged == NULL) {
        SAH_TRACEZ_WARNING(ME, "Could not create tmp directory");
        goto exit;
    }
    res = asprintf(&options,
                   "lowerdir=%s,upperdir=%s,workdir=%s", lower,
                   upper, work);
    if(res <= 0) {
        SAH_TRACEZ_WARNING(ME, "Could not create options string");
        goto exit;
    }
    res = mount(lower, merged, "overlay", 0, options);
    if(res == 0) {
        ovl_module = strdup("overlay");
    }
    if((res < 0) && (errno == ENODEV)) { /* Try other module name. */
        res = mount(lower, merged, "overlayfs", 0, options);
        if(res == 0) {
            ovl_module = strdup("overlayfs");
        }
    }
    if(res < 0) {
        SAH_TRACEZ_ERROR(ME, "Overlayfs is not supported (%d: %s)", errno, strerror(errno));
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "Overlayfs is supported (modulename: %s)", ovl_module);
    ovl_supported = true;
    // unmount the test mount
    cthulhu_overlayfs_unmount(merged);

exit:
    if(lower) {
        rmdir(lower);
    }
    if(upper) {
        rmdir(upper);
    }
    if(work) {
        cthulhu_rmdir(work);
    }
    if(merged) {
        rmdir(merged);
    }
    free(options);
}

char* cthulu_overlayfs_get_rootfs(const char* ctr_id) {
    char* rootfs = NULL;
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    if(asprintf(&rootfs, "%s/%s", app_data->rootfs_location, ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate memory to create rootfs string (%d: %s)", errno, strerror(errno));
        return NULL;
    }
    return rootfs;
}

int cthulhu_overlayfs_create_rootfs(const char* ctr_id, const char* data_dir) {
    int res = -1;
    char* rootfs = NULL;
    char* upperdir = NULL;
    char* workingdir = NULL;
    amxc_string_t options;
    amxd_object_t* ctr_obj = cthulhu_ctr_get(ctr_id);
    amxd_object_t* layers = NULL;
    int layer_cnt = 0;
    int i = 0;
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    SAH_TRACEZ_INFO(ME, "Create rootfs for [%s] with data dir [%s]", ctr_id, data_dir);

    amxc_string_init(&options, 255);

    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "No ctr object for [%s]", ctr_id);
        goto exit;
    }
    // create the rootfs
    rootfs = cthulu_overlayfs_get_rootfs(ctr_id);
    if(!rootfs) {
        SAH_TRACEZ_ERROR(ME, "Could not get rootfs string");
        goto exit;
    }
    if(cthulhu_isdir(rootfs)) {
        // try to unmount since maybe it is still mounted
        if(umount(rootfs) == 0) {
            SAH_TRACEZ_INFO(ME, "Rootfs [%s] was still mounted. unmount successful", rootfs);
        }
    } else if(cthulhu_mkdir(rootfs, false) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create rootfs [%s]", rootfs);
        goto exit;
    }
    layers = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_LAYERS);
    if(!layers) {
        SAH_TRACEZ_ERROR(ME, "Could not find layers for container [%s]", ctr_id);
        goto exit;
    }
    layer_cnt = amxd_object_get_instance_count(layers);
    if(layer_cnt < 1) {
        SAH_TRACEZ_ERROR(ME, "Container [%s] does not have layers", ctr_id);
        goto exit;
    }
    for(i = layer_cnt - 1; i >= 0; --i) {
        amxd_object_for_each(instance, it, layers) {
            amxd_object_t* layer = amxc_llist_it_get_data(it, amxd_object_t, it);
            int index = amxd_object_get_int32_t(layer, CTHULHU_CONTAINER_LAYERS_INDEX, NULL);
            if(index == i) {
                char* layer_name = amxd_object_get_cstring_t(layer, CTHULHU_CONTAINER_LAYERS_LAYER, NULL);
                char* p = NULL;
                char* layer_dir = NULL;
                if(!layer_name) {
                    SAH_TRACEZ_ERROR(ME, "The layer name is not defined");
                    goto exit;
                }
                p = strchr(layer_name, ':');
                if(p) {
                    *p = '/';
                }
                if(asprintf(&layer_dir, "%s/%s", app_data->layer_location, layer_name) < 0) {
                    SAH_TRACEZ_ERROR(ME, "Could not create layer_dir string (%d: %s)", errno, strerror(errno));
                    free(layer_name);
                    goto exit;
                }
                free(layer_name);
                if(!cthulhu_isdir(layer_dir)) {
                    SAH_TRACEZ_ERROR(ME, "The layer [%s] does not exist", layer_dir);
                    free(layer_dir);
                    goto exit;
                }
                if(i == layer_cnt - 1) {
                    // first entry
                    amxc_string_setf(&options, "lowerdir=%s", layer_dir);
                } else {
                    amxc_string_appendf(&options, ":%s", layer_dir);
                }
                free(layer_dir);
                break;
            }
        }
    }
    if(data_dir) {
        // make an upper and a working dir, since the working dir needs to be on the same
        // filesystem as the upperdir
        if(!asprintf(&upperdir, "%s/%s/upper", data_dir, ctr_id)) {
            SAH_TRACEZ_ERROR(ME, "Could not create upperdir string (%d: %s)", errno, strerror(errno));
            goto exit;
        }
        if(!asprintf(&workingdir, "%s/%s/work", data_dir, ctr_id)) {
            SAH_TRACEZ_ERROR(ME, "Could not create workingdir string (%d: %s)", errno, strerror(errno));
            goto exit;
        }
        if(cthulhu_mkdir(upperdir, true) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not create [%s]", upperdir);
            goto exit;
        }
        if(cthulhu_mkdir(workingdir, true) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not create [%s]", workingdir);
            goto exit;
        }
        amxc_string_appendf(&options, ",upperdir=%s,workdir=%s", upperdir, workingdir);

    } else if(layer_cnt == 1) {
        // if there is no upperdir, there should be at least two lower layers,
        // so we create a tmp one
        const char* tmp_lowerlayer = "/tmp/cthulhu_lowerlayer";
        if(cthulhu_mkdir(tmp_lowerlayer, false) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not create [%s]", workingdir);
            goto exit;
        }
        amxc_string_appendf(&options, ":%s", tmp_lowerlayer);
    }

    SAH_TRACEZ_INFO(ME, "Create overlayfs for rootfs [%s] Options [%s]", rootfs, options.buffer);
    res = mount("overlay", rootfs, ovl_module, 0, options.buffer);
    if(res < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create overlayfs for rootfs [%s] with options [%s] (%d: %s)",
                         rootfs, options.buffer, errno, strerror(errno));
        goto exit;
    }

    res = 0;
exit:
    free(rootfs);
    free(upperdir);
    free(workingdir);
    amxc_string_clean(&options);
    return res;
}

int cthulhu_overlayfs_umount_rootfs(const char* ctr_id) {
    int res = -1;
    char* rootfs = cthulu_overlayfs_get_rootfs(ctr_id);
    if(!rootfs) {
        SAH_TRACEZ_ERROR(ME, "Could not get rootfs string");
        goto exit;
    }
    if((res = umount(rootfs)) != 0) {
        if(errno == EINVAL) {
            SAH_TRACEZ_INFO(ME, "Rootfs [%s] was not mounted", rootfs);
            goto exit;
        }
        SAH_TRACEZ_ERROR(ME, "Rootfs [%s] could not be unmounted (%d: %s)", rootfs, errno, strerror(errno));
    }
exit:
    free(rootfs);
    return res;
}

static int cthulhu_overlayfs_unmount(const char* target) {
    int res = -1;
    if(umount(target) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to unmount [%s] (%d: %s)", target, errno, strerror(errno));
        goto exit;
    }
    res = 0;
exit:
    return res;
}

void cthulhu_overlayfs_cleanup(void) {
#ifdef DISABLE_OVERLAYFS
    return;
#endif
    free(ovl_module);
}

bool cthulhu_overlayfs_supported(void) {
    return ovl_supported;
}

static int cthulhu_layer_is_used_by_ctr(amxd_object_t* ctr_obj, const char* full_layer_name, bool* used) {
    bool res = -1;
    amxd_object_t* layer_templ = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_LAYERS);
    if(!layer_templ) {
        SAH_TRACEZ_ERROR(ME, "Could not get the layers");
        goto exit;
    }
    // go over all layers of a container
    amxd_object_for_each(instance, it_layer, layer_templ) {
        amxd_object_t* layer_obj = amxc_llist_it_get_data(it_layer, amxd_object_t, it);
        char* layer_name = amxd_object_get_cstring_t(layer_obj, CTHULHU_CONTAINER_LAYERS_LAYER, NULL);
        if(strcmp(layer_name, full_layer_name) == 0) {
            res = 0;
            *used = true;
            free(layer_name);
            goto exit;
        }
        free(layer_name);
    }
    res = 0;
exit:
    return res;
}

static int cthulhu_layer_is_used(const char* encoding, const char* hash, bool* used) {
    bool res = -1;
    *used = false;
    char* full_layer_name = NULL;
    if(asprintf(&full_layer_name, "%s:%s", encoding, hash) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate string (%d: %s)", errno, strerror(errno));
        goto exit;
    }

    amxd_object_t* ctr_templ = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    if(ctr_templ == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_CONTAINER_INSTANCES);
        goto exit;
    }
    // go over all containers
    amxd_object_for_each(instance, it_ctr, ctr_templ) {
        amxd_object_t* ctr_obj = amxc_llist_it_get_data(it_ctr, amxd_object_t, it);
        cthulhu_layer_is_used_by_ctr(ctr_obj, full_layer_name, used);
        if(*used) {
            res = 0;
            goto exit;
        }
    }
    res = 0;
exit:
    free(full_layer_name);
    return res;
}

static void cthulhu_remove_unused_layers_encoded(const char* layer_location, const char* encoding) {
    char* encoding_dir_path = NULL;
    DIR* encoding_dir = NULL;
    struct dirent* entry = NULL;

    if(asprintf(&encoding_dir_path, "%s/%s", layer_location, encoding) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate encoding_dir string (%d: %s)", errno, strerror(errno));
        goto exit;
    }
    encoding_dir = opendir(encoding_dir_path);
    if(!encoding_dir) {
        SAH_TRACEZ_ERROR(ME, "Could not open dir [%s]", encoding_dir_path);
        goto exit;
    }
    while((entry = readdir(encoding_dir))) {
        if((strcmp(entry->d_name, ".") == 0) ||
           (strcmp(entry->d_name, "..") == 0)) {
            continue;
        }
        if(entry->d_type == DT_DIR) {
            SAH_TRACEZ_INFO(ME, "Check if layer %s:%s is used", encoding, entry->d_name);
            bool used = false;
            if((cthulhu_layer_is_used(encoding, entry->d_name, &used) == 0  ) &&
               !used) {
                char* full_path = NULL;
                if(asprintf(&full_path, "%s/%s/%s", layer_location, encoding, entry->d_name) < 0) {
                    SAH_TRACEZ_ERROR(ME, "Could not allocate string (%d: %s)", errno, strerror(errno));
                    goto exit;
                }
                SAH_TRACEZ_INFO(ME, "Remove layer %s", full_path);
                cthulhu_rmdir(full_path);
                free(full_path);
            }
        }
    }
exit:
    free(encoding_dir_path);
    if(encoding_dir) {
        closedir(encoding_dir);
    }
}

void cthulhu_remove_unused_layers(void) {
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    DIR* layers_dir = opendir(app_data->layer_location);
    struct dirent* entry = NULL;
    if(!layers_dir) {
        SAH_TRACEZ_ERROR(ME, "Could not open [%s] (%d: %s)", app_data->layer_location, errno, strerror(errno));
        goto exit;
    }

    while((entry = readdir(layers_dir))) {
        if((strcmp(entry->d_name, ".") == 0) ||
           (strcmp(entry->d_name, "..") == 0)) {
            continue;
        }
        if(entry->d_type == DT_DIR) {
            cthulhu_remove_unused_layers_encoded(app_data->layer_location, entry->d_name);
        }
    }
exit:
    if(layers_dir) {
        closedir(layers_dir);
    }
}

int cthulhu_overlayfs_remove_data_dir(const char* ctr_id, const char* data_dir) {
    int res = -1;
    char* ctr_dir = NULL;
    if(!data_dir) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Datadir empty", ctr_id);
        goto exit;
    }
    if(asprintf(&ctr_dir, "%s/%s", data_dir, ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could not allocate string", ctr_id);
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "CTR[%s]: remove data dir [%s]", ctr_id, ctr_dir);
    if((res = cthulhu_rmdir(ctr_dir)) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Could rm dir [%s]", ctr_id, ctr_dir);
        goto exit;
    }
exit:
    free(ctr_dir);
    return res;
}