/****************************************************************************
**
** Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <sys/statvfs.h>
#include <inttypes.h>
#include <dirent.h>

#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_helpers.h>

#include "cthulhu_priv.h"

#define ME "cthulhu_storage"
#define FSTYPE "ext2"

static const char* cthulhu_storage_build_data_location(amxd_object_t* sb_obj) {
    const char* data_location = NULL;
    char* parent_sb_id = NULL;
    cthulhu_app_t* app_data = cthulhu_get_app_data();

    parent_sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_PARENT, NULL);
    if(!string_is_empty(parent_sb_id)) {
        data_location = cthulhu_storage_get_dir(parent_sb_id);
    } else {
        data_location = app_data->data_location;
    }
    free(parent_sb_id);
    return data_location;
}

static char* cthulhu_storage_build_mountpoint(const char* sb_id, const char* data_location) {
    char* mountpoint = NULL;
    when_null(sb_id, exit);
    when_null(data_location, exit);

    if(asprintf(&mountpoint, "%s/mounts/%s", data_location, sb_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create string (%d: %s)", errno, strerror(errno));
        goto exit;
    }
exit:
    return mountpoint;
}

static char* cthulhu_storage_build_images_location(const char* data_location) {
    char* images_location = NULL;

    if(asprintf(&images_location, "%s/images", data_location) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create string (%d: %s)", errno, strerror(errno));
        goto exit;
    }
exit:
    return images_location;

}

static char* cthulhu_storage_build_image_file_name(const char* sb_id, const char* images_location) {
    char* image_file_name = NULL;

    if(asprintf(&image_file_name, "%s/%s", images_location, sb_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create string (%d: %s)", errno, strerror(errno));
        goto exit;
    }
exit:
    return image_file_name;

}

static int cthulhu_fallocate(const char* filename, off_t length) {
    int res = -1;
    off_t written = 0;
    int fd = 0;
    char block[1024]; // write in blocks of 1k

    memset(block, 0, sizeof(block));
    fd = creat(filename, 0755);
    if(!fd) {
        SAH_TRACEZ_ERROR(ME, "creat failed [%s] (%d: %s)", filename, errno, strerror(errno));
        goto exit;
    }
    for(; written < length; written += sizeof(block)) {
        if(write(fd, block, sizeof(block)) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to create file of %lu, only %lu could be created [%s](%d: %s)", length, written, filename, errno, strerror(errno));
            goto error;
        }
    }
    SAH_TRACEZ_INFO(ME, "File of size %jd created [%s]", (intmax_t) written, filename);
    res = 0;
    goto exit;
error:
    if(fd) {
        close(fd);
    }
    unlink(filename);
    return res;
exit:
    if(fd) {
        close(fd);
    }
    return res;
}

static int cthulhu_storage_create_image(const char* image_file_name, int size_in_bytes) {
    int res = -1;
    char* mkfs_cmd = NULL;
    int cmd_status = -1;

    if(cthulhu_fallocate(image_file_name, size_in_bytes) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create storage image [%s] (%d: %s)", image_file_name, errno, strerror(errno));
        goto exit;
    }
    // on some targets (ie mkfs.ext2 provided by busybox), we need to execute
    // mkfs.ext2 -E nodiscard -F <filename>
    // -F is needed for busybox mkfs.ext2 to override a check if the target is a block device
    // -E nodiscard is needed for non-busybox mkfs.ext2 so the file is not made sparse (space is really allocated on disk)
    if(asprintf(&mkfs_cmd, "mkfs." FSTYPE " -E nodiscard -F %s", image_file_name) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create string (%d: %s)", errno, strerror(errno));
        goto exit;
    }
    if((cthulhu_proc_wait(mkfs_cmd, &cmd_status) < 0) || (cmd_status != 0)) {
        SAH_TRACEZ_ERROR(ME, "Could not create fs [%s] (%d)", mkfs_cmd, cmd_status);
        goto exit;
    }
    res = 0;
exit:

    free(mkfs_cmd);
    return res;
}

static int cthulhu_storage_mount_image(const char* image_file_name, const char* mountpoint) {
    int res = -1;
    char* mount_cmd = NULL;
    int cmd_status = -1;

    if(asprintf(&mount_cmd, "mount -t " FSTYPE " -o loop %s %s", image_file_name, mountpoint) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create string (%d: %s)", errno, strerror(errno));
        goto exit;
    }
    if((cthulhu_proc_wait(mount_cmd, &cmd_status) < 0) || (cmd_status != 0)) {
        SAH_TRACEZ_ERROR(ME, "Could not create fs [%s] (%d)", mount_cmd, cmd_status);
        goto exit;
    }

    res = 0;
exit:
    free(mount_cmd);
    return res;
}

/**
 * @brief Mount the storage file of a sandbox
 * if the storage file does not exist yet, a new one will be created
 *
 * @param sb_id
 * @param size_in_bytes the size of the storage if a new one should be made
 * @return int
 */
int cthulhu_storage_mount(const char* sb_id, int size_in_bytes) {
    int res = -1;
    amxd_object_t* sb_obj = cthulhu_sb_get(sb_id);
    cthulhu_sb_priv_t* sb_priv = NULL;
    const char* data_location = NULL;
    char* images_location = NULL;
    char* image_file_name = NULL;
    char* parent_sb_id = NULL;
    char* mountpoint = NULL;

    SAH_TRACEZ_INFO(ME, "Mount storage for sandbox [%s] size %d", sb_id, size_in_bytes);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] does not exit", sb_id);
        goto exit;
    }
    sb_priv = cthulhu_sb_get_priv(sb_obj);
    if(!sb_priv) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] does not have private data", sb_id);
        goto exit;
    }
    data_location = cthulhu_storage_build_data_location(sb_obj);
    if(!data_location) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not define the data location", sb_id);
        goto exit;
    }
    mountpoint = cthulhu_storage_build_mountpoint(sb_id, data_location);
    if(!mountpoint) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not define the mountpoint", sb_id);
        goto exit;
    }
    if(!cthulhu_isdir(mountpoint)) {
        if((res = cthulhu_mkdir(mountpoint, true)) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to create directory [%s]", mountpoint);
            goto exit;
        }
    }
    // if size < 0, then the sandbox has no storage limitation
    // so no image needs to be mounted
    if(size_in_bytes >= 0) {
        images_location = cthulhu_storage_build_images_location(data_location);
        if(!cthulhu_isdir(images_location)) {
            if(cthulhu_mkdir(images_location, false) < 0) {
                SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not create dir [%s]", sb_id, images_location);
                goto exit;
            }
        }
        image_file_name = cthulhu_storage_build_image_file_name(sb_id, images_location);

        if(!cthulhu_isfile(image_file_name)) {
            SAH_TRACEZ_INFO(ME, "Create new image file %s", image_file_name);
            if((res = cthulhu_storage_create_image(image_file_name, size_in_bytes)) < 0) {
                SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not create storage image [%s]", sb_id, image_file_name);
                goto exit;
            }
        }

        if((res = cthulhu_storage_mount_image(image_file_name, mountpoint)) < 0) {
            SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not mount image [%s]", sb_id, image_file_name);
            goto exit;
        }
    }
    sb_priv->storage_imagefile = image_file_name;
    sb_priv->storage_mountpoint = mountpoint;
    image_file_name = NULL;
    mountpoint = NULL;
    res = 0;
exit:
    free(parent_sb_id);
    free(image_file_name);
    free(images_location);
    free(mountpoint);
    return res;
}

int cthulhu_storage_umount(const char* sb_id) {
    int res = -1;
    amxd_object_t* sb_obj = cthulhu_sb_get(sb_id);
    cthulhu_sb_priv_t* sb_priv = NULL;

    SAH_TRACEZ_INFO(ME, "Umount storage for sandbox [%s]", sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] does not exit", sb_id);
        goto exit;
    }
    sb_priv = cthulhu_sb_get_priv(sb_obj);
    if(!sb_priv) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] does not have private data", sb_id);
        goto exit;
    }
    if(!sb_priv->storage_mountpoint) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: storage_mountpoint is not defined", sb_id);
        goto exit;
    }
    // image filename is empty when the sandbox does not have a diskspace limitation
    // in this case, the storage should not be unmounted
    if(sb_priv->storage_imagefile) {
        SAH_TRACEZ_INFO(ME, "Sandbox [%s]: umount [%s]", sb_id, sb_priv->storage_mountpoint);
        if((res = umount(sb_priv->storage_mountpoint)) != 0) {
            SAH_TRACEZ_INFO(ME, "Sandbox [%s]:  could not umount [%s] (%d: %s)",
                            sb_id, sb_priv->storage_mountpoint, errno, strerror(errno));
            goto exit;
        }
    }
    free(sb_priv->storage_mountpoint);
    sb_priv->storage_mountpoint = NULL;
    free(sb_priv->storage_imagefile);
    sb_priv->storage_imagefile = NULL;
    res = 0;

exit:
    return res;
}

int cthulhu_storage_remove(const char* sb_id) {
    int res = -1;
    amxd_object_t* sb_obj = cthulhu_sb_get(sb_id);
    const char* data_location = NULL;
    char* mountpoint = NULL;
    char* images_location = NULL;
    char* image_file_name = NULL;

    SAH_TRACEZ_INFO(ME, "Remove storage for sandbox [%s]", sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] does not exit", sb_id);
        goto exit;
    }

    data_location = cthulhu_storage_build_data_location(sb_obj);
    if(!data_location) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not define the data location", sb_id);
        goto exit;
    }
    mountpoint = cthulhu_storage_build_mountpoint(sb_id, data_location);
    if(!mountpoint) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not define the mountpoint", sb_id);
    } else if(cthulhu_isdir(mountpoint)) {
        if((res = cthulhu_rmdir(mountpoint)) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to remove directory [%s]", mountpoint);
            goto exit;
        }
    }
    images_location = cthulhu_storage_build_images_location(data_location);
    if(!images_location) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not define the image location", sb_id);
        goto exit;
    }
    image_file_name = cthulhu_storage_build_image_file_name(sb_id, images_location);

    if(cthulhu_isfile(image_file_name)) {
        if(unlink(image_file_name) < 0) {
            SAH_TRACEZ_ERROR(ME, "Sandbox [%s]:  could not remove image [%s] (%d: %s)",
                             sb_id, image_file_name, errno, strerror(errno));
            goto exit;
        }
    }
    res = 0;
exit:
    free(mountpoint);
    free(images_location);
    free(image_file_name);
    return res;
}

/**
 * @brief Get the storage dir of a sandbox
 * This will return NULL if the sandbox is not started or the storage file
 * is not mounted
 *
 * @param sb_id
 * @return const char*
 */
const char* cthulhu_storage_get_dir(const char* sb_id) {
    amxd_object_t* sb_obj = cthulhu_sb_get(sb_id);
    cthulhu_sb_priv_t* sb_priv = NULL;

    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s] does not exit", sb_id);
        return NULL;
    }
    sb_priv = cthulhu_sb_get_priv(sb_obj);
    if(!sb_priv) {
        return NULL;
    }
    return sb_priv->storage_mountpoint;
}

int cthulhu_sb_diskspace_resources_get(const char* sb_id, cthulhu_resource_stats_t* cthulhu_stats) {
    int res = -1;
    struct statvfs stats;

    when_null(cthulhu_stats, exit);

    const char* storage_dir = cthulhu_storage_get_dir(sb_id);
    when_null(storage_dir, exit);

    if(statvfs(storage_dir, &stats) < 0) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get stats for  %s (%d: %s)",
                         sb_id, storage_dir, errno, strerror(errno));
        goto exit;
    }

    cthulhu_stats->total = (stats.f_blocks * stats.f_frsize) / 1024;
    cthulhu_stats->free = (stats.f_bfree * stats.f_frsize) / 1024;
    cthulhu_stats->used = cthulhu_stats->total - cthulhu_stats->free;

    // SAH_TRACEZ_INFO(ME, "SB[%s]: Disk Total: %" PRIu64 " Kb Used: %" PRIu64 " Kb, Free: %" PRIu64 " Kb",
    //                 sb_id, cthulhu_stats->total, cthulhu_stats->used, cthulhu_stats->free);
    res = 0;
exit:
    return res;
}

/**
 * @brief Remove all the images still present in this sandbox that are not used anymore
 *
 * this can happen if a child sandbox is removed while this sandbox was disabled
 *
 * @param sb_id
 * @return int
 */
int cthulhu_storage_remove_unused_images(const char* sb_id) {
    int res = -1;
    char* mountpoint = NULL;
    char* mount_dir = NULL;
    const char* data_location = NULL;
    const char* storage_dir = NULL;
    char* images_location = NULL;

    DIR* dp = NULL;
    struct dirent* ep = NULL;
    amxd_object_t* sb_obj = cthulhu_sb_get(sb_id);

    when_null(sb_obj, exit);

    data_location = cthulhu_storage_build_data_location(sb_obj);
    if(!data_location) {
        SAH_TRACEZ_ERROR(ME, "Sandbox: could not define the data location");
        goto exit;
    }
    mountpoint = cthulhu_storage_build_mountpoint(sb_id, data_location);
    if(!mountpoint) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not define the mountpoint", sb_id);
    }
    if(asprintf(&mount_dir, "%s/mounts", mountpoint) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create string (%d: %s)", errno, strerror(errno));
        goto exit;
    }
    storage_dir = cthulhu_storage_get_dir(sb_id);
    if(!storage_dir) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not define the storage location", sb_id);
        goto exit;
    }
    images_location = cthulhu_storage_build_images_location(storage_dir);
    if(!images_location) {
        SAH_TRACEZ_ERROR(ME, "Sandbox [%s]: could not define the image location", sb_id);
        goto exit;
    }
    if(!cthulhu_isdir(mount_dir)) {
        res = 0;
        goto exit;
    }
    dp = opendir(mount_dir);
    if(!dp) {
        SAH_TRACEZ_ERROR(ME, "Could not open dir [%s]", mount_dir);
        goto exit;
    }
    while((ep = readdir(dp)) != NULL) {
        if(ep->d_name[0] == '.') {
            continue;
        }
        SAH_TRACEZ_INFO(ME, "Check if sandbox [%s] still exists", ep->d_name);
        if(!cthulhu_sb_get(ep->d_name)) {
            SAH_TRACEZ_INFO(ME, "Remove data for removed sandbox [%s]", ep->d_name);
            char* image_file_name = cthulhu_storage_build_image_file_name(ep->d_name, images_location);
            SAH_TRACEZ_INFO(ME, "Unlink %s", image_file_name);
            if(cthulhu_isfile(image_file_name)) {
                unlink(image_file_name);
            }
            free(image_file_name);
            char* sb_mount_dir = NULL;
            if(asprintf(&sb_mount_dir, "%s/%s", mount_dir, ep->d_name) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not create string (%d: %s)", errno, strerror(errno));
                continue;
            }
            SAH_TRACEZ_INFO(ME, "Remove %s", sb_mount_dir);
            cthulhu_rmdir(sb_mount_dir);
            free(sb_mount_dir);
        }
    }

    res = 0;
exit:
    if(dp) {
        closedir(dp);
    }
    free(images_location);
    free(mountpoint);
    free(mount_dir);
    return res;
}

