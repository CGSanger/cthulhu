/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <sys/mman.h>

#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_helpers.h>
#include <cthulhu/cthulhu_ctr_info.h>

#include "cthulhu_priv.h"

#define ME "cthulhu_sb"

static amxc_htable_t sb_sigmngrs;

static amxp_signal_mngr_t* cthulhu_sb_get_sigmngr(const char* sb_id) {
    return cthulhu_sigmngrs_get(&sb_sigmngrs, sb_id);
}

static cthulhu_action_res_t cthulhu_sb_stop_priv(amxd_object_t* sb_obj);

static const char* cthulhu_sb_status_names[] = {
    "Disabled",
    "Up",
    "Restarting"
};

/**
 * @brief Convert a @ref cthulhu_sb_status_t to a string
 *
 * @param status
 * @return const char*
 */
const char* cthulhu_sb_status_to_string(cthulhu_sb_status_t status) {
    if((status >= 0) && (status < cthulhu_sb_status_last)) {
        return cthulhu_sb_status_names[status];
    } else {
        return NULL;
    }
}

/**
 * @brief convett a string to a @ref cthulhu_sb_status_t
 *
 * @param status_string
 * @return cthulhu_sb_status_t
 */
cthulhu_sb_status_t cthulhu_string_to_sb_status(const char* status_string) {
    int status_index = (int) cthulhu_sb_status_disabled;
    cthulhu_sb_status_t status = cthulhu_sb_status_disabled;
    if(!status_string) {
        return cthulhu_sb_status_disabled;
    }
    while(status < cthulhu_sb_status_last) {
        if(strncmp(cthulhu_sb_status_names[status], status_string,
                   strlen(cthulhu_sb_status_names[status])) == 0) {
            return status;
        }
        status = (cthulhu_sb_status_t)++ status_index;
    }
    return cthulhu_sb_status_disabled;
}

/**
 * @brief delete sb_priv of a sandbox
 *
 * @param sb_obj
 */
void delete_sb_priv(amxd_object_t* sb_obj) {
    cthulhu_sb_priv_t* sb_priv = cthulhu_sb_get_priv(sb_obj);
    if(sb_priv) {
        free(sb_priv->cgroup_dir);
        if(sb_priv->storage_imagefile) {
            SAH_TRACEZ_ERROR(ME, "storage_imagefile is still defined");
        }
        free(sb_priv->storage_imagefile);
        if(sb_priv->storage_mountpoint) {
            SAH_TRACEZ_ERROR(ME, "storage_mountpoint is still defined");
        }
        free(sb_priv->storage_mountpoint);
        free(sb_priv->cb_data_stop.sb_id);
        free(sb_priv);
    }
    sb_obj->priv = NULL;
}

/**
 * @brief Create a sb priv object
 *
 * @param sb_obj
 * @return cthulhu_sb_priv_t*
 */
static cthulhu_sb_priv_t* create_sb_priv(amxd_object_t* sb_obj) {
    cthulhu_sb_priv_t* sb_priv = NULL;
    if(!sb_obj) {
        return NULL;
    }
    if(sb_obj->priv) {
        goto exit;
    }

    sb_priv = (cthulhu_sb_priv_t*) calloc(1, sizeof(cthulhu_sb_priv_t));
    sb_priv->cb_data_stop.sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_ID, NULL);
    sb_priv->real_status = cthulhu_sb_status_disabled;

    sb_obj->priv = sb_priv;

exit:
    return (cthulhu_sb_priv_t*) sb_obj->priv;
}

/**
 * @brief Get the status of a sandbox
 *
 * @param sb_obj
 * @return cthulhu_sb_status_t
 */
cthulhu_sb_status_t cthulhu_sb_get_status_dm(amxd_object_t* sb_obj) {
    cthulhu_sb_status_t sb_status = cthulhu_sb_status_disabled;
    char* sb_status_str = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_STATUS, NULL);
    if(!sb_status_str) {
        return sb_status;
    }
    sb_status = cthulhu_string_to_sb_status(sb_status_str);
    free(sb_status_str);
    return sb_status;
}

/**
 * @brief get the real status of the sandbox
 * this status can be different from the status in the DM, since the DM does funny things
 * with the restart status
 *
 * @param ctr_obj
 * @return cthulhu_ctr_status_t
 */
cthulhu_sb_status_t cthulhu_sb_get_status_real(amxd_object_t* sb_obj) {
    cthulhu_sb_priv_t* sb_priv = cthulhu_sb_get_priv(sb_obj);
    if(!sb_priv) {
        return cthulhu_sb_status_disabled;
    }
    return sb_priv->real_status;
}

int cthulhu_sb_set_status_real(amxd_object_t* sb_obj, cthulhu_sb_status_t sb_status) {
    int res = -1;
    cthulhu_sb_priv_t* sb_priv = cthulhu_sb_get_priv(sb_obj);
    if(!sb_priv) {
        goto exit;
    }
    sb_priv->real_status = sb_status;
    res = 0;
exit:
    return res;
}

/**
 * @brief Start all containers of a sandbox
 *
 * @param sb_id
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_start_ctrs_in_sb(const char* sb_id) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    ctr_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    if(ctr_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get " CTHULHU_DM_CONTAINER_INSTANCES, sb_id);
        goto exit;
    }
    amxd_object_for_each(instance, it, ctr_obj) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* ctr_id = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ID, NULL);
        char* ctr_sb = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_SANDBOX, NULL);
        bool autostart = amxd_object_get_bool(instance, CTHULHU_CONTAINER_AUTOSTART, NULL);
        if(autostart && !string_is_empty(ctr_sb) && (strcmp(ctr_sb, sb_id) == 0)) {
            cthulhu_notif_data_t* notif_data = NULL;
            cthulhu_notif_data_new(&notif_data, instance, cthulhu_notif_cmd_none, NULL);
            cthulhu_ctr_start(ctr_id, NULL);
            cthulhu_notif_data_delete(&notif_data);
        }
        free(ctr_id);
        free(ctr_sb);
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief Stop all containers of a sandbox
 *
 * @param sb_id
 * @return int 0 on success, -1 on failure
 */
static cthulhu_action_res_t cthulhu_stop_ctrs_in_sb(const char* sb_id, amxp_slot_fn_t busy_cb, void* const cb_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* ctr_obj = NULL;
    ctr_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    if(ctr_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get " CTHULHU_DM_CONTAINER_INSTANCES, sb_id);
        goto exit;
    }
    res = cthulhu_action_res_done;

    amxd_object_for_each(instance, it, ctr_obj) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* ctr_id = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ID, NULL);
        char* ctr_sb = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_SANDBOX, NULL);
        if(!string_is_empty(ctr_sb) && (strcmp(ctr_sb, sb_id) == 0)) {
            cthulhu_notif_data_t* notif_data = NULL;
            cthulhu_notif_data_new(&notif_data, instance, cthulhu_notif_cmd_none, NULL);
            cthulhu_action_res_t stop_res = cthulhu_ctr_stop(ctr_id, notif_data);
            cthulhu_notif_data_delete(&notif_data);
            if(stop_res == cthulhu_action_res_error) {
                res = cthulhu_action_res_error;
            } else if(stop_res == cthulhu_action_res_busy) {
                if(busy_cb) {
                    //set callback to continue when the sb is stopped
                    amxp_signal_mngr_t* sigmngr = cthulhu_ctr_get_sigmngr(ctr_id);
                    if(sigmngr) {
                        amxp_slot_connect(sigmngr,
                                          SIG_STOPPED,
                                          NULL, busy_cb, cb_data);
                    }
                    if(res != cthulhu_action_res_error) {
                        res = cthulhu_action_res_busy;
                    }
                }
            }
        }
        free(ctr_id);
        free(ctr_sb);
    }
exit:
    return res;
}

/**
 * @brief callback to check if all containers of a sandbox are stopped.
 * If all child sandboxes are stopped, we can continuw qwith the handling of stopping the sandbox
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_sb_stop_ctrs_cb(UNUSED const char* const sig_name,
                                    UNUSED const amxc_var_t* const data,
                                    void* const priv) {
    amxd_object_t* ctr_obj = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }

    cb_data_stop_t* cb_data = (cb_data_stop_t*) priv;
    SAH_TRACEZ_INFO(ME, "SB[%s]: Callback to check if all containers in sandbox are stopped", cb_data->sb_id);

    ctr_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    if(ctr_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get " CTHULHU_DM_CONTAINER_INSTANCES, cb_data->sb_id);
        return;
    }

    amxd_object_for_each(instance, it, ctr_obj) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* ctr_id = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ID, NULL);
        char* ctr_sb = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_SANDBOX, NULL);
        if(!string_is_empty(ctr_sb) && (strcmp(ctr_sb, cb_data->sb_id) == 0)) {
            cthulhu_ctr_status_t ctr_status = cthulhu_ctr_status_real_get(instance);
            if((ctr_status != cthulhu_ctr_status_ghost) && (ctr_status != cthulhu_ctr_status_stopped)) {
                SAH_TRACEZ_INFO(ME, "SB[%s]: Container [%s] is still %s", cb_data->sb_id, ctr_id, cthulhu_ctr_status_to_string(ctr_status));
                free(ctr_id);
                free(ctr_sb);
                return;
            } else {
                amxp_signal_mngr_t* sigmngr = cthulhu_ctr_get_sigmngr(ctr_id);
                if(sigmngr) {
                    amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_sb_stop_ctrs_cb);
                }
            }
        }
        free(ctr_id);
        free(ctr_sb);
    }

    // continue stopping the sandbox
    cthulhu_sb_stop_priv(cthulhu_sb_get(cb_data->sb_id));
}

/**
 * @brief start all enabled child sandboxes of a sandbox
 *
 * @param sb_id
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_start_sbs_in_sb(const char* sb_id) {
    bool res = -1;
    amxd_object_t* sb_obj = NULL;
    sb_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES);
    if(sb_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get " CTHULHU_DM_SANDBOX_INSTANCES, sb_id);
        goto exit;
    }
    amxd_object_for_each(instance, it, sb_obj) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* instance_sb_id = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_ID, NULL);
        char* parent_sb_id = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_PARENT, NULL);
        bool enable = amxd_object_get_bool(instance, CTHULHU_SANDBOX_ENABLE, NULL);
        if(!string_is_empty(parent_sb_id) && (strcmp(parent_sb_id, sb_id) == 0) && enable) {
            cthulhu_notif_data_t* notif_data = NULL;
            cthulhu_notif_data_new(&notif_data, instance, cthulhu_notif_cmd_none, NULL);
            cthulhu_sb_start(instance_sb_id, notif_data);
            cthulhu_notif_data_delete(&notif_data);
        }
        free(instance_sb_id);
        free(parent_sb_id);
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief stop all child sandboxes of a sandbox
 *
 * @param sb_id
 * @return int 0 on success, -1 on failure
 */
static cthulhu_action_res_t cthulhu_stop_sbs_in_sb(const char* sb_id, amxp_slot_fn_t busy_cb, void* const cb_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* sb_obj = NULL;
    sb_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES);
    if(sb_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get " CTHULHU_DM_SANDBOX_INSTANCES, sb_id);
        goto exit;
    }
    res = cthulhu_action_res_done;
    amxd_object_for_each(instance, it, sb_obj) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* parent_sb_id = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_PARENT, NULL);
        if(!string_is_empty(parent_sb_id) && (strcmp(parent_sb_id, sb_id) == 0)) {
            if(cthulhu_sb_get_status_real(instance) == cthulhu_sb_status_up) {
                cthulhu_action_res_t stop_res = cthulhu_sb_stop_priv(instance);
                if(stop_res == cthulhu_action_res_error) {
                    res = cthulhu_action_res_error;
                } else if(stop_res == cthulhu_action_res_busy) {
                    if(busy_cb) {
                        cthulhu_sb_priv_t* sb_priv = cthulhu_sb_get_priv(instance);
                        if(!sb_priv) {
                            SAH_TRACEZ_ERROR(ME, "SB[%s]: Sandbox has no private data", sb_id);
                            res = cthulhu_action_res_error;
                        } else {
                            //set callback to continue when the sb is stopped
                            amxp_signal_mngr_t* sigmngr = cthulhu_sb_get_sigmngr(sb_id);
                            if(sigmngr) {
                                amxp_slot_connect(sigmngr,
                                                  SIG_STOPPED,
                                                  NULL, busy_cb, cb_data);
                            }
                        }
                        if(res != cthulhu_action_res_error) {
                            res = cthulhu_action_res_busy;
                        }
                    }
                }
            }
        }
        free(parent_sb_id);
    }
exit:
    return res;
}

/**
 * @brief callback to check if all sbs are stopped.
 * If all child sandboxes are stopped, we can continuw qwith the handling of stopping the sandbox
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_sb_stop_sbs_cb(UNUSED const char* const sig_name,
                                   UNUSED const amxc_var_t* const data,
                                   void* const priv) {
    amxd_object_t* sb_obj = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_stop_t* cb_data = (cb_data_stop_t*) priv;

    sb_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES);
    if(sb_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_SANDBOX_INSTANCES);
        return;
    }
    amxd_object_for_each(instance, it, sb_obj) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* parent_sb_id = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_PARENT, NULL);
        char* instance_sb_id = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_ID, NULL);
        if(!string_is_empty(parent_sb_id) && (strcmp(parent_sb_id, cb_data->sb_id) == 0)) {
            if(cthulhu_sb_get_status_real(instance) == cthulhu_sb_status_up) {
                SAH_TRACEZ_INFO(ME, "SB[%s]: Child sandbox [%s] is still up", cb_data->sb_id, instance_sb_id);
                free(parent_sb_id);
                free(instance_sb_id);
                return;
            } else {
                amxp_signal_mngr_t* sigmngr = cthulhu_sb_get_sigmngr(cb_data->sb_id);
                if(sigmngr) {
                    amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_sb_stop_sbs_cb);
                }
            }
        }
        free(parent_sb_id);
        free(instance_sb_id);
    }
    // continue stopping the sandbox
    cthulhu_sb_stop_priv(cthulhu_sb_get(cb_data->sb_id));
}

/**
 * @brief Start a sandbox
 *
 * @param sb_id
 * @param notif_obj
 * @param command_id
 * @return int
 */
cthulhu_action_res_t cthulhu_sb_start(const char* sb_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* sb_obj = NULL;
    amxd_object_t* parent_sb_obj = NULL;
    char* parent_sb_id = NULL;
    cthulhu_sb_priv_t* sb_priv = NULL;
    int diskspace = 0;

    SAH_TRACEZ_NOTICE(ME, "SB[%s]: Start Sandbox", sb_id);

    // check if the Sandbox exists
    sb_obj = cthulhu_sb_get(sb_id);
    if(!sb_obj) {
        NOTIF_FAILED(notif_data, tr181_fault_unknown_exec_env, CTHULHU_SANDBOX " %s does not exist", sb_id);
        goto exit;
    }

    if(notif_data) {
        notif_data->obj = sb_obj;
    }

    if(cthulhu_sb_get_status_real(sb_obj) == cthulhu_sb_status_up) {
        SAH_TRACEZ_INFO(ME, "SB[%s]: Sandbox is already running", sb_id);
        res = cthulhu_action_res_done;
        goto exit;
    }
    // get the parent sandbox
    parent_sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_PARENT, NULL);
    if(!string_is_empty(parent_sb_id)) {
        parent_sb_obj = cthulhu_sb_get(parent_sb_id);
        if(cthulhu_sb_get_status_real(parent_sb_obj) != cthulhu_sb_status_up) {
            SAH_TRACEZ_INFO(ME, "SB[%s]: Cannot start sandbox because parent sandbox [%s] is disabled", sb_id, parent_sb_id);
            goto exit;
        }
    }
    char* sb_host_data_dir = cthulhu_get_sb_host_data(sb_id);
    cthulhu_plugin_sb_prestart(sb_id, sb_host_data_dir);
    free(sb_host_data_dir);

    if((sb_priv = create_sb_priv(sb_obj)) == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not create Sandbox private data");
        goto exit;
    }

    diskspace = amxd_object_get_int32_t(sb_obj, CTHULHU_SANDBOX_DISKSPACE, NULL);
    if(cthulhu_storage_mount(sb_id, (diskspace < 0) ? diskspace : diskspace* 1024) < 0) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Failed to create diskspace", sb_id);
        goto exit;
    }
    // remove storage of child sandboxes that might have been removed while this sandbox was disabled
    cthulhu_storage_remove_unused_images(sb_id);

    if(!sb_priv->cgroup_dir) {
        sb_priv->cgroup_dir = cthulhu_cgroup_dir_get(sb_id);
    }

    // create the cgroups
    if(cthulhu_cgroup_create(sb_priv->cgroup_dir) < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied,
                     "SB[%s]: Could not create cgroups for [%s]", sb_id, sb_priv->cgroup_dir);
        goto exit;
    }
    if(cthulhu_cgroup_config(sb_priv->cgroup_dir, sb_obj, notif_data) < 0) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not configure cgroups for [%s]", sb_id, sb_priv->cgroup_dir);
        goto exit;
    }
    // create namespaces
    if(cthulhu_namespaces_create(sb_obj, parent_sb_id) < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not create namespaces for [%s]", sb_id);
        goto exit;
    }

    cthulhu_sb_set_status_real(sb_obj, cthulhu_sb_status_up);
    {
        amxd_trans_t trans;
        amxd_status_t status = amxd_status_ok;
        amxd_trans_init(&trans);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&trans, sb_obj);
        amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_STATUS, cthulhu_sb_status_to_string(cthulhu_sb_status_up));
        status = amxd_trans_apply(&trans, cthulhu_get_dm());
        if(status != amxd_status_ok) {
            SAH_TRACEZ_NOTICE(ME, "SB[%s]: Transaction failed: %d", sb_id, status);
        }
        amxd_trans_clean(&trans);
    }
    cthulhu_plugin_sb_poststart(sb_id);

    cthulhu_start_ctrs_in_sb(sb_id);
    cthulhu_start_sbs_in_sb(sb_id);

    cthulhu_notif_sb_updated(notif_data, NULL);

    res = cthulhu_action_res_done;
exit:

    free(parent_sb_id);
    return res;
}

/**
 * @brief Stop a sandbox
 *
 * @param sb_obj
 * @return cthulhu_stop_state_t
 */
static cthulhu_action_res_t cthulhu_sb_stop_priv(amxd_object_t* sb_obj) {
    char* sb_id = NULL;
    cthulhu_action_res_t res = cthulhu_action_res_error;
    cthulhu_sb_priv_t* sb_priv = NULL;
    sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_ID, NULL);
    SAH_TRACEZ_NOTICE(ME, "SB[%s]: Stop Sandbox", sb_id);

    if(cthulhu_sb_get_status_real(sb_obj) == cthulhu_sb_status_disabled) {
        SAH_TRACEZ_INFO(ME, "SB[%s]: Sandbox is already disabled", sb_id);
        res = cthulhu_action_res_done;
        goto exit;
    }

    sb_priv = cthulhu_sb_get_priv(sb_obj);
    if(!sb_priv) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: sb_priv is NULL", sb_id);
        goto exit;
    }

    res = cthulhu_stop_sbs_in_sb(sb_id, cthulhu_sb_stop_sbs_cb, &(sb_priv->cb_data_stop));
    switch(res) {
    case cthulhu_action_res_error:
        SAH_TRACEZ_ERROR(ME, "SB[%s]: could not stop all child sandboxes", sb_id);
        goto exit;
        break;
    case cthulhu_action_res_busy:
        SAH_TRACEZ_INFO(ME, "SB[%s]: child sandboxes are still stopping", sb_id);
        goto exit;
        break;
    default:
        break;
    }

    res = cthulhu_stop_ctrs_in_sb(sb_id, cthulhu_sb_stop_ctrs_cb, &(sb_priv->cb_data_stop));
    switch(res) {
    case cthulhu_action_res_error:
        SAH_TRACEZ_ERROR(ME, "SB[%s]: could not stop all containers in the sandbox", sb_id);
        goto exit;
        break;
    case cthulhu_action_res_busy:
        SAH_TRACEZ_INFO(ME, "SB[%s]: containers in the sandbox are still stopping", sb_id);
        goto exit;
        break;
    default:
        break;
    }
    cthulhu_plugin_sb_prestop(sb_id);

    if(cthulhu_storage_umount(sb_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Failed to umount image", sb_id);
    }

    if(cthulhu_namespaces_destroy(sb_obj) < 0) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: could not destroy the namespaces", sb_id);
        res = cthulhu_action_res_error;
    }
    if(sb_priv && sb_priv->cgroup_dir) {
        if(cthulhu_cgroup_destroy(sb_priv->cgroup_dir) < 0) {
            SAH_TRACEZ_ERROR(ME, "SB[%s]: could not destroy the cgroups", sb_id);
            res = cthulhu_action_res_error;
        }
    }

    cthulhu_sb_set_status_real(sb_obj, cthulhu_sb_status_disabled);
    if(cthulhu_sb_get_status_dm(sb_obj) != cthulhu_sb_status_restarting) {
        amxd_trans_t trans;
        amxd_status_t status = amxd_status_ok;
        amxd_trans_init(&trans);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&trans, sb_obj);
        amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_STATUS, cthulhu_sb_status_to_string(cthulhu_sb_status_disabled));
        amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_RESTART_REASON, "");
        status = amxd_trans_apply(&trans, cthulhu_get_dm());
        if(status != amxd_status_ok) {
            SAH_TRACEZ_NOTICE(ME, "SB[%s]: Transaction failed: %d", sb_id, status);
        }
        amxd_trans_clean(&trans);
    }
    if(sb_priv->storage_mountpoint) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: storage mountpoint is not empty. Is it still mounted? [%s]", sb_id, sb_priv->storage_mountpoint);
    }
    if(sb_priv->storage_imagefile) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: storage image file is not empty. Is it still mounted? [%s]", sb_id, sb_priv->storage_imagefile);
    }
    amxp_signal_mngr_t* sigmngr = cthulhu_sb_get_sigmngr(sb_id);
    if(sigmngr) {
        amxp_sigmngr_emit_signal(sigmngr, SIG_STOPPED, NULL);
    }

exit:
    free(sb_id);
    return res;
}

typedef struct _cb_data_sb {
    char* sb_id;
    cthulhu_notif_data_t* notif_data;
} cb_data_sb_t;

/**
 * @brief Callback to continue stopping when the sandbox has stopped
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_sb_stop_cb(UNUSED const char* const sig_name,
                               UNUSED const amxc_var_t* const data,
                               void* const priv) {
    amxd_object_t* sb_obj = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_sb_t* cb_data = (cb_data_sb_t*) priv;
    SAH_TRACEZ_INFO(ME, "SB[%s]: Callback to stop sandbox", cb_data->sb_id);
    sb_obj = cthulhu_sb_get(cb_data->sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: does not exist anymore", cb_data->sb_id);
        goto exit;
    }

    // remove the callback
    amxp_signal_mngr_t* sigmngr = cthulhu_sb_get_sigmngr(cb_data->sb_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_sb_stop_cb);
    }
    // restart the container
    cthulhu_sb_stop(cb_data->sb_id, cb_data->notif_data);
exit:
    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->sb_id);
    free(cb_data);
}

/**
 * @brief Stop a sandbox
 *
 * @param sb_id
 * @param notif_obj
 * @param command_id
 * @return cthulhu_stop_state_t
 */
cthulhu_action_res_t cthulhu_sb_stop(const char* sb_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* sb_obj = NULL;

    SAH_TRACEZ_NOTICE(ME, "SB[%s]: Stop Sandbox", sb_id);
    // check if the Sandbox exists
    sb_obj = cthulhu_sb_get(sb_id);
    if(!sb_obj) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, CTHULHU_SANDBOX " %s does not exist", sb_id);
        goto exit;
    }
    if(notif_data) {
        notif_data->obj = sb_obj;
    }

    res = cthulhu_sb_stop_priv(sb_obj);
    if(res == cthulhu_action_res_error) {
        goto exit;
    }
    if(res == cthulhu_action_res_busy) {
        SAH_TRACEZ_NOTICE(ME, "SB[%s]: Sandbox is not stopped yet", sb_id);
        cb_data_sb_t* cb_data = calloc(1, sizeof(cb_data_sb_t));
        cb_data->sb_id = strdup(sb_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        amxp_signal_mngr_t* sigmngr = cthulhu_sb_get_sigmngr(sb_id);
        if(sigmngr) {
            amxp_slot_connect(sigmngr,
                              SIG_STOPPED,
                              NULL, cthulhu_sb_stop_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->sb_id);
            free(cb_data);
        }
        goto exit;
    }

    cthulhu_notif_sb_updated(notif_data, NULL);
    cthulhu_plugin_sb_poststop(sb_id);

exit:
    return res;
}

/**
 * @brief Callback to continue restarting when the sandbox has stopped
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_sb_restart_cb(UNUSED const char* const sig_name,
                                  UNUSED const amxc_var_t* const data,
                                  void* const priv) {
    amxd_object_t* sb_obj = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_sb_t* cb_data = (cb_data_sb_t*) priv;
    SAH_TRACEZ_INFO(ME, "SB[%s]: Callback to restart sandbox", cb_data->sb_id);
    sb_obj = cthulhu_sb_get(cb_data->sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: does not exist anymore", cb_data->sb_id);
        goto exit;
    }

    // remove the callback
    amxp_signal_mngr_t* sigmngr = cthulhu_sb_get_sigmngr(cb_data->sb_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_sb_restart_cb);
    }
    // restart the container
    cthulhu_sb_restart(cb_data->sb_id, cb_data->notif_data);
exit:
    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->sb_id);
    free(cb_data);
}

/**
 * @brief Restart a sandbox
 *
 * First the sandbox will be stopped. If it is not stopped, then we will subscribe
 * to the stopped signal of the sandbox, and continue with starting when
 * the sandbox is completely stopped
 *
 * @param sb_id
 * @param notif_obj
 * @return cthulhu_action_res_t
 */
cthulhu_action_res_t cthulhu_sb_restart(const char* sb_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* sb_obj = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;

    SAH_TRACEZ_NOTICE(ME, "SB[%s]: Restart Sandbox", sb_id);
    // check if the Sandbox exists
    sb_obj = cthulhu_sb_get(sb_id);
    if(!sb_obj) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, CTHULHU_SANDBOX " %s does not exist", sb_id);
        goto exit;
    }
    if(notif_data) {
        notif_data->obj = sb_obj;
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);

    res = cthulhu_sb_stop_priv(sb_obj);
    if(res == cthulhu_action_res_error) {
        goto exit;
    }
    if(res == cthulhu_action_res_busy) {
        SAH_TRACEZ_NOTICE(ME, "SB[%s]: Sandbox will be restarted after it has stopped", sb_id);
        cb_data_sb_t* cb_data = calloc(1, sizeof(cb_data_sb_t));
        cb_data->sb_id = strdup(sb_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        amxp_signal_mngr_t* sigmngr = cthulhu_sb_get_sigmngr(sb_id);
        if(sigmngr) {
            amxp_slot_connect(sigmngr,
                              SIG_STOPPED,
                              NULL, cthulhu_sb_restart_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->sb_id);
            free(cb_data);
        }
        goto exit;
    }

    // restart when the container is stopped
    res = cthulhu_sb_start(sb_id, notif_data_no_id);
    if(res != cthulhu_action_res_done) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Failed to start sandbox");
        goto exit;
    }
    cthulhu_notif_sb_updated(notif_data, NULL);
exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    return res;
}

static bool cthulhu_sb_exists(const char* const sb_id) {
    return !!cthulhu_sb_get(sb_id);
}

/**
 * @brief Callback to continue removing when the sandbox has stopped
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void cthulhu_sb_remove_cb(UNUSED const char* const sig_name,
                                 UNUSED const amxc_var_t* const data,
                                 void* const priv) {
    amxd_object_t* sb_obj = NULL;
    if(!priv) {
        SAH_TRACEZ_ERROR(ME, "No private data");
        return;
    }
    cb_data_sb_t* cb_data = (cb_data_sb_t*) priv;
    SAH_TRACEZ_INFO(ME, "SB[%s]: Callback to restart sandbox", cb_data->sb_id);
    sb_obj = cthulhu_sb_get(cb_data->sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: does not exist anymore", cb_data->sb_id);
        goto exit;
    }

    // remove the callback
    amxp_signal_mngr_t* sigmngr = cthulhu_sb_get_sigmngr(cb_data->sb_id);
    if(sigmngr) {
        amxp_slot_disconnect(sigmngr, SIG_STOPPED, cthulhu_sb_remove_cb);
    }
    // do the remove again
    cthulhu_sb_remove(cb_data->sb_id, cb_data->notif_data);
exit:
    cthulhu_notif_data_delete(&cb_data->notif_data);
    free(cb_data->sb_id);
    free(cb_data);
}
/**
 * @brief Remove a sandbox
 *
 * @param sb_id
 * @param notif_obj
 * @param command_id
 * @return int
 */
cthulhu_action_res_t cthulhu_sb_remove(const char* sb_id, cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_object_t* sb_obj = NULL;
    cthulhu_notif_data_t* notif_data_no_id = NULL;

    SAH_TRACEZ_NOTICE(ME, "SB[%s]: Remove Sandbox", sb_id);
    sb_obj = cthulhu_sb_get(sb_id);
    if(!sb_obj) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, CTHULHU_SANDBOX " %s does not exist", sb_id);
        goto exit;
    }
    if(notif_data) {
        notif_data->obj = sb_obj;
    }
    notif_data_no_id = cthulhu_notif_data_no_id(notif_data);

    if((res = cthulhu_sb_stop(sb_id, notif_data_no_id)) == cthulhu_action_res_busy) {
        SAH_TRACEZ_NOTICE(ME, "SB[%s]: Sandbox will be restarted after it has stopped", sb_id);
        cb_data_sb_t* cb_data = calloc(1, sizeof(cb_data_sb_t));
        cb_data->sb_id = strdup(sb_id);
        cb_data->notif_data = cthulhu_notif_data_no_obj(notif_data);
        amxp_signal_mngr_t* sigmngr = cthulhu_sb_get_sigmngr(sb_id);
        if(sigmngr) {
            amxp_slot_connect(sigmngr,
                              SIG_STOPPED,
                              NULL, cthulhu_sb_remove_cb, cb_data);
        } else {
            cthulhu_notif_data_delete(&cb_data->notif_data);
            free(cb_data->sb_id);
            free(cb_data);
        }
        goto exit;
    }
    cthulhu_plugin_sb_preremove(sb_id);


    // remove data
    cthulhu_storage_remove(sb_id);

    delete_sb_priv(sb_obj);
    cthulhu_notif_sb_removed(notif_data);
    amxd_object_delete(&sb_obj);
    cthulhu_sigmngrs_delayed_remove(&sb_sigmngrs, sb_id, cthulhu_sb_exists);

    cthulhu_plugin_sb_postremove(sb_id);


exit:
    cthulhu_notif_data_delete(&notif_data_no_id);
    return res;
}

/**
 * @brief Create sandbox data and fill in all the relevant data
 *
 * @param sb_id
 * @return cthulhu_sb_data_t*
 */
cthulhu_sb_data_t* cthulhu_create_sb_data(const char* sb_id) {
    cthulhu_sb_data_t* sb_data = NULL;
    amxd_object_t* sb_obj = cthulhu_sb_get(sb_id);
    amxd_object_t* dm_obj = NULL;
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get Sandbox", sb_id);
        return NULL;
    }
    if(cthulhu_sb_data_new(&sb_data)) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not init sandbox data", sb_id);
        return NULL;
    }
    sb_data->sb_id = strdup(sb_id);
    sb_data->ns_pid = amxd_object_get_int32_t(sb_obj, CTHULHU_SANDBOX_PID, NULL);
    dm_obj = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_UTS_NS);
    if(!dm_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get " CTHULHU_SANDBOX_UTS_NS " from DM", sb_id);
    } else {
        sb_data->ns_inherrit_uts = amxd_object_get_bool(dm_obj, CTHULHU_SANDBOX_UTS_NS_ENABLE, NULL);
    }

/* Not implemented yet
    dm_obj = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_USER_NS);
    if(!dm_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get " CTHULHU_SANDBOX_USER_NS " from DM", sb_id);
    } else {
        sb_data->ns_inherrit_user = amxd_object_get_bool(dm_obj, CTHULHU_SANDBOX_USER_NS_ENABLE, NULL);
    }
 */

    dm_obj = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_NETWORK_NS);
    if(!dm_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get " CTHULHU_SANDBOX_NETWORK_NS " from DM", sb_id);
    } else {
        sb_data->ns_inherrit_net = amxd_object_get_bool(dm_obj, CTHULHU_SANDBOX_NETWORK_NS_ENABLE, NULL);
    }

/* Not implemented yet
    dm_obj = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_MOUNT_NS);
    if(!dm_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get " CTHULHU_SANDBOX_MOUNT_NS " from DM", sb_id);
    } else {
        sb_data->ns_inherrit_mnt = amxd_object_get_bool(dm_obj, CTHULHU_SANDBOX_MOUNT_NS_ENABLE, NULL);
    }
 */
    return sb_data;
}

void cthulhu_sb_init(void) {
    cthulhu_sigmngrs_init(&sb_sigmngrs);
    cthulhu_namespaces_init();
    // start all sandboxes that are enabled and that have no parent
    amxd_object_t* sb_obj = NULL;
    sb_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES);
    if(sb_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Sandbox: Could not get " CTHULHU_DM_SANDBOX_INSTANCES);
        return;
    }
    amxd_object_for_each(instance, it, sb_obj) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* sb_id = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_ID, NULL);
        char* parent = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_PARENT, NULL);
        char* created = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_CREATED, NULL);
        if(string_is_empty(created)) {
            amxc_string_t time_string;
            amxc_string_init(&time_string, 0);
            time_t now;
            time(&now);
            // ctime returns a string with \n, so we strip this
            amxc_string_setf(&time_string, "%s", ctime(&now));
            amxc_string_trimr(&time_string, iscntrl);
            amxd_object_set_cstring_t(instance, CTHULHU_SANDBOX_CREATED, time_string.buffer);
            amxc_string_clean(&time_string);
        }
        bool enable = amxd_object_get_bool(instance, CTHULHU_SANDBOX_ENABLE, NULL);
        if(enable && string_is_empty(parent)) {
            cthulhu_notif_data_t* notif_data = NULL;
            cthulhu_notif_data_new(&notif_data, instance, cthulhu_notif_cmd_none, NULL);
            cthulhu_sb_start(sb_id, notif_data);
            cthulhu_notif_data_delete(&notif_data);
        }
        free(sb_id);
        free(parent);
        free(created);
    }
}

void cthulhu_sb_cleanup(void) {
    cthulhu_sigmngrs_clean(&sb_sigmngrs);
    cthulhu_namespaces_cleanup();
}

int cthulhu_sb_exec(const char* sb_id, const char* cmd) {
    int res = -1;
    amxd_object_t* sb_obj = NULL;

    SAH_TRACEZ_NOTICE(ME, "SB[%s]: Exec [%s] in SB", sb_id, cmd);

    // check if the Sandbox exists
    sb_obj = cthulhu_sb_get(sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Sandbox does not exist", sb_id);
        goto exit;
    }

    if(cthulhu_sb_get_status_real(sb_obj) != cthulhu_sb_status_up) {
        SAH_TRACEZ_INFO(ME, "SB[%s]: Sandbox is not running", sb_id);
        res = 0;
        goto exit;
    }

    cthulhu_namespaces_exec(sb_obj, cmd);

    res = 0;
exit:
    return res;
}

cthulhu_action_res_t cthulhu_sb_create(const char* sb_id, const char* parent_sb,
                                       const char* version, const char* vendor,
                                       bool enable, uint32_t cpu, uint32_t memory,
                                       uint32_t diskspace, bool created_by_container,
                                       cthulhu_notif_data_t* notif_data) {
    cthulhu_action_res_t res = cthulhu_action_res_error;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* dm_obj = NULL;
    amxd_object_t* sb_obj = NULL;
    char alias[ALIAS_SIZE + 1];
    amxc_string_t time_string;
    amxd_trans_t trans;
    amxd_dm_t* dm = cthulhu_get_dm();
    char* sb_host_data_dir = NULL;

    amxc_string_init(&time_string, 0);
    amxd_trans_init(&trans);

    // check if there is already an Sandbox with this sb_id
    sb_obj = cthulhu_sb_get(sb_id);
    if(sb_obj != NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, CTHULHU_SANDBOX " %s already exists", sb_id);
        goto exit;
    }

    dm_obj = amxd_dm_findf(dm, CTHULHU_DM_SANDBOX_INSTANCES);
    if(dm_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not find " CTHULHU_DM_SANDBOX_INSTANCES);
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "DM error");
        goto exit;
    }

    time_t now;
    time(&now);
    // ctime returns a string with \n, so we strip this
    amxc_string_setf(&time_string, "%s", ctime(&now));
    amxc_string_trimr(&time_string, iscntrl);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, dm_obj);
    snprintf(alias, ALIAS_SIZE, "cpe-%s", sb_id);
    amxd_trans_add_inst(&trans, 0, alias);
    amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_ID, sb_id);
    amxd_trans_set_int32_t(&trans, CTHULHU_SANDBOX_CPU, cpu);
    amxd_trans_set_bool(&trans, CTHULHU_SANDBOX_ENABLE, enable);
    amxd_trans_set_int32_t(&trans, CTHULHU_SANDBOX_MEMORY, memory);
    amxd_trans_set_int32_t(&trans, CTHULHU_SANDBOX_DISKSPACE, diskspace);
    amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_CREATED, time_string.buffer);
    if(parent_sb) {
        amxd_trans_set_value(cstring_t, &trans, CTHULHU_SANDBOX_PARENT, parent_sb);
    }
    if(version) {
        amxd_trans_set_value(cstring_t, &trans, CTHULHU_SANDBOX_VERSION, version);
    }
    if(vendor) {
        amxd_trans_set_value(cstring_t, &trans, CTHULHU_SANDBOX_VENDOR, vendor);
    }
    amxd_trans_set_bool(&trans, CTHULHU_SANDBOX_CREATEDBYCONTAINER, created_by_container);
    if(created_by_container) {
        // if the sandbox is created by a container, then put the defaults
        // for NetworkNS to enable=true and type=ParentSb
        // this way it will properly inherit the network namespace from the parent sb
        // this is especially valid for installing a container that autostarts
        // if this is not set, the container will have no network namespace
        amxd_trans_select_pathf(&trans, "." CTHULHU_SANDBOX_NETWORK_NS);
        amxd_trans_set_cstring_t(&trans, CTHULHU_SANDBOX_NETWORK_NS_TYPE, CTHULHU_SANDBOX_NETWORK_NS_TYPE_PARENTSB);
        amxd_trans_set_bool(&trans, CTHULHU_SANDBOX_NETWORK_NS_ENABLE, true);
    }


    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Transaction failed. Status: %d", status);
        goto exit;
    }

    sb_obj = cthulhu_sb_get(sb_id);
    if(notif_data) {
        notif_data->obj = sb_obj;
    }
    cthulhu_notif_sb_created(notif_data);
    sb_host_data_dir = cthulhu_get_sb_host_data(sb_id);
    cthulhu_plugin_sb_create(sb_id, sb_host_data_dir);

    if(enable) {
        cthulhu_notif_data_t* notif_data_no_id = NULL;
        notif_data_no_id = cthulhu_notif_data_no_id(notif_data);
        cthulhu_sb_start(sb_id, notif_data_no_id);
        cthulhu_notif_data_delete(&notif_data_no_id);
    }

    res = cthulhu_action_res_done;
exit:
    free(sb_host_data_dir);
    amxc_string_clean(&time_string);
    amxd_trans_clean(&trans);

    return res;
}
