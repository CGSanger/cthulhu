/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>

#include "cthulhu_priv.h"

#define ME "cthulhu_oci"

static const char* mediatype_image_index_v1 = "application/vnd.oci.image.index.v1+json";
static const char* mediatype_image_config_v1 = "application/vnd.oci.image.config.v1+json";

amxc_string_t* cthulhu_get_digest_file(const char* digest,
                                       const char* base_dir) {
    amxc_string_t* digest_file = NULL;
    char* digest_copy = NULL;
    char* pos = NULL;

    digest_copy = strdup(digest);
    if(digest_copy == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not copy string");
        goto exit;
    }
    // replace : with /
    if((pos = strchr(digest_copy, ':')) != NULL) {
        *pos = '/';
    }
    amxc_string_new(&digest_file, 0);
    if(digest_file == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate string");
        goto exit;
    }
    amxc_string_setf(digest_file, "%s/%s", base_dir, digest_copy);
exit:
    free(digest_copy);
    return digest_file;
}

amxc_string_t* cthulhu_get_digest_hash(const char* digest) {
    amxc_string_t string;
    amxc_string_t* res = NULL;
    amxc_llist_t list;

    amxc_string_init(&string, 0);
    amxc_llist_init(&list);
    amxc_string_setf(&string, "%s", digest);

    if(amxc_string_split_to_llist(&string, &list, ':') != AMXC_STRING_SPLIT_OK) {
        SAH_TRACEZ_ERROR(ME, "Could not split digest [%s]", digest);
        goto exit;
    }
    if(amxc_llist_size(&list) != 2) {
        SAH_TRACEZ_ERROR(ME,
                         "Digest consistes of %zd parts instead of 2 [%s]",
                         amxc_llist_size(&list), digest);
        goto exit;
    }
    res = amxc_string_from_llist_it(amxc_llist_take_last(&list));

exit:
    amxc_string_clean(&string);
    amxc_llist_clean(&list, amxc_string_list_it_free);
    return res;
}

image_spec_schema_image_index_schema*
cthulhu_get_image_index_schema(const char* bundle_location,
                               const char* image_location) {
    image_spec_schema_image_index_schema* image_index_schema = NULL;
    amxc_string_t image_index_file;
    parser_error err = NULL;

    amxc_string_init(&image_index_file, 0);

    if(string_is_empty(bundle_location)) {
        SAH_TRACEZ_ERROR(ME, "Bundle name is empty");
        goto exit;
    }
    if(string_is_empty(image_location)) {
        SAH_TRACEZ_ERROR(ME, "Image location is empty");
        goto exit;
    }

    // load the image spec
    // https://github.com/opencontainers/image-spec/blob/master/image-index.md
    amxc_string_setf(&image_index_file, "%s/%s/index.json", image_location, bundle_location);
    SAH_TRACEZ_INFO(ME, "Load image index %s", image_index_file.buffer);
    image_index_schema
        = image_spec_schema_image_index_schema_parse_file(image_index_file.buffer,
                                                          NULL, &err);
    if(image_index_schema == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not parse image index file %s", image_index_file.buffer);
        SAH_TRACEZ_ERROR(ME, "Err: %s", err);
        goto exit;
    }
exit:
    amxc_string_clean(&image_index_file);
    return image_index_schema;
}

image_spec_schema_image_manifest_schema*
cthulhu_get_image_manifest_schema(image_spec_schema_image_index_schema* image_index,
                                  const char* bundle_version,
                                  const char* blob_location) {
    amxc_string_t* manifest_file = NULL;
    parser_error err = NULL;
    image_spec_schema_image_manifest_schema* image_manifest = NULL;
    image_spec_schema_image_index_schema_manifests_element* manifest = NULL;

    if(image_index == NULL) {
        SAH_TRACEZ_ERROR(ME, "image_index_schema is empty");
        goto exit;
    }

    SAH_TRACE_INFO("get version %s", bundle_version);

    // load the manifest
    // https://github.com/opencontainers/image-spec/blob/master/manifest.md
    if(image_index->manifests_len == 0) {
        SAH_TRACEZ_ERROR(ME, "There is no manifest in the image index");
        goto exit;
    }
    for(size_t manifest_nr = 0; manifest_nr < image_index->manifests_len; ++manifest_nr) {
        // find annotation org.opencontainers.image.ref.name
        // and compare it with the bundle_version
        json_map_string_string* annotations = image_index->manifests[manifest_nr]->annotations;
        if(!annotations) {
            continue;
        }
        for(size_t i = 0; i < annotations->len; ++i) {
            SAH_TRACEZ_INFO(ME, "%zd: %s %s", manifest_nr, annotations->keys[i], annotations->values[i]);
            if((strcmp(annotations->keys[i], "org.opencontainers.image.ref.name") == 0) &&
               ( strcmp(annotations->values[i], bundle_version) == 0)) {
                manifest = image_index->manifests[manifest_nr];
                break;
            }
        }
        if(manifest) {
            break;
        }
    }
    if(!manifest) {
        SAH_TRACEZ_ERROR(ME, "Manifest with version %s not found", bundle_version);
        goto exit;
    }

    if(strcmp(mediatype_image_index_v1, manifest->media_type) == 0) {
        SAH_TRACEZ_ERROR(ME, "Unknown manifest type: %s", manifest->media_type);
        goto exit;
    }
    manifest_file = cthulhu_get_digest_file(manifest->digest, blob_location);
    SAH_TRACEZ_INFO(ME, "Load manifest %s", manifest_file->buffer);


    image_manifest
        = image_spec_schema_image_manifest_schema_parse_file(manifest_file->buffer,
                                                             NULL,
                                                             &err);
    if(image_manifest == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not parse Image Manifest file %s", manifest_file->buffer);
        SAH_TRACEZ_ERROR(ME, "Err: %s", err);
        goto exit;
    }
    if(image_manifest->schema_version_present && (image_manifest->schema_version != 2)) {
        SAH_TRACEZ_ERROR(ME, "The manifest has an invalid schema version.");
        SAH_TRACEZ_ERROR(ME, "Manifest file: %s", manifest_file->buffer);
        SAH_TRACEZ_ERROR(ME, "Schema version: %d", image_manifest->schema_version);
        free_image_spec_schema_image_manifest_schema(image_manifest);
        image_manifest = NULL;
        goto exit;
    }
exit:
    amxc_string_delete(&manifest_file);
    free(err);
    return image_manifest;
}

image_spec_schema_image_manifest_schema*
cthulhu_get_image_manifest_schema_from_image(const char* bundle_location,
                                             const char* bundle_version,
                                             const char* image_location,
                                             const char* blob_location) {
    image_spec_schema_image_index_schema* image_index_schema
        = cthulhu_get_image_index_schema(bundle_location, image_location);
    image_spec_schema_image_manifest_schema* image_manifest = NULL;
    if(image_index_schema == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not find image index schema %s", bundle_location);
        goto exit;
    }
    image_manifest = cthulhu_get_image_manifest_schema(image_index_schema, bundle_version, blob_location);
exit:
    free_image_spec_schema_image_index_schema(image_index_schema);
    return image_manifest;
}

image_spec_schema_config_schema*
cthulhu_get_image_config(image_spec_schema_image_manifest_schema* image_manifest,
                         const char* blob_location) {
    image_spec_schema_config_schema* image_config = NULL;
    amxc_string_t* config_file = NULL;
    parser_error err = NULL;

    if(image_manifest == NULL) {
        SAH_TRACEZ_ERROR(ME, "image_manifest is NULL");
        goto exit;
    }
    if(image_manifest->config == NULL) {
        SAH_TRACEZ_ERROR(ME, "image config is NULL");
        goto exit;
    }
    if(strcmp(mediatype_image_config_v1,
              image_manifest->config->media_type) != 0) {
        SAH_TRACEZ_ERROR(ME, "Unexpected config type: %s Expected %s",
                         image_manifest->config->media_type, mediatype_image_config_v1);
        goto exit;
    }
    config_file = cthulhu_get_digest_file(image_manifest->config->digest, blob_location);
    SAH_TRACEZ_INFO(ME, "Load config schema %s", config_file->buffer);
    image_config = image_spec_schema_config_schema_parse_file(config_file->buffer, NULL, &err);
    if(image_config == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not parse Image Config file %s", config_file->buffer);
        SAH_TRACEZ_ERROR(ME, "Err: %s", err);
        goto exit;
    }

exit:
    free(err);
    amxc_string_delete(&config_file);
    return image_config;
}


