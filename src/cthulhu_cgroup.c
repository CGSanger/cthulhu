/****************************************************************************
**
** Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sys/vfs.h>
#include <sys/stat.h>
#include <linux/magic.h>
#include <dirent.h>
#include <errno.h>

#include <cthulhu/cthulhu_helpers.h>
#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_priv.h"

#define ME "cthulhu_cgroup"


#ifdef CGROUP_DIR_OVERRIDE
#define CGROUP_DIR STR(CGROUP_DIR_OVERRIDE)
#else
#define CGROUP_DIR "/sys/fs/cgroup"
#endif

#define CGROUP_BASE "cthulhu"

#ifndef CGROUP_SUPER_MAGIC
#define CGROUP_SUPER_MAGIC 0x27e0eb
#endif

#ifndef CGROUP2_SUPER_MAGIC
#define CGROUP2_SUPER_MAGIC 0x63677270
#endif

typedef enum _cgroup_layout {
    cgroup_layout_error = -2,
    cgroup_layout_unknown = -1,
    cgroup_layout_hierarchic = 0,
    cgroup_layout_unified = 1,
} cgroup_layout_t;

static cgroup_layout_t s_cgroup_layout = cgroup_layout_unknown;


static cgroup_layout_t cthulhu_cgroup_layout_get(void) {
    struct statfs fs;
    int ret;

    if(s_cgroup_layout != cgroup_layout_unknown) {
        return s_cgroup_layout;
    }

    ret = statfs(CGROUP_DIR, &fs);
    if(ret < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not stat " CGROUP_DIR);
        s_cgroup_layout = cgroup_layout_error;
        return s_cgroup_layout;
    }

    switch(fs.f_type) {
    case CGROUP2_SUPER_MAGIC:
        SAH_TRACEZ_INFO(ME, "Cgroup type v2");
        s_cgroup_layout = cgroup_layout_unified;
        break;
    case CGROUP_SUPER_MAGIC:
        SAH_TRACEZ_INFO(ME, "Cgroup type v1, assume unified");
        s_cgroup_layout = cgroup_layout_unified;
        break;
    case TMPFS_MAGIC:
        SAH_TRACEZ_INFO(ME, "Cgroup type tmpfs, assume hierarchic");
        s_cgroup_layout = cgroup_layout_hierarchic;
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Does not compute [0x%lx], assume hierarchic", fs.f_type);
        s_cgroup_layout = cgroup_layout_hierarchic;
        break;
    }

    return s_cgroup_layout;
}

/**
 * @brief Create cgroups for a sandbox
 *
 * @param name
 * @return int 0 on success, -1 on failure
 */
int cthulhu_cgroup_create(const char* cgroup_name) {
    int res = -1;
    DIR* cgroup_root = NULL;
    char path[1024];
    cgroup_layout_t cgroup_layout;

    cgroup_layout = cthulhu_cgroup_layout_get();
    if(cgroup_layout == cgroup_layout_error) {
        SAH_TRACEZ_ERROR(ME, "Failed to get cgroup layout");
        goto exit;
    }

    if(cgroup_layout == cgroup_layout_hierarchic) {
        cgroup_root = opendir(CGROUP_DIR);
        if(!cgroup_root) {
            SAH_TRACEZ_ERROR(ME, "Could not open %s (%d: %s)", CGROUP_DIR, errno, strerror(errno));
            goto exit;
        }
        struct dirent* entry;
        while((entry = readdir(cgroup_root))) {
            if((strcmp(entry->d_name, ".") == 0) ||
               (strcmp(entry->d_name, "..") == 0)) {
                continue;
            }
            if(entry->d_type == DT_DIR) {
                sprintf(path, "%s/%s/%s", CGROUP_DIR, entry->d_name, cgroup_name);
                if(cthulhu_mkdir(path, true) < 0) {
                    SAH_TRACEZ_ERROR(ME, "Could not make %s", path);
                    goto exit;
                }
            }
        }
    } else {
        sprintf(path, "%s/%s", CGROUP_DIR, cgroup_name);
        if(cthulhu_mkdir(path, true) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not make %s", path);
            goto exit;
        }
    }
    res = 0;
exit:
    if(cgroup_root) {
        closedir(cgroup_root);
    }
    return res;
}

/**
 * @brief Destroy cgroups from a sandbox
 *
 * @param cgroup_name
 * @return int 0 on success, -1 on failure
 */
int cthulhu_cgroup_destroy(const char* cgroup_name) {
    int res = -1;
    DIR* cgroup_root = NULL;
    char path[1024];
    cgroup_layout_t cgroup_layout;

    cgroup_layout = cthulhu_cgroup_layout_get();
    if(cgroup_layout == cgroup_layout_error) {
        SAH_TRACEZ_ERROR(ME, "Failed to get cgroup layout");
        goto exit;
    }

    if(cgroup_layout == cgroup_layout_hierarchic) {
        cgroup_root = opendir(CGROUP_DIR);
        if(!cgroup_root) {
            SAH_TRACEZ_ERROR(ME, "Could not open %s (%d: %s)", CGROUP_DIR, errno, strerror(errno));
            goto exit;
        }
        struct dirent* entry;
        while((entry = readdir(cgroup_root))) {
            if((strcmp(entry->d_name, ".") == 0) ||
               (strcmp(entry->d_name, "..") == 0)) {
                continue;
            }
            if(entry->d_type == DT_DIR) {
                sprintf(path, "%s/%s/%s", CGROUP_DIR, entry->d_name, cgroup_name);
                if(rmdir(path) < 0) {
                    SAH_TRACEZ_NOTICE(ME, "Could not remove dir [%s] (%d: %s)", path, errno, strerror(errno));
                }
            }
        }
    } else {
        sprintf(path, "%s/%s", CGROUP_DIR, cgroup_name);
        if(rmdir(path) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not remove dir [%s] (%d: %s)", path, errno, strerror(errno));
        }
    }
    // rm CGROUP_BASE, this may fail if there are still cgroups present
    sprintf(path, "%s/%s", CGROUP_DIR, CGROUP_BASE);
    rmdir(path);

    res = 0;
exit:
    if(cgroup_root) {
        closedir(cgroup_root);
    }
    return res;
}

/**
 * @brief Set a setting of a cgroup
 *
 * @param cgroup_name The cgroup name of the sandbox
 * @param cgroup the cgroup
 * @param setting
 * @param data
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_cgroup_setting_set(const char* cgroup_name, const char* cgroup, const char* setting, const char* data) {
    int res = -1;
    FILE* f = NULL;
    char path[1024];
    cgroup_layout_t cgroup_layout;

    cgroup_layout = cthulhu_cgroup_layout_get();
    if(cgroup_layout == cgroup_layout_error) {
        SAH_TRACEZ_ERROR(ME, "Failed to get cgroup layout");
        goto exit;
    }

    if(cgroup_layout == cgroup_layout_hierarchic) {

        sprintf(path, "%s/%s/%s/%s", CGROUP_DIR, cgroup, cgroup_name, setting);
    } else {
        sprintf(path, "%s/%s/%s", CGROUP_DIR, cgroup_name, setting);
    }
    f = fopen(path, "w");
    if(!f) {
        SAH_TRACEZ_ERROR(ME, "Could not open %s (%d: %s)", path, errno, strerror(errno));
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "Write %s to %s", data, path);
    if(fputs(data, f) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not write %s to %s (%d: %s)", data, path, errno, strerror(errno));
        goto exit;
    }

    res = 0;
exit:
    if(f) {
        fclose(f);
    }
    return res;
}

static char* cthulhu_cgroup_setting_get(const char* cgroup_name, const char* cgroup, const char* setting) {
    FILE* f = NULL;
    char path[1024];
    char* data = NULL;
    cgroup_layout_t cgroup_layout;

    data = (char*) calloc(1, 64);

    cgroup_layout = cthulhu_cgroup_layout_get();
    if(cgroup_layout == cgroup_layout_error) {
        SAH_TRACEZ_ERROR(ME, "Failed to get cgroup layout");
        goto error;
    }

    if(cgroup_layout == cgroup_layout_hierarchic) {

        sprintf(path, "%s/%s/%s/%s", CGROUP_DIR, cgroup, cgroup_name, setting);
    } else {
        sprintf(path, "%s/%s/%s", CGROUP_DIR, cgroup_name, setting);
    }
    f = fopen(path, "r");
    if(!f) {
        SAH_TRACEZ_ERROR(ME, "Could not open %s (%d: %s)", path, errno, strerror(errno));
        goto error;
    }
    if(!fgets(data, 64, f)) {
        SAH_TRACEZ_ERROR(ME, "Could not read from %s (%d: %s)", path, errno, strerror(errno));
        goto error;
    }
    goto exit;
error:
    free(data);
    data = NULL;
exit:
    if(f) {
        fclose(f);
    }
    return data;
}

static bool cthulhu_cgroup_setting_exists(const char* cgroup, const char* setting) {
    bool exists = false;
    char path[1024];
    cgroup_layout_t cgroup_layout;

    cgroup_layout = cthulhu_cgroup_layout_get();
    if(cgroup_layout == cgroup_layout_error) {
        SAH_TRACEZ_ERROR(ME, "Failed to get cgroup layout");
        goto exit;
    }
    if(cgroup_layout == cgroup_layout_hierarchic) {

        sprintf(path, "%s/%s/%s", CGROUP_DIR, cgroup, setting);
    } else {
        sprintf(path, "%s/%s", CGROUP_DIR, setting);
    }

    if(access(path, F_OK) == 0) {
        exists = true;
    }
exit:
    return exists;
}


static int64_t cgroup_mem_undefined = 0x1FFFFFFFFFFFFC;  // this is 0x7FFFFFFFFFFFF000 / 1024
/**
 * @brief recursively get the smallest available memory in the sandbox hierarchy
 *
 * @param sb_id
 * @return uint64_t
 */
static uint64_t cthulhu_cgroup_available_mem_get(const char* sb_id) {
    uint64_t my_limit = cgroup_mem_undefined;
    uint64_t parent_limit = cgroup_mem_undefined;
    char* parent_sb_id = NULL;
    char* cgroup_dir = NULL;
    char* data = NULL;
    amxd_object_t* sb_obj = NULL;

    sb_obj = cthulhu_sb_get(sb_id);
    when_null(sb_obj, exit);
    parent_sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_PARENT, NULL);

    cgroup_dir = cthulhu_cgroup_dir_get(sb_id);
    if(string_is_empty(cgroup_dir)) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get cgroup dir", sb_id);
        goto exit;
    }
    data = cthulhu_cgroup_setting_get(cgroup_dir, "memory", "memory.limit_in_bytes");
    if(!data) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get memory.limit_in_bytes", sb_id);
        goto exit;
    }
    my_limit = atol(data) / 1024;
    if(!string_is_empty(parent_sb_id)) {
        parent_limit = cthulhu_cgroup_available_mem_get(parent_sb_id);
    }

exit:
    free(cgroup_dir);
    free(parent_sb_id);
    free(data);
    return (my_limit < parent_limit) ? my_limit : parent_limit;
}

int cthulhu_cgroup_memory_resources_get(const char* sb_id, cthulhu_resource_stats_t* cthulhu_stats) {
    int res = -1;
    char* cgroup_dir = NULL;
    char* data = NULL;

    when_null(cthulhu_stats, exit);

    cgroup_dir = cthulhu_cgroup_dir_get(sb_id);
    if(string_is_empty(cgroup_dir)) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get cgroup dir", sb_id);
        goto exit;
    }

    data = cthulhu_cgroup_setting_get(cgroup_dir, "memory", "memory.usage_in_bytes");
    if(!data) {
        SAH_TRACEZ_INFO(ME, "SB[%s]: Could not get memory.usage_in_bytes", sb_id);
        goto exit;
    }
    cthulhu_stats->used = atol(data) / 1024;
    cthulhu_stats->total = cthulhu_cgroup_available_mem_get(sb_id);

    if(cthulhu_stats->total == cgroup_mem_undefined) {
        // no limit configured
        // optimization: get free memory from /proc/meminfo
        cthulhu_stats->total = -1;
        cthulhu_stats->free = -1;
        goto exit;
    } else {
        cthulhu_stats->free = cthulhu_stats->total - cthulhu_stats->used;
    }
    // SAH_TRACEZ_INFO(ME, "SB[%s]: Memory Total: %" PRIu64 " Kb Used: %" PRIu64 " Kb, Free: %" PRIu64 " Kb",
    //                 sb_id, cthulhu_stats->total, cthulhu_stats->used, cthulhu_stats->free);
    res = 0;

exit:
    free(data);
    free(cgroup_dir);
    return res;
}

/**
 * @brief Configure the devices cgroup
 *
 * @param cgroup_name The cgroup name of the sandbox
 * @param sb_obj
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_config_devices(const char* cgroup_name, amxd_object_t* sb_obj) {
    int res = 0;
    amxd_object_t* devices = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_DEVICES);
    if(!devices) {
        SAH_TRACEZ_ERROR(ME, "Could not get Devices of sandbox");
        return -1;
    }
    amxd_object_for_each(instance, it, devices) {
        amxd_object_t* device = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* devicename = amxd_object_get_cstring_t(device, CTHULHU_SANDBOX_DEVICES_DEVICE, NULL);
        char* permission = amxd_object_get_cstring_t(device, CTHULHU_SANDBOX_DEVICES_PERMISSION, NULL);
        if(devicename && permission) {
            if(strcmp(permission, "Allow") == 0) {
                cthulhu_cgroup_setting_set(cgroup_name, "devices", "devices.allow", devicename);
            } else {
                cthulhu_cgroup_setting_set(cgroup_name, "devices", "devices.deny", devicename);
            }
        }
        free(devicename);
        free(permission);

    }
    return res;
}

/**
 * @brief Configure the memory cgroup
 *
 * @param cgroup_name The cgroup name of the sandbox
 * @param sb_obj
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_cgroup_config_memory(const char* cgroup_name, amxd_object_t* sb_obj) {
    int res = 0;
    char memory_string[32];
    int32_t memory = amxd_object_get_int32_t(sb_obj, CTHULHU_SANDBOX_MEMORY, NULL);
    if(memory < 0) {
        // Writing -1 resets the previous memory restriction
        memory_string[0] = '-';
        memory_string[1] = '1';
        memory_string[2] = 0;
    } else {
        snprintf(memory_string, 32, "%dk", memory); // let cgroups convert to kb
    }
    res |= cthulhu_cgroup_setting_set(cgroup_name, "memory", "memory.limit_in_bytes", memory_string);
    res |= cthulhu_cgroup_setting_set(cgroup_name, "memory", "memory.soft_limit_in_bytes", memory_string);
    return res;
}

/**
 * @brief helper to convert cgroup settings to int
 *
 * "1" -> 1
 * "0-3" -> 4
 *
 * @param element

 */
static int cthulhu_cgroup_nr_of_cpus_convert(const amxc_string_t* element) {
    amxc_llist_t list;
    int res = 1;

    amxc_llist_init(&list);
    if(amxc_string_split_to_llist(element, &list, '-') != AMXC_STRING_SPLIT_OK) {
        SAH_TRACEZ_ERROR(ME, "Could not split setting [%s]", element->buffer);
        goto exit;
    }
    switch(amxc_llist_size(&list)) {
    case 1:
        goto exit;
    case 2:
    {
        int one, two;
        amxc_string_t* s = amxc_string_from_llist_it(amxc_llist_get_at(&list, 0));
        one = atoi(s->buffer);
        s = amxc_string_from_llist_it(amxc_llist_get_at(&list, 1));
        two = atoi(s->buffer);
        res = two - one + 1;
        goto exit;
    }
    default:
        SAH_TRACEZ_ERROR(ME, "String cannot be decoded [%s]", element->buffer);
        break;
    }

exit:
    amxc_llist_clean(&list, amxc_string_list_it_free);
    return res;
}

static int cthulhu_cgroup_nr_of_cpus(const char* cgroup_name) {
    amxc_string_t cpus;
    amxc_llist_t list;
    amxc_llist_it_t* it;
    int res = 0;

    char* setting = cthulhu_cgroup_setting_get(cgroup_name, "cpuset", "cpuset.cpus");
    if(!setting) {
        SAH_TRACEZ_ERROR(ME, "Failed to get cpuset.cpus");
        return 1;
    }
    amxc_string_init(&cpus, 0);
    amxc_llist_init(&list);
    amxc_string_setf(&cpus, "%s", setting);

    if(amxc_string_split_to_llist(&cpus, &list, ',') != AMXC_STRING_SPLIT_OK) {
        SAH_TRACEZ_ERROR(ME, "Could not split setting [%s]", setting);
        goto exit;
    }
    while((it = amxc_llist_take_first(&list)) != NULL) {
        amxc_string_t* cpu_str = amxc_string_from_llist_it(it);
        res += cthulhu_cgroup_nr_of_cpus_convert(cpu_str);
        amxc_string_delete(&cpu_str);
    }

exit:
    amxc_string_clean(&cpus);
    amxc_llist_clean(&list, amxc_string_list_it_free);
    free(setting);
    return res;
}

/**
 * @brief Configure the CPU cgroup
 *
 * @param cgroup_name The cgroup name of the sandbox
 * @param sb_obj
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_cgroup_config_cpu(const char* cgroup_name, amxd_object_t* sb_obj) {
    int res = 0;
    int nr_of_cpus = 0;

    int32_t cpu = amxd_object_get_int32_t(sb_obj, CTHULHU_SANDBOX_CPU, NULL);
    nr_of_cpus = cthulhu_cgroup_nr_of_cpus(cgroup_name);
    SAH_TRACEZ_INFO(ME, "Nr of cpus %d", nr_of_cpus);

    if(cthulhu_cgroup_setting_exists("cpu", "cpu.cfs_quota_us")) {
        // cfs scheduler
        if((cpu < 0) || (cpu >= 100)) {
            res |= cthulhu_cgroup_setting_set(cgroup_name, "cpu", "cpu.cfs_quota_us", "-1");
        } else {
            char cpu_string[32];
            sprintf(cpu_string, "%d", cpu * 1000 * nr_of_cpus);
            res |= cthulhu_cgroup_setting_set(cgroup_name, "cpu", "cpu.cfs_quota_us", cpu_string);
            res |= cthulhu_cgroup_setting_set(cgroup_name, "cpu", "cpu.cfs_period_us", "100000");
        }
    } else if(cthulhu_cgroup_setting_exists("cpu", "cpu.shares")) {
        // rt scheduler
        if((cpu < 0) || (cpu >= 100)) {
            res |= cthulhu_cgroup_setting_set(cgroup_name, "cpu", "cpu.shares", "1024");
        } else {
            char cpu_string[32];
            sprintf(cpu_string, "%d", (cpu * 1024) / 100);
            res |= cthulhu_cgroup_setting_set(cgroup_name, "cpu", "cpu.shares", cpu_string);

        }
    }
    return res;
}

/**
 * @brief Configure the cgroups of a sandbox
 *
 * @param cgroup_name The cgroup name of the sandbox
 * @param sb_obj
 * @param notif_obj
 * @param command_id
 * @return int 0 on success, -1 on failure
 */
int cthulhu_cgroup_config(const char* cgroup_name, amxd_object_t* sb_obj, cthulhu_notif_data_t* notif_data) {
    int res = -1;
    if(cthulhu_config_devices(cgroup_name, sb_obj) < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not configure device cgroup for [%s]", cgroup_name);
        goto exit;
    }
    if(cthulhu_cgroup_config_cpu(cgroup_name, sb_obj) < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not configure cpu cgroup for [%s]", cgroup_name);
        goto exit;
    }
    if(cthulhu_cgroup_config_memory(cgroup_name, sb_obj) < 0) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Could not configure memory cgroup for [%s]", cgroup_name);
        goto exit;
    }
    res = 0;
exit:
    return res;
}
/**
 * @brief get or create a cgroup name for a sandbox
 * the function takes cgroups of parents into consideration
 * The returned char* should be freed
 *
 * @param sb_id
 * @return char*
 */
char* cthulhu_cgroup_dir_get(const char* sb_id) {
    char* cgroup_dir = NULL;
    char* parent_cgroup_dir = NULL;
    char* parent_sb_id = NULL;
    amxd_object_t* sb_obj = NULL;
    cthulhu_sb_priv_t* sb_priv = NULL;

    sb_obj = cthulhu_sb_get(sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Sandbox does not exist", sb_id);
        return NULL;
    }
    sb_priv = cthulhu_sb_get_priv(sb_obj);
    if(sb_priv && sb_priv->cgroup_dir) {
        // return the cached value
        return strdup(sb_priv->cgroup_dir);
    }
    parent_sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_PARENT, NULL);
    // case 0: the sb has no parent
    if(string_is_empty(parent_sb_id)) {
        if(asprintf(&cgroup_dir, "%s/%s", CGROUP_BASE, sb_id) < 0) {
            SAH_TRACEZ_ERROR(ME, "SB[%s]: Failed to allocate memory for string", sb_id);
        }
        goto exit;
    }
    // case 1: append this sb to the parent dir
    parent_cgroup_dir = cthulhu_cgroup_dir_get(parent_sb_id);
    if(string_is_empty(parent_cgroup_dir)) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Parent cgroup dir is empty", sb_id);
        goto exit;
    }
    if(asprintf(&cgroup_dir, "%s/%s", parent_cgroup_dir, sb_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Failed to allocate memory for string", sb_id);
    }

exit:
    free(parent_sb_id);
    free(parent_cgroup_dir);
    return cgroup_dir;
}
