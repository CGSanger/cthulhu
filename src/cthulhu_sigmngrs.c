/****************************************************************************
**
** Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE 1 // to make strdup work

#include "cthulhu_priv.h"

#define ME "cthulhu_sigmngrs"

typedef struct cthulhu_sigmngr {
    amxp_signal_mngr_t* sigmngr;
    amxc_htable_it_t it;
} cthulhu_sigmngr_t;


static void cthulhu_sigmngr_del(cthulhu_sigmngr_t** cthulhu_sigmngr) {
    amxc_htable_it_take(&(*cthulhu_sigmngr)->it);
    amxp_sigmngr_delete(&(*cthulhu_sigmngr)->sigmngr);
    free(*cthulhu_sigmngr);
    *cthulhu_sigmngr = NULL;
}

static int cthulhu_sigmngr_new(cthulhu_sigmngr_t** cthulhu_sigmngr) {
    int retval = -1;

    *cthulhu_sigmngr = (cthulhu_sigmngr_t*) calloc(1, sizeof(cthulhu_sigmngr_t));
    if(*cthulhu_sigmngr == NULL) {
        goto exit;
    }
    amxp_sigmngr_new(&(*cthulhu_sigmngr)->sigmngr);
    amxp_sigmngr_add_signal((*cthulhu_sigmngr)->sigmngr, SIG_RUNNING);
    amxp_sigmngr_add_signal((*cthulhu_sigmngr)->sigmngr, SIG_RUNNING_CANCELED);
    amxp_sigmngr_add_signal((*cthulhu_sigmngr)->sigmngr, SIG_STOPPED);
    amxp_sigmngr_add_signal((*cthulhu_sigmngr)->sigmngr, SIG_REMOVED);

    retval = 0;

exit:
    if((retval != 0) && (*cthulhu_sigmngr != NULL)) {
        cthulhu_sigmngr_del(cthulhu_sigmngr);
    }
    return retval;
}


static void cthulhu_sigmngrs_clean_it(UNUSED const char* key, amxc_htable_it_t* it) {
    cthulhu_sigmngr_t* cthulhu_sigmngr = amxc_htable_it_get_data(it, cthulhu_sigmngr_t, it);
    cthulhu_sigmngr_del(&cthulhu_sigmngr);
}

int cthulhu_sigmngrs_init(amxc_htable_t* sigmngrs) {
    return amxc_htable_init(sigmngrs, 10);
}

void cthulhu_sigmngrs_clean(amxc_htable_t* sigmngrs) {
    amxc_htable_clean(sigmngrs, cthulhu_sigmngrs_clean_it);
}

amxp_signal_mngr_t* cthulhu_sigmngrs_get(amxc_htable_t* sigmngrs, const char* key) {
    amxc_htable_it_t* it = NULL;
    cthulhu_sigmngr_t* cthulhu_sigmngr = NULL;

    it = amxc_htable_get(sigmngrs, key);
    if(it) {
        cthulhu_sigmngr = amxc_htable_it_get_data(it, cthulhu_sigmngr_t, it);
        return cthulhu_sigmngr->sigmngr;
    }
    if(cthulhu_sigmngr_new(&cthulhu_sigmngr) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create signal manager");
        return NULL;
    }
    amxc_htable_insert(sigmngrs, key, &cthulhu_sigmngr->it);
    return cthulhu_sigmngr->sigmngr;

}

typedef struct delayed_remove_data {
    amxc_htable_t* sigmngrs;
    char* key;
    cthulhu_sigmngrs_check_exists_fn_t fn;
} delayed_remove_data_t;

static void delayed_remove_data_delete(delayed_remove_data_t** data) {
    free((*data)->key);
    free(*data);
    *data = NULL;
}

static void cthulhu_sigmngrs_delayed_remove_cb(UNUSED amxp_timer_t* timer, void* priv) {
    delayed_remove_data_t* data = priv;
    if(!data->fn(data->key)) {
        // the key can be removed
        amxc_htable_it_t* it = amxc_htable_take(data->sigmngrs, data->key);
        if(it) {
            amxc_htable_it_clean(it, cthulhu_sigmngrs_clean_it);
        }
    }

    amxp_timer_delete(&timer);
    delayed_remove_data_delete(&data);
}


void PRIVATE cthulhu_sigmngrs_delayed_remove(amxc_htable_t* sigmngrs, const char* key, cthulhu_sigmngrs_check_exists_fn_t fn) {
    amxp_timer_t* timer = NULL;
    delayed_remove_data_t* data = calloc(1, sizeof(delayed_remove_data_t));
    data->sigmngrs = sigmngrs;
    data->key = strdup(key);
    data->fn = fn;

    if(amxp_timer_new(&timer, cthulhu_sigmngrs_delayed_remove_cb, data) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create timer");
        delayed_remove_data_delete(&data);
        return;
    }

    amxp_timer_start(timer, 100);

}