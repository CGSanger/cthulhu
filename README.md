# Cthulhu

[[_TOC_]]

## Introduction

Cthulhu is a container manager that processes images that follow the OCI image spec, and creates containers for different container runtime engines, such as [lxc](https://gitlab.com/soft.at.home/lcm/modules/cthulhu-lxc) and crun.

## Building, installing and testing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    If you have no clue how to do this here are some links that could help you:

    - [Get Docker Engine - Community for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)  <br /><br />

    Make sure you user id is added to the docker group:

    ```
    sudo usermod -aG docker $USER
    ```

1. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/workspace/lcm
    ```

    Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx/:/home/$USER/workspace/lcm/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for the lcm project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites

- [lib\_amxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [lib\_amxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [lib\_amxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [lib\_amxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [lib\_amxm](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxm)
- [lib\_sahtrace](https://gitlab.com/soft.at.home/network/libsahtrace)
- [yajl](https://lloyd.github.io/yajl/)
- [libarchive](https://www.libarchive.org/)
- [libcthulhu](https://gitlab.com/soft.at.home/lcm/libraries/libcthulhu)

#### Build cthulhu

1. Clone the git repository

    To be able to build it, you need the source code. So make a directory for the LCM and clone this library in it.

    ```bash
    mkdir -p ~/workspace/lcm/applications
    cd ~/workspace/lcm/applications
    git clone git@gitlab.com:prpl-foundation/lcm/applications/cthulhu.git
    ```

1. Install dependencies

    Although the container will contain all tools needed for building, it does not contain all the libraries needed for building `libcthulhu`. To be able to build `libcthulhu` you need certain libraries to be installed in the container.

    ```bash
    sudo apt update
    sudo apt install libamxc
    sudo apt install libamxp
    sudo apt install libamxd
    sudo apt install libamxo
    sudo apt install libamxm
    sudo apt install libsahtrace
    sudo apt install libyajl-dev
    sudo apt install libarchive-dev

    ```

1. Build it

   In the container:

    ```bash
    cd ~/workspace/lcm/applications/cthulhu
    make
    ```

### Installing

#### Using make target install

You can install your own compiled version easily in the container by running the install target.

```bash
cd ~/workspace/lcm/applications/cthulhu
sudo -E make install
```

#### Using package

From within the container you can create packages.

```bash
cd ~/workspace/lcm/applications/cthulhu
make package
```

The packages generated are:

```
~/workspace/lcm/applications/cthulhu/cthulhu-<VERSION>.tar.gz
~/workspace/lcm/applications/cthulhu/cthulhu-<VERSION>.deb
```

You can copy these packages and extract/install them.

For ubuntu or debian distributions use dpkg:

```bash
sudo dpkg -i ~/workspace/lcm/applications/cthulhu/cthulhu_<VERSION>.deb
```

### Testing

#### Prerequisites

No extra components are needed for testing `cthulhu`.

#### Run tests

You can run the tests by executing the following command.

```bash
cd ~/workspace/lcm/applications/cthulhu
make test
```

Or this command if you also want the coverage tests to run:

```bash
cd ~/workspace/lcm/applications/cthulhu/test
make run coverage
```

#### Coverage reports

The coverage target will generate coverage reports using [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and [gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each file (*.c files) is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are available in the output directory of the compiler used.
Example: using native gcc
When the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML coverage reports can be found at `~/workspace/lcm/applications/cthulhu/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser.
In the container start a python3 http server in background.

```bash
cd ~/workspace/lcm/
python3 -m http.server 8080 &
```

Use the following url to access the reports `http://<IP ADDRESS OF YOUR CONTAINER>:8080/workspace/lcm/applications/cthulhu/output/<MACHINE>/coverage/report`
You can find the ip address of your container by using the `ip` command in the container.

Example:

```bash
$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
173: eth0@if174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:ac:11:00:07 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.7/16 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 2001:db8:1::242:ac11:7/64 scope global nodad
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:7/64 scope link
       valid_lft forever preferred_lft forever
```

in this case the ip address of the container is `172.17.0.7`.
So the uri you should use is: `http://172.17.0.7:8080/workspace/lcm/applications/cthulhu/output/x86_64-linux-gnu/coverage/report/`

## Usage

After installation the process can be started with
```bash
cthulhu
```

## System Bus API

Cthulhu exposes an API on the system bus via Ambiorix/BAAPI.

### Loading plugins

**TBD**

## Sandboxes
Sandboxes can be used to create an environment for the containers. It contains 3 families of restrictions:
* cgroups
* namespaces
* disk space

### cgroups
Cgroups will restrict a resource of the container
### CPU
The CPU restricion is a percent. This percent will translate in
#### on kernel with a Completely Fair Scheduler (CFS)
* cpu.cfs_quota_us = CPU percent * 10000
* cpu.cfs_period_us = 1000000
#### on a RT kernel
* cpu.shares = (CPU percent * 1024) / 100

### Commands

To send a command to Cthulhu, invoke the RPC "command()", with parameters:

* **Command**: the name of the command. Each command has its own subsection below.
* **CommandID**: (optional) a correlation ID which can be used to link this command with any resulting notificatiuon(s).

If a commandID is present then Cthulhu will copy this into any notification or error message from Cthulhu to the Client which is a result of this command.
For example if a "create" command contains the property ```"commandID" : "b6620f1d-b4bd-40e2-9971-05d9980ce611"```,
then the resulting notification of an error or of a change of state to "created" will also contain the property ```"commandID" : "b6620f1d-b4bd-40e2-9971-05d9980ce611"```.
(Note : This is similar to the treatment of the msg_id field in TR-369).

If a command from the Client to Cthulhu does not contain a "commandID" property then this property will not be present in any resulting notification from Cthulhu to the Client.
The property will also not be present in notifications from Cthulhu to the Client which are not the result of a command from the Client to Cthulhu
(for example notification of unexpected termination of a container).

* **Parameters**: (optional) hashtable holding the parameters of the command: the applicable keys depend on the command.

Ambiorix ODL definition:

    void command(%in %mandatory %strict string Command,
                 %in %strict string CommandID,
                 %in %strict htable Parameters);

#### create

Instructs Cthulhu to create a container.

Return value: *void*.

* mandatory parameter: *string* **containerID**

If the **containerID** matches that of any existing container on the CPE (including those in a 'paused' or 'stopped' state),
Cthulhu will immediately publish a "created" notification and will take no further action.
Otherwise Cthulhu will attempt to create the container and either a "created" notification or an error will result.

* mandatory parameter: *string* **bundlename**

This determines the name of a subdirectory within the configured OCI bundle directory where the OCI runtime bundle will be created.

#### delete

Instructs Cthulhu to remove the definition for a container.

Return value: *void*.

* mandatory parameter: *string* **containerID**

If **containerID** does not match the containerID of any existing container on the CPE, Cthulhu will immediately publish a "deleted" notification and will take no further action.
Otherwise Cthulhu will attempt to delete the container and either a "deleted" notification or an error will result.

* optional parameter: *boolean* **force**

If the "force" property is present and has the value true then the container will be deleted even if it is in the "running" state;
otherwise an attempt to delete a running container will result in an error (and the container will not be deleted).

#### start

Instructs Cthulhu to start a container.

* mandatory parameter: *string* **containerID**

If **containerID** does not match the containerID of an existing container then Cthulhu will publish an error message and will take no further action.

If the specified container is already running then Cthulhu will immediately publish a "started" notification and will take no further action.
Otherwise Cthulhu will attempt to start the container and either a "started" notification or an error will result.

#### stop

Instructs Cthulhu to stop a container.

* mandatory parameter: *string* **containerID**

If **containerID** does not match the containerID of an existing container then Cthulhu will publish an error message and will take no further action.

If the specified container is not running then Cthulhu will immediately publish a "stopped" notification and will take no further action.
Otherwise Cthulhu will attempt to stop the container and either a "stopped" notification or an error will result.

#### ls

Requests a list of the defined containers.  The result is returned in a "list" notification.

* optional parameter: *boolean* **quiet**

If this parameter is present and has the value true then the resulting notification will contain only the containerIDs.
Otherwise it will also contain the PID, status, and bundle path of each container.

### Notifications

Cthulhu reports state changes by means of Ambiorix events. The execution of a command generally results in an event, reporting either the success of the command or its failure.
Therefore the component which sends commands to Cthulhu should first subscribe to receive these events.

Each notification contains the following fields:

* **ContainerID** of the container to which the notification relates
* **Parameters** depending on the specific event, see below

Exception: the **list** event does not have a **containerID** field; instead it has a **containers** field which lists all known containers.

If the notification is the result of a command and that command contained a **CommandID**, the following field will also be present:
* **CommandID** of the command which resulted in this notification.

#### event cthulhu:created

Parameters:
* **status**
* **bundlename**
* **pid**

#### event cthulhu:running

Parameters: none

#### event cthulhu:paused

Parameters: none

#### event cthulhu:stopped

Parameters: none

#### event cthulhu:list

Parameters:
* **containers**
    * **containerID**
    * **status**
    * **bundlename**
    * **created**
    * **owner**
    * **pid**

#### event cthulhu:ps

Parameters:

#### event cthulhu:state

Parameters:

#### event cthulhu:update

Parameters:

#### event cthulhu:error

Parameters:
* **command**
* **type**
* **reason**
* **information**



### AutoRestart
A container can be configured to start automatically when Cthulhu starts, or to restart automatically when the container is stopped, either manually or because of a failure. The configuration is
done in the datamodel:
```bash
Cthulhu.Containers.test2
Cthulhu.Containers.test2.Bundle=alpine
Cthulhu.Containers.test2.Owner=
Cthulhu.Containers.test2.Name=test2
Cthulhu.Containers.test2.Pid=2614
Cthulhu.Containers.test2.Status=Running
Cthulhu.Containers.test2.Created=Mon May 10 09:09:48 2021
Cthulhu.Containers.test2.AutoRestart
Cthulhu.Containers.test2.AutoRestart.Enabled=1
Cthulhu.Containers.test2.AutoRestart.LastRetry=0001-01-01T00:00:00Z
Cthulhu.Containers.test2.AutoRestart.RunningSince=2021-05-12T15:27:20.158785082Z
Cthulhu.Containers.test2.AutoRestart.NextRetry=1970-01-01T00:00:00Z
Cthulhu.Containers.test2.AutoRestart.RetryCount=0
```
The AutoRestart section has as parameters:

* **Enabled**: if true, the feature is enabled
* **LastRetry**: the last time a try was done to start the container
* **RunningSince**: the time that the container was started. This will be reset when the container stops
* **NextRetry**: the time when the container will be started automatically again
* **RetryCount**: indicates how many times the container has been retied to start. This will reset when a container is successfully running for 24h

If a container does not start, or does not run longer then 24h, then the interval between retries will be augmented. The interval between each retry will be $$60*2^{RetryCount}$$ seconds, with a maximum of 24h between each retry attempt.

### OverlayFS
The RootFs of a container will be constructed from the different layers that are defined in the image spec of the image. OverlayFS can be used to create the RootFs of the (runtime image) container. This can be used to accomplish two distinct goals:
* **Disk usage efficiency**: If different containers share one or more layers, each layer will be extracted to disk only once. Without overlayFS, each layer will be extracted for every container that uses it.
* **Persistent storage for the container**: Optionally, a *upper dir* can be layered on top of the RootFs. This permits that every disk action performed by the container is stored seperately in a layer; the base layers are unaffected. If desired, this upper dir can also be limited in size.
#### Creation of a container
During the creation of a container, Cthulhu will check if OverlayFS is supported, and
* either extract the layers seperately (if they are not yet extracted),
* or extract all layers in one RootFs if OverlayFS is not enabled
```mermaid
graph TD
    A(Container.Create) --> B{Cthulhu.Config.UseOverlayFS=True?}
    B --> |Y|C{OverlayFS supported on CPE?}
    B --> |N|D("Extract RootFs to<br/>/usr/share/cthulhu/rootfs/#lt;ctrName#gt;")
    C --> |Y|E("Extract each layer seperately to<br/>/usr/share/cthulhu/layers/#lt;layerHash#gt;")
    C --> |N|D
```
#### Running of a container
When the container starts up, it needs a RootFs. If overlayFS is disabled, then the RootFs is extracted during the creation step. If overlayFs is enabled, then the layers will be mounted as overlay to create the RootFs. Then, depending on the configuration, an upper dir can be layered on top to have a writable persistent storage for the container.
* **Persistent OverlayFS disabled**: a user may choose not to enable the overlayFS writable layer by setting Cthulhu.Sandbox.x.AllocatedDiskSpace to 0
* **Limited Persistent Overlay**: if Cthulhu.Sandbox.x.AllocatedDiskSpace is not 0, then an ext2 filesystem image is created as a file, and then mounted to /usr/share/cthulhu/persistence/<ctr_name>. This dir will be used as upperdir for overlayFS.
```mermaid
graph TD
    A(Container.Start) --> B{Cthulhu.Config.OverlayFS=True?}
    B --> |Y|C{OverlayFS supported on CPE?}
    B --> |N|D("Use RootFs created in Container.create")
    C --> |Y|E{"Persistent OverlayFs enabled?"}
    C --> |N|D
    E --> |Y|H("Create ext4 fs image<br/>if it does not exist yet")
    E --> |N|L("Create OverlayFS without<br/>upper dir (=read-only)")
    H --> J("Create OverlayFS with<br/>upper dir (=persistent)")
```

