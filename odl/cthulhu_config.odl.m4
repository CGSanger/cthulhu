%populate {
    object 'Cthulhu' {
        object 'Config' {
            parameter 'DefaultBackend' = "LIBDIR/cthulhu-lxc/cthulhu-lxc.so";
            parameter 'ImageLocation' = "/usr/share/rlyeh/images";
            parameter 'BlobLocation' = "/usr/share/rlyeh/blobs";
            parameter 'StorageLocation' = "/usr/share/cthulhu";
            parameter 'PluginLocation' = "LIBDIR/amx/cthulhu/plugins";
            parameter 'UseOverlayFS' = true;
        }
    }
}