#!/bin/sh

name="cthulhu"
dm_name="Cthulhu"

kill_pid()
{
    PIDFILE=${1}
    if [ -f "${PIDFILE}" ]; then
        kill `cat ${PIDFILE}`
    else
        echo "Can't find ${PIDFILE}"
    fi
}

case $1 in
    start|boot)
        echo 1 > /sys/fs/cgroup/memory/memory.use_hierarchy
        $name -D
        ;;
    stop)
        kill_pid /var/run/${name}.pid
        ;;
    debuginfo)
        ubus-cli "Cthulhu.?"        
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
        echo "TODO log ${name}"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac
