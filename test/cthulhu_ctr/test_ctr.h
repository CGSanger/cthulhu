/****************************************************************************
**
** - DISCLAIMER OF WARRANTY -
**
** THIS FILE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
** EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
** PURPOSE.
**
** THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOURCE
** CODE IS WITH YOU. SHOULD THE SOURCE CODE PROVE DEFECTIVE, YOU
** ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
**
** - LIMITATION OF LIABILITY -
**
** IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
** WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES
** AND/OR DISTRIBUTES THE SOURCE CODE, BE LIABLE TO YOU FOR DAMAGES,
** INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
** ARISING OUT OF THE USE OR INABILITY TO USE THE SOURCE CODE
** (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
** INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
** OF THE SOURCE CODE TO OPERATE WITH ANY OTHER PROGRAM), EVEN IF SUCH
** HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGES.
**
****************************************************************************/

#ifndef __TEST_LOAD_PLUGIN_H__
#define __TEST_LOAD_PLUGIN_H__

int test_cthulhu_ctr_setup(void** state);
int test_cthulhu_ctr_teardown(void** state);


void test_cthulhu_create_ctr(void** state);
void test_cthulhu_start_ctr(void** state);
void test_cthulhu_stop_ctr(void** state);
void test_cthulhu_remove_ctr(void** state);
void test_cthulhu_restart_ctr(void** state);
void test_cthulhu_list_ctr(void** state);
void test_cthulhu_update_ctr_keepdata(void** state);
void test_cthulhu_update_ctr_removedata(void** state);

void test_cthulhu_create_ctr_disklocation(void** state);
void test_cthulhu_update_ctr_disklocation(void** state);
void test_cthulhu_remove_ctr_disklocation(void** state);

void test_cthulhu_create_ctr_cmd(void** state);
void test_cthulhu_remove_ctr_cmd(void** state);

#endif // __TEST_LOAD_PLUGIN_H__
