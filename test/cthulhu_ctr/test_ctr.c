/****************************************************************************
**
** - DISCLAIMER OF WARRANTY -
**
** THIS FILE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
** EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
** PURPOSE.
**
** THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOURCE
** CODE IS WITH YOU. SHOULD THE SOURCE CODE PROVE DEFECTIVE, YOU
** ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
**
** - LIMITATION OF LIABILITY -
**
** IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
** WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES
** AND/OR DISTRIBUTES THE SOURCE CODE, BE LIABLE TO YOU FOR DAMAGES,
** INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
** ARISING OUT OF THE USE OR INABILITY TO USE THE SOURCE CODE
** (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
** INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
** OF THE SOURCE CODE TO OPERATE WITH ANY OTHER PROGRAM), EVEN IF SUCH
** HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGES.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>


#include "cthulhu_priv.h"
#include "test_ctr.h"
#include "expected_signal.h"
#include "test_setup.h"



#define ME "test"

const char* sb_name = "sb_for_ctr";

int test_cthulhu_ctr_setup(void** state) {
    int res = test_cthulhu_setup(state);
    testhelper_cthulhu_create_sandbox(sb_name, true);
    return res;
}
int test_cthulhu_ctr_teardown(void** state) {
    testhelper_cthulhu_stop_sandbox(sb_name);
    return test_cthulhu_teardown(state);
}

void test_cthulhu_create_ctr(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_create_ctr_cmd(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";
    char* command_id = "cid_1";

    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_CTR_CREATE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, params, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, params, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, params, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, params, CTHULHU_CONTAINER_AUTOSTART, false);

    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_start_ctr(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_START, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_STARTING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_stop_ctr(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_STOP, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_restart_ctr(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_RESTART, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_RESTARTING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "dm:object-changed", false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_RESTARTING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_list_ctr(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_LS, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:list", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_update_ctr_keepdata(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.15");
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_REMOVEDATA, false);

    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLE, "alpine", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLEVERSION, "3.15", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_RUNNING, false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_update_ctr_removedata(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_REMOVEDATA, true);

    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLE, "alpine", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLEVERSION, "3.14", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_RUNNING, false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_remove_ctr(UNUSED void** state) {
    testhelper_remove_ctr("ctr_1");
}

void test_cthulhu_remove_ctr_cmd(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";
    char* command_id = "cid_2";

    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_CTR_REMOVE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_CONTAINER_ID, ctr_id);

    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:removed", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_create_ctr_disklocation(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_disklocation";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "disklocation");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.20");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_DISKLOCATION, "subdir1/subdir2/disklocation");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_update_ctr_disklocation(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_disklocation";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "disklocationupdate");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.21");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_DISKLOCATION, "subdir1/subdir3/disklocationupdate");
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_REMOVEDATA, false);

    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLE, "disklocationupdate", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLEVERSION, "3.21", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_RUNNING, false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_remove_ctr_disklocation(UNUSED void** state) {
    testhelper_remove_ctr("ctr_disklocation");
}