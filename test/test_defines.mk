MACHINE = $(shell $(CC) -dumpmachine)
SHELL = /bin/bash

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv)
INCDIR_LIBOCISPEC =  $(realpath ../../src/libocispec/src)
COMMON_SRCDIR = $(realpath ../common/)
INCDIR += $(realpath ../common_includes)

HEADERS = $(wildcard $(INCDIR)/$(TARGET)/*.h)
SOURCES = $(wildcard $(SRCDIR)/cthulhu*.c)
SOURCES += $(wildcard $(COMMON_SRCDIR)/*.c)

CFLAGS += -Wall -Wextra -Wno-attributes \
          --std=gnu99 -g3 -Wmissing-declarations \
		  -Wno-unused-but-set-variable \
		  -Wno-unused-variable \
		  $(addprefix -I ,$(INCDIR) $(INCDIR_LIBOCISPEC)) \
		  -fprofile-arcs -ftest-coverage \
		  -fkeep-inline-functions -fkeep-static-functions \
		  $(shell pkg-config --cflags cmocka)
LDFLAGS += -fprofile-arcs -ftest-coverage \
           -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxd -lamxm -lamxo -lamxp -lamxj -lamxb -lsahtrace \
		   -lcthulhu -L../../src/libocispec/.libs -locispec -larchive \
		   -lyajl

CFLAGS += $(shell pkg-config --cflags libnl-3.0 libnl-route-3.0)
LDFLAGS += $(shell pkg-config --libs libnl-3.0 libnl-route-3.0)
