
typedef struct _expected_signal {
    char* signame;
    bool print;
    amxc_htable_t matches;
} expected_signal_t;

void sig_read_all(void);

int expected_sig_init(expected_signal_t* sig, const char* const signame, bool print);
void expected_sig_clean(expected_signal_t* sig);
int expected_sig_add_match_bool(expected_signal_t* sig, const char* const key, bool val, bool exclusive);
int expected_sig_add_match_int32_t(expected_signal_t* sig, const char* const key, int32_t val, bool exclusive);
int expected_sig_add_match_cstring_t(expected_signal_t* sig, const char* const key, const char* const val, bool exclusive);

int expected_sig_wait(expected_signal_t* const sig, int timeout);
