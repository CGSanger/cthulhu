#define UNUSED __attribute__((unused))

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>

int test_cthulhu_setup(UNUSED void** state);
int test_cthulhu_teardown(UNUSED void** state);

void testhelper_cthulhu_create_sandbox(const char* sb_name, bool enabled);
void testhelper_cthulhu_stop_sandbox(const char* name);

void testhelper_remove_ctr(const char* ctr_id);

amxd_dm_t* test_get_dm(void);
