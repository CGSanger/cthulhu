/****************************************************************************
**
** - DISCLAIMER OF WARRANTY -
**
** THIS FILE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
** EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
** PURPOSE.
**
** THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOURCE
** CODE IS WITH YOU. SHOULD THE SOURCE CODE PROVE DEFECTIVE, YOU
** ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
**
** - LIMITATION OF LIABILITY -
**
** IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
** WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES
** AND/OR DISTRIBUTES THE SOURCE CODE, BE LIABLE TO YOU FOR DAMAGES,
** INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
** ARISING OUT OF THE USE OR INABILITY TO USE THE SOURCE CODE
** (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
** INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
** OF THE SOURCE CODE TO OPERATE WITH ANY OTHER PROGRAM), EVEN IF SUCH
** HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGES.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_priv.h"
#include "test_sb.h"
#include "expected_signal.h"
#include "test_setup.h"



#define ME "test"

static void test_cthulhu_stop_sandbox_with_name(const char* name);

int test_cthulhu_sb_setup(UNUSED void** state) {
    return test_cthulhu_setup(state);
}
int test_cthulhu_sb_teardown(UNUSED void** state) {
    test_cthulhu_stop_sandbox_with_name("my_sandbox");
    return test_cthulhu_teardown(state);
}

void test_cthulhu_create_sandbox(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_SANDBOX_ENABLE, false);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_CREATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_start_sandbox(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_START, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void test_cthulhu_stop_sandbox_with_name(const char* name) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_STOP, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Disabled", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_stop_sandbox(UNUSED void** state) {
    test_cthulhu_stop_sandbox_with_name("my_sandbox");
}

void test_cthulhu_restart_sandbox(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    SAH_TRACEZ_INFO(ME, "Restart");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_RESTART, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Sandbox.Instances.cpe-my_sandbox.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", "Restarting", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

}

void test_cthulhu_list_sandbox(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_LS, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:list", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    amxc_var_add_new_key_bool(&args, CTHULHU_CMD_CTR_LS_QUIET, true);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_LS, &args, &ret), amxd_status_ok);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_exec_sandbox(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CMD_SB_EXEC_CMD, "ls");
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_EXEC, &args, &ret), amxd_status_ok);
    sig_read_all();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_remove_sandbox(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_REMOVE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:removed", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    sleep(1);
    amxp_timers_calculate();
    amxp_timers_check();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_create_sandbox_cmd(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_1";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_CREATE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(bool, params, CTHULHU_SANDBOX_ENABLE, false);

    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_start_sandbox_cmd(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_1";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_START);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


void test_cthulhu_stop_sandbox_cmd(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_2";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_STOP);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Disabled", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_restart_sandbox_cmd(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_3";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_RESTART);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

}

void test_cthulhu_list_sandbox_cmd(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_4";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_LS);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_LS, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:list", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


void test_cthulhu_remove_sandbox_cmd(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_5";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_REMOVE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:removed", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    sleep(1);
    amxp_timers_calculate();
    amxp_timers_check();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_create_sandbox_cmd_empty(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_CREATE);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(bool, params, CTHULHU_SANDBOX_ENABLE, false);

    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_create_sandbox_enabled(UNUSED void** state) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_SANDBOX_ENABLE, true);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_CREATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}