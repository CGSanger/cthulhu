all: $(TARGET)

run: $(TARGET)
	set -o pipefail; CMOCKA_TEST_ABORT='1' valgrind --num-callers=50 --leak-check=full \
	 --suppressions=../valgrind.supp --show-possibly-lost=no \
	 --error-exitcode=1 ./$< 2>&1 | tee -a $(OBJDIR)/unit_test_results.txt;

$(TARGET): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)

-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: $(TEST_SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/%.o: $(COMMON_SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	mkdir -p $@

clean:
ifneq ($(TARGET),)
	rm -rf $(TARGET)
else
	$(warning TARGET is empty)
endif
ifneq ($(OBJDIR),)
	rm -rf $(OBJDIR)/*
else
	$(warning OBJDIR is empty)
endif

.PHONY: clean $(OBJDIR)/
