

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>
#include "cthulhu_priv.h"

#include "wrap_amxm.h"
#include "expected_signal.h"
#include "test_setup.h"

static amxm_module_t dummy;
static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_definition = "../../odl/cthulhu_definition.odl";
static const char* odl_config = "../config/cthulhu_config.odl";

amxd_dm_t* test_get_dm(void) {
    return &dm;
}

void ignore_alarm(UNUSED int signum);
void ignore_alarm(UNUSED int signum) {
}

int test_cthulhu_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* cthulhu = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_parser_parse_file(&parser, "../config/cthulhu.odl", root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU "." CTHULHU_CMD_LOADBACKEND, AMXO_FUNC(_Cthulhu_loadBackend)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_CREATE, AMXO_FUNC(_Container_create)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_START, AMXO_FUNC(_Container_start)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_STOP, AMXO_FUNC(_Container_stop)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_REMOVE, AMXO_FUNC(_Container_remove)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_UPDATE, AMXO_FUNC(_Container_update)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_MODIFY, AMXO_FUNC(_Container_modify)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_LS, AMXO_FUNC(_Container_ls)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_RESTART, AMXO_FUNC(_Container_restart)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_COMMAND, AMXO_FUNC(_Container_command)), 0);

    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_CREATE, AMXO_FUNC(_Sandbox_create)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_START, AMXO_FUNC(_Sandbox_start)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_STOP, AMXO_FUNC(_Sandbox_stop)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_RESTART, AMXO_FUNC(_Sandbox_restart)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_REMOVE, AMXO_FUNC(_Sandbox_remove)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_LS, AMXO_FUNC(_Sandbox_ls)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_EXEC, AMXO_FUNC(_Sandbox_exec)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_COMMAND, AMXO_FUNC(_Sandbox_command)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);

    will_return(__wrap_amxm_so_open, 0);
    will_return(__wrap_amxm_so_get_module, &dummy);

    _cthulhu_main(0, &dm, &parser);
    _odl_loaded(NULL, NULL, NULL);

    sig_read_all();

    signal(SIGALRM, ignore_alarm);

    wrap_amxm_init();

    return 0;
}

int test_cthulhu_teardown(UNUSED void** state) {
    // process lingering signals and timers
    expected_signal_t expected_sig;
    expected_sig_init(&expected_sig, "nonexistingsignal", false);
    expected_sig_wait(&expected_sig, 5);
    expected_sig_clean(&expected_sig);

    _cthulhu_main(1, &dm, &parser);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    wrap_amxm_cleanup();

    return 0;
}

void testhelper_cthulhu_create_sandbox(const char* sb_name, bool enabled) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_SANDBOX_ENABLE, enabled);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_CREATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false);
    if(enabled) {
        expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    }
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void testhelper_cthulhu_stop_sandbox(const char* name) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_STOP, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Disabled", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void testhelper_remove_ctr(const char* ctr_id) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_REMOVE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:removed", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
