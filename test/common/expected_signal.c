#include "cthulhu_priv.h"
#include "expected_signal.h"

#define ME "test"

static bool expected_sig_found = false;
static bool expected_sig_exclusive_error = false;

typedef struct _match_data {
    amxc_htable_it_t hit; /**< Hash table iterator, can be used to store the variant in a hash table */
    amxc_var_t var;
    bool exclusive;       /* if exclusive is set, then this match is only allowed if the signal name and the other matches also match */
} match_data_t;

static int match_data_new(match_data_t** match_data) {
    int retval = -1;
    when_null(match_data, exit);

    *match_data = (match_data_t*) calloc(1, sizeof(match_data_t));
    when_null(*match_data, exit);
    when_failed(amxc_htable_it_init(&(*match_data)->hit), exit)

    retval = amxc_var_init(&(*match_data)->var);

exit:
    return retval;
}

static void match_data_delete(match_data_t** match_data) {
    when_null(match_data, exit);

    if(*match_data != NULL) {
        amxc_var_clean(&(*match_data)->var);
        amxc_htable_it_clean(&((*match_data)->hit), NULL);
    }

    free(*match_data);
    *match_data = NULL;

exit:
    return;
}

static void match_data_it_free(UNUSED const char* key, amxc_htable_it_t* it) {
    match_data_t* match_data = amxc_htable_it_get_data(it, match_data_t, hit);
    match_data_delete(&match_data);
}

static bool var_contains_data(const amxc_var_t* const data, const char* const path, const amxc_var_t* const match) {
    bool res = false;
    int comp_res = 0;
    const amxc_var_t* var = amxc_var_get_path(data, path, AMXC_VAR_FLAG_DEFAULT);
    when_null(var, exit);
    when_failed(amxc_var_compare(var, match, &comp_res), exit);
    res = !comp_res;
exit:
    return res;

}

static void find_matches(const amxc_var_t* const data,
                         const amxc_htable_t* const matches,
                         int* totalfound,
                         int* totalnotfound,
                         int* exclusivesfound,
                         bool print) {
    amxc_htable_for_each(it, matches) {
        match_data_t* match_data = amxc_htable_it_get_data(it, match_data_t, hit);
        const char* path = amxc_htable_it_get_key(it);
        if(var_contains_data(data, path, &match_data->var)) {
            *totalfound += 1;
            if(match_data->exclusive) {
                *exclusivesfound += 1;
            }
            if(print) {
                printf("%smatch found [path %s]:\n", match_data->exclusive? "exclusive ":"", path);
                amxc_var_dump(&match_data->var, 1);
            }
        } else {
            *totalnotfound += 1;
            if(print) {
                printf("%smatch not found [path %s]:\n", match_data->exclusive? "exclusive ":"", path);
                amxc_var_dump(&match_data->var, 1);
            }

        }
    }

}

static void expected_sig_slot(const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    when_null(priv, exit);
    int totalfound = 0;
    int totalnotfound = 0;
    int exclusivesfound = 0;

    expected_signal_t* const sig = (expected_signal_t* const) priv;

    if(sig->print) {
        printf("Received sig: %s, found %d notfound %d exclusives %d\n", sig_name, totalfound, totalnotfound, exclusivesfound);
        amxc_var_dump(data, 1);
    }
    find_matches(data, &sig->matches, &totalfound, &totalnotfound, &exclusivesfound, sig->print);
    if(sig->print) {
        printf("Matches found: %d notfound: %d exclusives: %d\n", totalfound, totalnotfound, exclusivesfound);
    }

    if(sig->signame && (strcmp(sig_name, sig->signame) != 0)) {
        if(exclusivesfound) {
            expected_sig_exclusive_error = true;
            if(sig->print) {
                printf("Exclusive error in Sig: %s\n", sig_name);
                amxc_var_dump(data, 1);
            }
        }
        goto exit;
    }

    if(totalnotfound == 0) {
        expected_sig_found = true;
    } else if(exclusivesfound) {
        printf("Exclusive error in Sig: %s\n", sig_name);
        expected_sig_exclusive_error = true;
    }
exit:
    return;
}

void sig_read_all(void) {
    while(amxp_signal_read() == 0) {
    }
}

int expected_sig_init(expected_signal_t* sig, const char* const signame, bool print) {
    int res = -1;
    when_null(sig, exit);

    sig->print = print;
    if(signame) {
        sig->signame = strdup(signame);
    } else {
        sig->signame = NULL;
    }
    res = amxc_htable_init(&(sig->matches), 5);
exit:
    return res;

}
void expected_sig_clean(expected_signal_t* sig) {
    when_null(sig, exit);
    free(sig->signame);
    amxc_htable_clean(&(sig->matches), match_data_it_free);
exit:
    return;
}

int expected_sig_add_match_bool(expected_signal_t* sig, const char* const key, bool val, bool exclusive) {
    int res = -1;
    match_data_t* match_data = NULL;

    when_null(sig, exit);
    when_null(key, exit);
    when_failed(match_data_new(&match_data), exit);
    when_failed(amxc_var_set_bool(&match_data->var, val), error);
    when_failed(amxc_htable_insert(&sig->matches, key, &match_data->hit), error);
    match_data->exclusive = exclusive;

    res = 0;
    goto exit;
error:
    match_data_delete(&match_data);
exit:
    return res;
}

int expected_sig_add_match_int32_t(expected_signal_t* sig, const char* const key, int32_t val, bool exclusive) {
    int res = -1;
    match_data_t* match_data = NULL;

    when_null(sig, exit);
    when_null(key, exit);
    when_failed(match_data_new(&match_data), exit);
    when_failed(amxc_var_set_int32_t(&match_data->var, val), error);
    when_failed(amxc_htable_insert(&sig->matches, key, &match_data->hit), error);
    match_data->exclusive = exclusive;

    res = 0;
    goto exit;
error:
    match_data_delete(&match_data);
exit:
    return res;
}

int expected_sig_add_match_cstring_t(expected_signal_t* sig, const char* const key, const char* const val, bool exclusive) {
    int res = -1;
    match_data_t* match_data = NULL;

    when_null(sig, exit);
    when_null(key, exit);
    when_null(val, exit);
    when_failed(match_data_new(&match_data), exit);
    when_failed(amxc_var_set_cstring_t(&match_data->var, val), error);
    when_failed(amxc_htable_insert(&sig->matches, key, &match_data->hit), error);
    match_data->exclusive = exclusive;

    res = 0;
    goto exit;
error:
    match_data_delete(&match_data);
exit:
    return res;
}

int expected_sig_wait(expected_signal_t* const sig, int timeout) {
    int res = -1;
    expected_sig_found = false;
    expected_sig_exclusive_error = false;

    if(amxp_slot_connect(NULL, "*", NULL, expected_sig_slot, sig) != 0) {
        SAH_TRACE_ERROR("Could not connect slot");
        goto exit;
    }

    while(timeout > 0) {
        while(!expected_sig_found && !expected_sig_exclusive_error && (amxp_signal_read() == 0)) {
        }
        if(expected_sig_exclusive_error) {
            goto exit;
        }
        if(expected_sig_found) {
            res = 0;
            goto exit;
        }
        sleep(1);
        amxp_timers_calculate();
        amxp_timers_check();
        timeout--;
    }
    if(expected_sig_found && !expected_sig_exclusive_error) {
        res = 0;
    }
exit:
    amxp_slot_disconnect(NULL, "*", expected_sig_slot);
    return res;
}

