#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_variant.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_priv.h"
#include "cthulhu_trace.h"
#include "wrap_amxm.h"

#define UNUSED __attribute__((unused))

static amxc_htable_t* containers;

typedef struct _ctr_info_with_it {
    cthulhu_ctr_info_t* ctr_info;
    amxc_llist_it_t lit;        /**< Linked list iterator, can be used to store the variant in a linked list */
    amxc_htable_it_t hit;       /**< Hash table iterator, can be used to store the variant in a hash table */
} ctr_info_with_it_t;

static void container_htable_it_free(UNUSED const char* key, amxc_htable_it_t* it) {
    ctr_info_with_it_t* ctr_info_wrapper = amxc_htable_it_get_data(it, ctr_info_with_it_t, hit);
    cthulhu_ctr_info_delete(&(ctr_info_wrapper->ctr_info));
    free(ctr_info_wrapper);
}

void wrap_amxm_init() {
    amxc_htable_new(&containers, 5);
}

void wrap_amxm_cleanup() {
    amxc_htable_delete(&containers, container_htable_it_free);
}

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);
amxm_module_t* __wrap_amxm_so_get_module(const amxm_shared_object_t* const shared_object,
                                         const char* const module_name);
int __wrap_amxm_so_close(amxm_shared_object_t** so);
int __wrap_amxm_so_execute_function(amxm_shared_object_t* const so,
                                    const char* const module_name,
                                    const char* const func_name,
                                    amxc_var_t* args,
                                    amxc_var_t* ret);

int __real_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so) {
    if(strcmp(shared_object_name, "packager") == 0) {
        int rv = (int) mock();
        if(rv == 0) {
            *so = (amxm_shared_object_t*) calloc(1, sizeof(amxm_shared_object_t));
        } else {
            *so = NULL;
        }
        return rv;
    }
    return __real_amxm_so_open(so, shared_object_name, path_to_so);
}

amxm_module_t* __real_amxm_so_get_module(const amxm_shared_object_t* const shared_object,
                                         const char* const module_name);

amxm_module_t* __wrap_amxm_so_get_module(const amxm_shared_object_t* const shared_object,
                                         const char* const module_name) {
    if(strcmp(module_name, "mod_cthulhu") == 0) {
        amxm_module_t* mod = mock_ptr_type(amxm_module_t*);
        return mod;
    }
    return __real_amxm_so_get_module(shared_object, module_name);
}

int __wrap_amxm_so_close(amxm_shared_object_t** so) {
    if(*so != NULL) {
        free(*so);
        *so = NULL;
    }

    return 0;
}

static int container_create(amxc_var_t* args) {
    int res = -1;
    const cthulhu_config_t* config = NULL;
    ctr_info_with_it_t* ctr_info_wrapper = NULL;
    config = amxc_var_get_const_cthulhu_config_t(args);
    if(!config) {
        SAH_TRACE_ERROR("Config argument is not of type cthulhu_config_t");
        goto exit;
    }
    if(amxc_htable_get(containers, config->id)) {
        SAH_TRACE_ERROR("container already exists");
        goto exit;
    }
    ctr_info_wrapper = calloc(1, sizeof(ctr_info_with_it_t));
    if(cthulhu_ctr_info_new(&(ctr_info_wrapper->ctr_info))) {
        SAH_TRACE_ERROR("Could not init ctr_info");
        free(ctr_info_wrapper);
        goto exit;
    }
    cthulhu_ctr_info_t* ctr_info = ctr_info_wrapper->ctr_info;
    ctr_info->ctr_id = strdup(config->id);
    ctr_info->bundle = strdup(config->bundle_name);
    ctr_info->bundle_version = strdup(config->bundle_version);
    ctr_info->sb_id = strdup(config->sandbox);
    ctr_info->status = cthulhu_ctr_status_stopped;

    amxc_htable_insert(containers, config->id, &ctr_info_wrapper->hit);

    res = 0;
exit:
    return res;
}

static int container_remove(amxc_var_t* args) {
    int res = -1;
    const cthulhu_config_t* config = NULL;
    ctr_info_with_it_t* ctr_info_wrapper = NULL;
    amxc_htable_it_t* it = NULL;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    if(string_is_empty(ctr_id)) {
        SAH_TRACE_ERROR("ContainerId is not set");
        goto exit;
    }
    it = amxc_htable_take(containers, ctr_id);
    if(!it) {
        SAH_TRACE_ERROR("Container does not exist");
        goto exit;
    }
    amxc_htable_it_clean(it, container_htable_it_free);

    res = 0;
exit:
    return res;
}

static int container_start(amxc_var_t* args) {
    int res = -1;
    const cthulhu_config_t* config = NULL;
    ctr_info_with_it_t* ctr_info_wrapper = NULL;
    amxc_htable_it_t* it = NULL;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    if(string_is_empty(ctr_id)) {
        SAH_TRACE_ERROR("ContainerId is not set");
        goto exit;
    }
    it = amxc_htable_get(containers, ctr_id);
    if(!it) {
        SAH_TRACE_ERROR("Container does not exist");
        goto exit;
    }
    ctr_info_wrapper = amxc_htable_it_get_data(it, ctr_info_with_it_t, hit);
    ctr_info_wrapper->ctr_info->status = cthulhu_ctr_status_running;

    res = 0;
exit:
    return res;
}

static int container_stop(amxc_var_t* args) {
    int res = -1;
    const cthulhu_config_t* config = NULL;
    ctr_info_with_it_t* ctr_info_wrapper = NULL;
    amxc_htable_it_t* it = NULL;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    if(string_is_empty(ctr_id)) {
        SAH_TRACE_ERROR("ContainerId is not set");
        goto exit;
    }
    it = amxc_htable_get(containers, ctr_id);
    if(!it) {
        SAH_TRACE_ERROR("Container does not exist");
        goto exit;
    }
    ctr_info_wrapper = amxc_htable_it_get_data(it, ctr_info_with_it_t, hit);
    ctr_info_wrapper->ctr_info->status = cthulhu_ctr_status_stopped;

    res = 0;
exit:
    return res;
}

static int container_list(UNUSED amxc_var_t* args, amxc_var_t* ret) {
    amxc_var_init(ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_htable_for_each(it, containers) {
        ctr_info_with_it_t* ctr_info_wrapper = amxc_htable_it_get_data(it, ctr_info_with_it_t, hit);
        // printf("ADD TO LIST: %s STATE: %s\n", ctr_info_wrapper->ctr_info->ctr_id,
        //        cthulhu_ctr_status_to_string(ctr_info_wrapper->ctr_info->status));
        cthulhu_ctr_info_t* cpy = NULL;
        cthulhu_ctr_info_new(&cpy);
        cthulhu_ctr_info_copy(cpy, ctr_info_wrapper->ctr_info);
        amxc_var_add_new_cthulhu_ctr_info_t(ret, cpy);
    }
    return 0;
}

int __real_amxm_so_execute_function(amxm_shared_object_t* const so,
                                    const char* const module_name,
                                    const char* const func_name,
                                    amxc_var_t* args,
                                    amxc_var_t* ret);

int __wrap_amxm_so_execute_function(amxm_shared_object_t* const so,
                                    const char* const module_name,
                                    const char* const func_name,
                                    amxc_var_t* args,
                                    amxc_var_t* ret) {
    // printf("Module_name %s fname %s\n", module_name, func_name);
    if(strcmp(module_name, "mod_cthulhu") == 0) {
        if(strcmp(func_name, "list") == 0) {
            return container_list(args, ret);
        }
        if(strcmp(func_name, "version") == 0) {
            return 0;
        }
        if(strcmp(func_name, "create") == 0) {
            return container_create(args);
        }
        if(strcmp(func_name, "remove") == 0) {
            return container_remove(args);
        }
        if(strcmp(func_name, "start") == 0) {
            return container_start(args);
        }
        if(strcmp(func_name, "stop") == 0) {
            return container_stop(args);
        }
        int rv = (int) mock();

        return rv;
    }
    return __real_amxm_so_execute_function(so, module_name, func_name, args, ret);
}

