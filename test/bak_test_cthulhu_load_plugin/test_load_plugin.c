/****************************************************************************
**
** - DISCLAIMER OF WARRANTY -
**
** THIS FILE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
** EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
** PURPOSE.
**
** THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOURCE
** CODE IS WITH YOU. SHOULD THE SOURCE CODE PROVE DEFECTIVE, YOU
** ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
**
** - LIMITATION OF LIABILITY -
**
** IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
** WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES
** AND/OR DISTRIBUTES THE SOURCE CODE, BE LIABLE TO YOU FOR DAMAGES,
** INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
** ARISING OUT OF THE USE OR INABILITY TO USE THE SOURCE CODE
** (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
** INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
** OF THE SOURCE CODE TO OPERATE WITH ANY OTHER PROGRAM), EVEN IF SUCH
** HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGES.
**
****************************************************************************/
#include <sys/time.h>
#include <sys/resource.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <inttypes.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_priv.h"
#include "test_load_plugin.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_definitions = "../../odl/cthulhu_definition.odl";

static amxm_module_t dummy;

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);
amxm_module_t* __wrap_amxm_so_get_module(const amxm_shared_object_t* const shared_object,
                                         const char* const module_name);
int __wrap_amxm_so_close(amxm_shared_object_t** so);
int __wrap_amxm_so_execute_function(amxm_shared_object_t* const so,
                                    const char* const module_name,
                                    const char* const func_name,
                                    amxc_var_t* args,
                                    amxc_var_t* ret);

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        UNUSED const char* shared_object_name,
                        UNUSED const char* const path_to_so) {
    int rv = (int) mock();
    if(rv == 0) {
        *so = (amxm_shared_object_t*) calloc(1, sizeof(amxm_shared_object_t));
    } else {
        *so = NULL;
    }

    return rv;
}

amxm_module_t* __wrap_amxm_so_get_module(UNUSED const amxm_shared_object_t* const shared_object,
                                         UNUSED const char* const module_name) {
    amxm_module_t* mod = mock_ptr_type(amxm_module_t*);
    return mod;
}

int __wrap_amxm_so_close(amxm_shared_object_t** so) {
    if(*so != NULL) {
        free(*so);
        *so = NULL;
    }

    return 0;
}

int __wrap_amxm_so_execute_function(UNUSED amxm_shared_object_t* const so,
                                    UNUSED const char* const module_name,
                                    UNUSED const char* const func_name,
                                    UNUSED amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    return 0;
}

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int test_cthulhu_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "loadPlugin", AMXO_FUNC(_Cthulhu_loadBackend)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "create", AMXO_FUNC(_Container_create)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "start", AMXO_FUNC(_Container_start)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "stop", AMXO_FUNC(_Container_stop)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_definitions, root_obj), 0);

    _cthulhu_main(0, &dm, &parser);

    handle_events();

    return 0;
}

int test_cthulhu_teardown(UNUSED void** state) {
    _cthulhu_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_cthulhu_rpc_fails_when_no_backend_loaded(UNUSED void** state) {
    amxd_object_t* cthulhu = amxd_dm_findf(&dm, "Cthulhu");
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(cthulhu);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, "my_container");
    amxc_var_add_key(cstring_t, &args, "bundlename", "alpine");
    assert_int_not_equal(amxd_object_invoke_function(cthulhu, "create", &args, &ret), amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, "my_container");
    assert_int_not_equal(amxd_object_invoke_function(cthulhu, "start", &args, &ret), amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, "my_container");
    assert_int_not_equal(amxd_object_invoke_function(cthulhu, "stop", &args, &ret), amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_can_load_plugin(UNUSED void** state) {
    amxd_object_t* cthulhu = amxd_dm_findf(&dm, "Cthulhu");
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(cthulhu);

    will_return(__wrap_amxm_so_open, -1);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/cthulhu/my_mod.so");
    assert_int_not_equal(amxd_object_invoke_function(cthulhu, "loadPlugin", &args, &ret), amxd_status_ok);

    will_return(__wrap_amxm_so_open, 0);
    will_return(__wrap_amxm_so_get_module, NULL);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/cthulhu/my_mod.so");
    assert_int_not_equal(amxd_object_invoke_function(cthulhu, "loadPlugin", &args, &ret), amxd_status_ok);

    will_return(__wrap_amxm_so_open, 0);
    will_return(__wrap_amxm_so_get_module, &dummy);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/cthulhu/my_mod.so");
    assert_int_equal(amxd_object_invoke_function(cthulhu, "loadPlugin", &args, &ret), amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/cthulhu/my_mod_other.so");
    assert_int_not_equal(amxd_object_invoke_function(cthulhu, "loadPlugin", &args, &ret), amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
